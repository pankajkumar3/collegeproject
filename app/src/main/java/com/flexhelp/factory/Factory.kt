package com.flexhelp.factory

import android.content.Context
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.flexhelp.MainVM
import com.flexhelp.views.aboutt.AboutVM
import com.flexhelp.views.add.address.AddAddressVM
import com.flexhelp.views.addcards.AddCardVM
import com.flexhelp.views.addresses.AddressVM
import com.flexhelp.views.bookings.BookingVM
import com.flexhelp.views.cancelbookings.CancelBookingVM
import com.flexhelp.views.changepasswords.ChangePasswordVM
import com.flexhelp.views.chats.ChatVM
import com.flexhelp.views.congratulations.CongratulationVM
import com.flexhelp.views.contactsus.ContactUsVM
import com.flexhelp.views.feedback.FeedBacksVM
import com.flexhelp.views.forgotpassword.ForgotPasswordsVM
import com.flexhelp.views.homee.HomeVM
import com.flexhelp.views.login.SignInVM
import com.flexhelp.views.mybooking.MyBookingsVM
import com.flexhelp.views.notification.NotificationsVM
import com.flexhelp.views.orderdetails.BookingOrderDetailVM
import com.flexhelp.views.otpverify.OtpVerificationVM
import com.flexhelp.views.paymentmethods.PaymentMethodVM
import com.flexhelp.views.payments.PaymentVM
import com.flexhelp.views.privacypolicies.PrivacyAndPolicyVM
import com.flexhelp.views.profile.ProfilesVM
import com.flexhelp.views.resetpasswords.ResetPasswordVM
import com.flexhelp.views.review.ReviewsVM
import com.flexhelp.views.service.ServicesVM
import com.flexhelp.views.servicedetails.ServiceDetailPageVM
import com.flexhelp.views.setting.SettingsVM
import com.flexhelp.views.signup.SignUpVM
import com.flexhelp.views.signuptwo.SignUpSecondVM
import com.flexhelp.views.subcat.SubCategoriesVM
import com.flexhelp.views.suggestions.SuggestionVM
import com.flexhelp.views.termsandconditions.TermAndConditionVM
import com.flexhelp.views.trackingsp.TrackingServiceProviderVM
import com.flexhelp.views.usertype.UserTypesVM
import com.flexhelp.views.walksthrough.WalkThroughVM
import java.lang.ref.WeakReference

class Factory(val context: WeakReference<Context>, private val childFragmentManager: FragmentManager) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return when {
            modelClass.isAssignableFrom(HomeVM::class.java) -> {
                HomeVM(this.context.get()!!) as T
            }
            modelClass.isAssignableFrom(FeedBacksVM::class.java) -> {
                FeedBacksVM(this.context.get()!!) as T
            }
            modelClass.isAssignableFrom(ChangePasswordVM::class.java) -> {
                ChangePasswordVM(this.context.get()!!) as T
            }
            modelClass.isAssignableFrom(SignUpSecondVM::class.java) -> {
                SignUpSecondVM(this.context.get()!!) as T
            }

            modelClass.isAssignableFrom(AddCardVM::class.java) -> {
                AddCardVM(this.context.get()!!) as T
            }

            modelClass.isAssignableFrom(ForgotPasswordsVM::class.java) -> {
                ForgotPasswordsVM(this.context.get()!!) as T
            }
            modelClass.isAssignableFrom(PrivacyAndPolicyVM::class.java) -> {
                PrivacyAndPolicyVM(this.context.get()!!) as T
            }
            modelClass.isAssignableFrom(AddressVM::class.java) -> {
                AddressVM(this.context.get()!!) as T
            }

            modelClass.isAssignableFrom(ContactUsVM::class.java) -> {
                ContactUsVM(this.context.get()!!) as T
            }

            modelClass.isAssignableFrom(AddAddressVM::class.java) -> {
                AddAddressVM(this.context.get()!!) as T
            }

            modelClass.isAssignableFrom(ResetPasswordVM::class.java) -> {
                ResetPasswordVM(this.context.get()!!) as T
            }

            modelClass.isAssignableFrom(MainVM::class.java) -> {
                MainVM(this.context.get()!!) as T
            }
            modelClass.isAssignableFrom(OtpVerificationVM::class.java) -> {
                OtpVerificationVM(this.context.get()!!) as T
            }
            modelClass.isAssignableFrom(SubCategoriesVM::class.java) -> {
                SubCategoriesVM(this.context.get()!!) as T
            }
            modelClass.isAssignableFrom(UserTypesVM::class.java) -> {
                UserTypesVM(this.context.get()!!) as T
            }
            modelClass.isAssignableFrom(WalkThroughVM::class.java) -> {
                WalkThroughVM(this.context.get()!!) as T
            }
            modelClass.isAssignableFrom(SignInVM::class.java) -> {
                SignInVM(this.context.get()!!) as T
            }


            modelClass.isAssignableFrom(SignUpVM::class.java) -> {
                SignUpVM(this.context.get()!!) as T
            }


            modelClass.isAssignableFrom(TermAndConditionVM::class.java) -> {
                TermAndConditionVM(this.context.get()!!) as T
            }

            modelClass.isAssignableFrom(ProfilesVM::class.java) -> {
                ProfilesVM(this.context.get()!!) as T
            }
            modelClass.isAssignableFrom(ServiceDetailPageVM::class.java) -> {
                ServiceDetailPageVM(this.context.get()!!, childFragmentManager) as T
            }
            modelClass.isAssignableFrom(ReviewsVM::class.java) -> {
                ReviewsVM(this.context.get()!!) as T
            }
            modelClass.isAssignableFrom(ServicesVM::class.java) -> {
                ServicesVM(this.context.get()!!) as T
            }
            modelClass.isAssignableFrom(AboutVM::class.java) -> {
                AboutVM(this.context.get()!!) as T
            }
            modelClass.isAssignableFrom((NotificationsVM::class.java)) -> {
                NotificationsVM(this.context.get()!!) as T
            }
            modelClass.isAssignableFrom((BookingVM::class.java)) -> {

                BookingVM(this.context.get()!!) as T
            }
            modelClass.isAssignableFrom((PaymentMethodVM::class.java)) -> {

                PaymentMethodVM(this.context.get()!!) as T
            }
            modelClass.isAssignableFrom((CongratulationVM::class.java)) -> {
                CongratulationVM(this.context.get()!!) as T
            }
            modelClass.isAssignableFrom((BookingOrderDetailVM::class.java)) -> {
                BookingOrderDetailVM(this.context.get()!!) as T
            }
            modelClass.isAssignableFrom((SettingsVM::class.java)) -> {
                SettingsVM(this.context.get()!!) as T
            }
            modelClass.isAssignableFrom((TrackingServiceProviderVM::class.java)) -> {
                TrackingServiceProviderVM(this.context.get()!!) as T
            }
            modelClass.isAssignableFrom((CancelBookingVM::class.java)) -> {
                CancelBookingVM(this.context.get()!!) as T
            }
            modelClass.isAssignableFrom((PaymentVM::class.java)) -> {
                PaymentVM(this.context.get()!!) as T
            }

            modelClass.isAssignableFrom((MyBookingsVM::class.java)) -> {
                MyBookingsVM(this.context.get()!!) as T
            }

            modelClass.isAssignableFrom((ChatVM::class.java)) -> {
                ChatVM(this.context.get()!!) as T
            }

            modelClass.isAssignableFrom((SuggestionVM::class.java)) -> {
                SuggestionVM(this.context.get()!!) as T
            }
            else -> throw IllegalArgumentException("Error")
        }
    }
}

