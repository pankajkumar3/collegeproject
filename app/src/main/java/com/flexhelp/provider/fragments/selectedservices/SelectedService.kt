package com.flexhelp.provider.fragments.selectedservices

import android.os.Bundle
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.flexhelp.R
import com.flexhelp.databinding.SelectedserviceBinding


class SelectedService : AppCompatActivity() {

    var viewmodel: SelectedServiceVM? = null
    var binding: SelectedserviceBinding? = null
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.selectedservice)
        val factory = SelectedServiceFactory(this,intent)
        val viewModel = ViewModelProvider(this, factory).get(SelectedServiceVM::class.java)
        binding!!.viewmodel = viewmodel
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)

    }
}