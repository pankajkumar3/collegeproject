package com.flexhelpprovider.fragments.pastBooking.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.flexhelp.R
import kotlinx.android.synthetic.main.adapter_add_services.view.*
import kotlinx.android.synthetic.main.adapter_past_booking.view.*

class AdapterPastBooking(val context: Context): RecyclerView.Adapter<AdapterPastBooking.MyViewModel>() {
    class MyViewModel(item: View): RecyclerView.ViewHolder(item) {
        val btnCompleted=item.btnComplete
        val btnCancelled=item.btnCancelled
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapterPastBooking.MyViewModel {
        val layout= LayoutInflater.from(context).inflate(R.layout.adapter_past_booking,parent,false)
        return MyViewModel(layout)
    }

    override fun getItemCount(): Int {
        return 4
    }

    override fun onBindViewHolder(holder: AdapterPastBooking.MyViewModel, position: Int) {

        if(position%2==0)
        {
            holder.btnCompleted.visibility=VISIBLE
            holder.btnCancelled.visibility=GONE
        }
        else
        {
            holder.btnCancelled.visibility=VISIBLE
            holder.btnCompleted.visibility=GONE
        }
    }
}