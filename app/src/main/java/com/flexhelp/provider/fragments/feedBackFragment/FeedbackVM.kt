package com.flexhelp.provider.fragments.feedBackFragment

import android.content.Context
import androidx.lifecycle.ViewModel
import com.flexhelp.interfaces.HomeInterface
import com.flexhelp.utils.CommonMethods
import com.flexhelp.provider.fragments.homeFragment.HomeFragment

class FeedbackVM(val context: Context):ViewModel() {
    companion object{
        var HomeInterface: HomeInterface?=null
    }
    fun click(value:String)
    {
        when(value)
        {
            "back"->
            {
                HomeInterface!!.backpress()
            }
            "submit"->
            {
                CommonMethods.loadFragment(context, HomeFragment())
            }
        }
    }
}