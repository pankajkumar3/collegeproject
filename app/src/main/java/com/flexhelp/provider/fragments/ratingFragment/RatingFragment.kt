package com.flexhelpprovider.fragments.ratingFragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.flexhelp.databinding.FragmentRatingBinding
import com.flexhelp.provider.fragments.ratingFragment.RatingVM


class RatingFragment : Fragment() {


    private lateinit var vm: RatingVM
    private lateinit var binding: FragmentRatingBinding


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentRatingBinding.inflate(inflater)
        return binding.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getBinds()
    }

    private fun getBinds()
    {
        vm = RatingVM(context!!)
        binding.viewModel= vm
    }
}