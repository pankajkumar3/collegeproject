package com.flexhelp.provider.fragments.accountfragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.flexhelp.R
import com.flexhelp.databinding.FragmentAccountBinding
import com.flexhelp.provider.fragments.accountfragment.AccountVM


class AccountFragment : Fragment() {

    private lateinit var vm: AccountVM
    private lateinit var binding:FragmentAccountBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding= FragmentAccountBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getBinds()
    }
    fun getBinds()
    {
        vm= AccountVM(requireContext())
        binding.viewModel=vm
    }
}