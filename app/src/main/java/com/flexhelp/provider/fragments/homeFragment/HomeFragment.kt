package com.flexhelp.provider.fragments.homeFragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import com.flexhelp.R
import com.flexhelp.databinding.FragmentHomeBinding
import kotlinx.android.synthetic.main.fragment_home.*


class HomeFragment : Fragment() {

    private lateinit var vm: HomeVM
    private lateinit var binding: FragmentHomeBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentHomeBinding.inflate(inflater)
        return binding.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getBinds()
    }

    private fun getBinds() {
        vm = HomeVM(requireContext())
        binding.viewModel = vm

        if (vm.boolean.get())
        {
            lsAreaSwitchhome.colorOff = ResourcesCompat.getColor(requireContext().resources,R.color.green,requireContext().theme)
            lsAreaSwitchhome.colorOn = ResourcesCompat.getColor(requireContext().resources,R.color.white,requireContext().theme)
            layoutLinear.visibility = View.INVISIBLE
        }
        else
        {
            lsAreaSwitchhome.colorOff = ResourcesCompat.getColor(requireContext().resources,R.color.white,requireContext().theme)
            lsAreaSwitchhome.colorOn = ResourcesCompat.getColor(requireContext().resources,R.color.gray,requireContext().theme)
            layoutLinear.visibility = View.VISIBLE
        }

        lsAreaSwitchhome.setOnToggledListener { _, isOn ->
            if (isOn)
            {
                lsAreaSwitchhome.colorOff = ResourcesCompat.getColor(requireContext().resources,R.color.green,requireContext().theme)
                lsAreaSwitchhome.colorOn = ResourcesCompat.getColor(requireContext().resources,R.color.white,requireContext().theme)
                layoutLinear.visibility = View.VISIBLE
            }
            else
            {
                lsAreaSwitchhome.colorOff = ResourcesCompat.getColor(requireContext().resources,R.color.white,requireContext().theme)
                lsAreaSwitchhome.colorOn = ResourcesCompat.getColor(requireContext().resources,R.color.gray,requireContext().theme)
                layoutLinear.visibility = View.GONE
            }
        }
        vm.click(vm.lastAction.value!!)
    }



}