package com.flexhelpprovider.fragments.orderFragment.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.flexhelp.R

class OrderAdapter1(val context: Context): RecyclerView.Adapter<OrderAdapter1.MyViewModel>() {
    class MyViewModel(item: View): RecyclerView.ViewHolder(item)
    {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrderAdapter1.MyViewModel {
        val layout= LayoutInflater.from(context).inflate(R.layout.adapter_order_detail_1,parent,false)
        return MyViewModel(layout)
    }

    override fun getItemCount(): Int
    {
        return 4
    }

    override fun onBindViewHolder(holder: OrderAdapter1.MyViewModel, position: Int) {
    }
}