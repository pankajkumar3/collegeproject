package com.flexhelpprovider.fragments.pastBooking

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.flexhelp.databinding.FragmentPastBookBinding
import com.flexhelp.provider.fragments.pastBooking.PastBookVM


class PastBookFragment : Fragment() {


    private lateinit var vm: PastBookVM
    private lateinit var binding: FragmentPastBookBinding


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentPastBookBinding.inflate(inflater)
        return binding.root

    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getBinds()
    }

    private fun getBinds() {
        vm = PastBookVM(context!!)
        binding.viewModel= vm
    }
}