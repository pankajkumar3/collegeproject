package com.flexhelp.provider.fragments.chPassword

import android.content.Context
import android.util.Log
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.flexhelp.R
import com.flexhelp.interfaces.HomeInterface
import com.flexhelp.networkcalls.RequestProcessor
import com.flexhelp.networkcalls.RetrofitApi
import com.flexhelp.networkcalls.RetrofitCall
import com.flexhelp.utils.CommonMethods
import com.flexhelp.utils.CommonMethods.isNetworkAvailable
import com.flexhelp.utils.CommonMethods.showToast
import com.flexhelp.views.changepasswords.changepasswordsresponse.ChangePasswordsResponse
import retrofit2.Response

class ChangeVM(val context: Context) : ViewModel() {


    var oldPassword = ObservableField("")
    var newPassword = ObservableField("")
    var confirmPassword = ObservableField("")
    var authorization=ObservableField("")


    companion object {
        var HomeInterface: HomeInterface? = null
    }

    fun clicks(value: String) {

        when (value) {
            "back" -> {
                HomeInterface?.backpress()
            }
            "update" -> {

                if (isNetworkAvailable(context)) {
                    if (validations()) {
                        changePasswordProvider()
                    }
                } else {

                    showToast(context, "please check your internet connection first")
                }
            }
        }
    }

    private fun validations(): Boolean {

        when {
            oldPassword.get()!!.isEmpty() -> {

                showToast(context, context.getString(R.string.pleaseEnterPassword))
                return false


            }
            newPassword.get()!!.isEmpty() -> {
                showToast(context, context.getString(R.string.pleaseEnterNewPassword))
                return false
            }

            !CommonMethods.isValidPassword(newPassword.get()) && newPassword.get()
                .toString().length < 8 -> {
                showToast(context, "please enter valid password ")

                return false
            }

            confirmPassword.get()!!.isEmpty() -> {
                showToast(context, context.getString(R.string.pleaseEnterConfirmPassword))
                return false
            }

            !newPassword.get().equals(confirmPassword.get()!!) || confirmPassword.get()!!
                .isEmpty() -> {
                showToast(context, context.getString(R.string.passwordNotMatch))
                return false
            }
            else -> {
                return true

            }

        }
    }


    private fun changePasswordProvider() {
        try {
            RetrofitCall().callService(
                context,
                true,
                "",
                object : RequestProcessor<Response<ChangePasswordsResponse>> {
                    override suspend fun sendRequest(retrofitApi: RetrofitApi): Response<ChangePasswordsResponse> {
                        return retrofitApi.changePassword(
                           // PreferenceKeys. PreferenceFile.retrieveAuthToken(context)!!,
                            authorization.get()!!,
                            oldPassword.get()!!,
                            newPassword.get()!!,
                            confirmPassword.get()!!
                        )
                    }

                    override fun onResponse(res: Response<ChangePasswordsResponse>) {
                        if (res.body()?.success == true) {
//                            val response = res.body()!!
                            showToast(context, "Password Changed Successfully")
                        }
                    }

                    override fun onException(message: String?) {
                        Log.e("userException", "====$message")
                    }
                })

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


}
