package com.flexhelp.provider.fragments.notificationFragment

import android.content.Context
import androidx.lifecycle.ViewModel
import com.flexhelp.interfaces.HomeInterface
import com.luxbubbleprovider.view.fragments.notification.adapter.NotificationAdapter

class NotificationVM(val context: Context):ViewModel()
{
    companion object
    {
        var HomeInterface: HomeInterface?=null
    }
    val adapter=NotificationAdapter(context)
    fun click(value:String)
    {
        when(value)
        {
            "back"->
            {
                HomeInterface!!.backpress()
            }
        }
    }
}