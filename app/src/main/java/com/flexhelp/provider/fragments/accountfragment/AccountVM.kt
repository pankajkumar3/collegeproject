package com.flexhelp.provider.fragments.accountfragment

import android.content.Context
import androidx.lifecycle.ViewModel
import com.flexhelp.interfaces.HomeInterface

class AccountVM(val context: Context) : ViewModel() {
    companion object
    {
        var HomeInterface: HomeInterface?=null
    }

    fun clicks(value: String) {
        when (value) {
            "back" -> {
                HomeInterface!!.backpress()
            }
            "next" -> {
                HomeInterface!!.backpress()
            }
        }
    }
}