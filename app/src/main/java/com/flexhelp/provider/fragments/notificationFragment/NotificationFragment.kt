package com.flexhelp.provider.fragments.notificationFragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.flexhelp.databinding.FragmentNotificationBinding
import com.flexhelp.provider.fragments.notificationFragment.NotificationVM


class NotificationFragment : Fragment() {

    private lateinit var vm: NotificationVM
    private lateinit var binding: FragmentNotificationBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View?
    {
        binding= FragmentNotificationBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getBinds()
    }
    fun getBinds()
    {
        vm= NotificationVM(
            requireContext()
        )
        binding.viewModel=vm
    }
}