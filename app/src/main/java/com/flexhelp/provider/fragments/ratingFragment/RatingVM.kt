package com.flexhelp.provider.fragments.ratingFragment

import android.content.Context
import androidx.lifecycle.ViewModel
import com.flexhelp.interfaces.HomeInterface
import com.flexhelpprovider.fragments.ratingFragment.adapter.RatingAdapter

class RatingVM(val context: Context):ViewModel() {
    val adapter=RatingAdapter(context)
    companion object
    {
        var HomeInterface: HomeInterface?=null
    }
    fun clicks(value:String)
    {
        when(value)
        {
            "back"->
            {
                HomeInterface!!.backpress()
            }
        }
    }
}