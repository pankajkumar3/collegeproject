package com.flexhelp.provider.fragments.orderDetail

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.flexhelp.databinding.FragmentOrderDetailBinding
import com.flexhelp.provider.fragments.orderDetail.OrderDetailVM

class OrderDetailFragment : Fragment() {
    private lateinit var vm: OrderDetailVM
    private lateinit var binding:FragmentOrderDetailBinding
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding= FragmentOrderDetailBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        var type = "ratenow"
        if (arguments != null)
        {
            type = requireArguments().getString("type","rate")
        }
        getBinds(type)
    }

    fun getBinds(type:String)
    {
       vm= OrderDetailVM(requireContext(),type)
        binding.viewModel=vm
    }
}