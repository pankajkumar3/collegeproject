package com.flexhelp.provider.fragments.orderDetail

import android.app.Activity
import android.content.Context
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.flexhelp.provider.fragments.cancelBooking.CancelBookingFragment
import com.flexhelp.provider.fragments.feedBackFragment.FeedbackFragment
import com.flexhelp.utils.CommonMethods
import com.flexhelp.provider.fragments.chatFragment.ChatFragment
import com.flexhelp.views.cancelbookings.CancelBooking

class OrderDetailVM(val context: Context, val type: String) : ViewModel() {
    val codeBool = ObservableBoolean(true)
    val codeBoolOrder = ObservableBoolean(true)
    val codeBoolStart = ObservableBoolean(false)
    val codeBoolcomplete = ObservableBoolean(true)
    val codeBoolDashedline = ObservableBoolean(false)
    val codeBoolCompletegreen = ObservableBoolean(false)
    val codeBoolRate = ObservableBoolean(false)
    val codeBoolongoing = ObservableBoolean(false)
    val codeBoolCancel = ObservableBoolean(true)
    val codeBoollayoutrate = ObservableBoolean(true)
    val codeBoollayout = ObservableBoolean(true)

    val codeBoolonrequested = ObservableBoolean(true)
    val onGoing = ObservableField("On going")

    init {
        if (type == "rate") {
            codeBoolRate.set(true)
            codeBoolOrder.set(false)
            codeBoolStart.set(false)
            codeBool.set(false)
            codeBoollayoutrate.set(false)
            codeBoollayout.set(false)
            codeBoolcomplete.set(false)
            codeBoolDashedline.set(true)
            codeBoolCancel.set(false)
            codeBoolongoing.set(false)
            codeBoolonrequested.set(false)
        }
    }

    fun click(value: String) {
        when (value) {
            "back" -> {
                (context as Activity).onBackPressed()
            }
            "cancel" -> {

                CommonMethods.loadFragment(context, CancelBookingFragment())
               // (context as Activity).onBackPressed()

            }
            "accept" -> {
                codeBoolOrder.set(false)
                codeBool.set(false)
                codeBoolStart.set(true)
                codeBoolCancel.set(true)
                codeBoolDashedline.set(false)
                codeBoolonrequested.set(false)
            }
            "chat" -> {
                CommonMethods.loadFragment(context, ChatFragment())
            }
            "start" -> {
                codeBoolOrder.set(false)
                codeBoolStart.set(false)
                codeBool.set(true)
                codeBoolcomplete.set(true)
                codeBoolCancel.set(true)
                codeBoolDashedline.set(false)
                codeBoolCompletegreen.set(true)
                codeBoolongoing.set(true)
                codeBoolonrequested.set(false)
            }
            "complete" -> {
                codeBoolDashedline.set(true)
                codeBoolRate.set(true)
                codeBoolOrder.set(false)
                codeBoolStart.set(false)
                codeBool.set(false)
                codeBoolcomplete.set(false)
                codeBoolCancel.set(true)
                codeBoollayout.set(false)
                codeBoollayoutrate.set(false)
                codeBoolCompletegreen.set(false)
                codeBoolCancel.set(false)
                codeBoolonrequested.set(false)
            }
            "rate" -> {
                CommonMethods.loadFragment(context, FeedbackFragment())
            }
        }
    }
}