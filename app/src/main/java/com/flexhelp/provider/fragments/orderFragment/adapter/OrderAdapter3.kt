package com.flexhelp.provider.fragments.orderFragment.adapter

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.flexhelp.R
import com.flexhelp.utils.CommonMethods
import com.flexhelp.provider.fragments.orderDetail.OrderDetailFragment

class OrderAdapter3 (val context: Context): RecyclerView.Adapter<OrderAdapter3.MyViewModel>() {
    val rate = "rate"

    class MyViewModel(item: View) : RecyclerView.ViewHolder(item) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewModel {
        val layout =
            LayoutInflater.from(context).inflate(R.layout.adapter_order_completed, parent, false)
        return MyViewModel(layout)
    }

    override fun getItemCount(): Int {
        return 4
    }

    override fun onBindViewHolder(holder: MyViewModel, position: Int) {
        holder.itemView.setOnClickListener {
            val fragment = OrderDetailFragment()
            val bundle = Bundle()
            bundle.putString("type",rate)
            Log.e("rate", bundle.putString("type",rate).toString())
            fragment.arguments = bundle
            CommonMethods.loadFragment(context, fragment)
        }
    }
}