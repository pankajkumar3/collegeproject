package com.flexhelp.provider.fragments.manageServices

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.flexhelp.databinding.FragmentManageServicesBinding
import com.flexhelp.provider.fragments.manageServices.ManageServicesVM

class ManageServicesFragment : Fragment() {
    private lateinit var vm: ManageServicesVM
    private lateinit var binding: FragmentManageServicesBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =  FragmentManageServicesBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getBinds()
    }

    private fun getBinds() {
        vm = ManageServicesVM(requireContext())
        binding.viewModel = vm
    }
}