package com.luxbubbleprovider.view.fragments.notification.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.flexhelp.R
import kotlinx.android.synthetic.main.adapter_notification_items_list.view.*

class NotificationAdapter(val context: Context): RecyclerView.Adapter<NotificationAdapter.myholder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotificationAdapter.myholder
    {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.adapter_notification_items_list, parent, false)
        return myholder(v)
    }
    override fun getItemCount(): Int
    {
        return 8
    }
    override fun onBindViewHolder(holder: NotificationAdapter.myholder, position: Int)
    {
        if (position < 2)
        {

            if (position == 0)
            {
                holder.tvNotiText.visibility = View.VISIBLE
                holder.tvNotiText.text = "Recent"
            }
            holder.llNoti.setBackgroundResource(R.drawable.button_round_blue_light)

            holder.ivNotiImage.setImageResource(R.drawable.ic_recent)
        }

        else
        {
            if (position == 2)
            {
                holder.tvNotiText.visibility = View.VISIBLE
                holder.tvNotiText.text = "Older notification"
            }
            holder.ivNotiImage.setImageResource(R.drawable.ic_older)
            holder.llNoti.setBackgroundResource(R.drawable.button_round_blue_light)
        }
    }

    class myholder(itemView: View):RecyclerView.ViewHolder(itemView){
        val tvNotiText=itemView.tvNotiText
        val llNoti=itemView.llNoti
        val ivNotiImage=itemView.ivNotiImage
        val tbNotiMsg=itemView.tbNotiMsg
    }
}