package com.flexhelp.provider.fragments.manageServices.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.flexhelp.R

class ManageServicesAdapter(val context: Context): RecyclerView.Adapter<ManageServicesAdapter.MyViewModel>() {
    class MyViewModel(item: View): RecyclerView.ViewHolder(item) {
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewModel {
        val layout= LayoutInflater.from(context).inflate(R.layout.adapter_manage_services_row,parent,false)
        return MyViewModel(layout)
    }

    override fun getItemCount(): Int {
        return 8
    }

    override fun onBindViewHolder(holder: MyViewModel, position: Int) {
    }
}