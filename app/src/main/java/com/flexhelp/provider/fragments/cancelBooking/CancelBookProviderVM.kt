package com.flexhelp.provider.fragments.cancelBooking

import android.content.Context
import androidx.lifecycle.ViewModel
import com.flexhelp.MainActivity
import com.flexhelp.interfaces.HomeInterface
import com.flexhelp.utils.CommonMethods
import com.flexhelp.provider.fragments.homeFragment.HomeFragment
import com.flexhelp.views.cancelbookings.CancelBooking

class CancelBookProviderVM(val context: Context):ViewModel() {


    companion object
    {
        var HomeInterface: HomeInterface?=null
    }

    fun clicks(value: String) {

        when (value) {
            "back" -> {
               HomeInterface?.backpress()
            }
            "submit" -> {
                CommonMethods.loadFragment(context, HomeFragment())}
        }
    }
}

