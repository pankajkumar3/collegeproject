package com.flexhelpprovider.fragments.orderFragment

import android.content.Context
import androidx.lifecycle.ViewModel
import com.flexhelp.interfaces.HomeInterface
import com.flexhelp.provider.fragments.orderFragment.OrderFragment
import com.flexhelp.provider.fragments.orderFragment.adapter.OrderAdapter
import com.flexhelp.provider.fragments.orderFragment.adapter.OrderAdapter2
import com.flexhelp.provider.fragments.orderFragment.adapter.OrderAdapter3
import com.flexhelpprovider.fragments.orderFragment.adapter.*
import com.flexhelp.provider.fragments.pastBooking.PastBookVM
import com.flexhelp.utils.CommonMethods

class OrderVM(val context: Context):ViewModel()
{

    companion object
    {
        var HomeInterface: HomeInterface?=null
    }

    val adapterCurrent= OrderAdapter(context)
    val adapterCancelled= OrderAdapter2(context)
    val adapterCompleted= OrderAdapter3(context)
    val adapterPending= OrderAdapter1(context)
    fun clicks(value:String)
    {
        when(value)
        {
            "back"->
            {
                PastBookVM.HomeInterface!!.backpress()
            }
            "next"->
            {
                CommonMethods.loadFragment(context, OrderFragment())
            }
        }
    }
}