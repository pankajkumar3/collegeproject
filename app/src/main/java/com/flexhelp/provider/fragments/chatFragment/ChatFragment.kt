package com.flexhelp.provider.fragments.chatFragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.flexhelp.MainActivity
import com.flexhelp.databinding.FragmentChatBinding
import com.flexhelp.provider.fragments.chatFragment.ChatVM


class ChatFragment : Fragment() {
    private lateinit var vm: ChatVM
    private lateinit var binding: FragmentChatBinding
    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?

    ): View?

    {
        binding= FragmentChatBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        getBinds()
    }
    fun getBinds()
    {
        vm= ChatVM(requireContext())
        binding.viewModel=vm
    }

    override fun onResume() {
        super.onResume()
        MainActivity.HomeInterface?.isVisible(isVisible = false)


    }

}