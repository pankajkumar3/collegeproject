package com.flexhelp.provider.fragments.pastBooking

import android.content.Context
import androidx.lifecycle.ViewModel
import com.flexhelp.interfaces.HomeInterface
import com.flexhelpprovider.fragments.pastBooking.adapter.AdapterPastBooking

class PastBookVM(val context: Context):ViewModel() {
    val adapter=AdapterPastBooking(context)
    companion object
    {
    var HomeInterface: HomeInterface?=null
    }
    fun clicks(value:String)
    {
        when(value)
        {
            "back"->
            {
                HomeInterface!!.backpress()
            }
        }
    }
}