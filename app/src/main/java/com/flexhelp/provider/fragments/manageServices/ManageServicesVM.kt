package com.flexhelp.provider.fragments.manageServices

import android.content.Context
import android.content.Intent
import androidx.lifecycle.ViewModel
import com.flexhelp.interfaces.HomeInterface
import com.flexhelp.provider.fragments.manageServices.adapter.ManageServicesAdapter
import com.flexhelp.provider.activities.addServicesList.AddServicesListActivity


class ManageServicesVM(val context: Context):ViewModel() {
    companion object{
        var HomeInterface: HomeInterface?=null
    }
    val adapter= ManageServicesAdapter(context)
    fun clicks(value:String) {
        when (value)
        {
            "back" ->
            {
                HomeInterface!!.backpress()
            }
            "add"->
            {
                context.startActivity(Intent(context, AddServicesListActivity::class.java).putExtra("manage","btnSubmit"))
            }
        }
    }
}