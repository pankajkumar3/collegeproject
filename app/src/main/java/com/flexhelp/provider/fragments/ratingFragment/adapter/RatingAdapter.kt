package com.flexhelpprovider.fragments.ratingFragment.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.flexhelp.R

class RatingAdapter(val context: Context): RecyclerView.Adapter<RatingAdapter.MyViewModel>() {
    class MyViewModel(item: View) : RecyclerView.ViewHolder(item) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RatingAdapter.MyViewModel {
        val layout =
            LayoutInflater.from(context).inflate(R.layout.adapter_earning_row, parent, false)
        return MyViewModel(layout)
    }

    override fun getItemCount(): Int {
        return 8
    }

    override fun onBindViewHolder(holder: RatingAdapter.MyViewModel, position: Int) {
    }
}