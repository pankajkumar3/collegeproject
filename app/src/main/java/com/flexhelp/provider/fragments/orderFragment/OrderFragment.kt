package com.flexhelp.provider.fragments.orderFragment

import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.flexhelp.R
import com.flexhelp.databinding.FragmentOrderBinding
import com.flexhelp.provider.fragments.orderFragment.adapter.OrderAdapter
import com.flexhelpprovider.fragments.orderFragment.adapter.OrderAdapter1
import com.flexhelp.provider.fragments.orderFragment.adapter.OrderAdapter2
import com.flexhelp.provider.fragments.orderFragment.adapter.OrderAdapter3
import com.flexhelpprovider.fragments.orderFragment.OrderVM
import kotlinx.android.synthetic.main.fragment_order.*

class OrderFragment : Fragment() {
    private lateinit var vm: OrderVM
    private lateinit var binding: FragmentOrderBinding
    lateinit var homeAdapter: OrderAdapter
    lateinit var homeAdapter1:OrderAdapter1
    lateinit var homeAdapter2: OrderAdapter2
    lateinit var homeAdapter3: OrderAdapter3
    var type=""

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View?
    {
        binding = FragmentOrderBinding.inflate(inflater)
        return binding.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getBinds(type)
        tabs()
    }
    private fun getBinds(type:String) {
        vm = OrderVM(requireContext())
        binding.viewModel = vm
    }
    fun tabs()
    {
        recyclerCurrent!!.layoutManager = LinearLayoutManager(activity)
        homeAdapter = OrderAdapter(requireActivity())
        currentTab.setBackgroundResource(R.drawable.txt_back_skyblue)
        currentTab.setTextColor(Color.WHITE)
        recyclerCurrent?.adapter = homeAdapter

        topHeading.visibility=View.VISIBLE

        recyclerCancelled.visibility=View.GONE
        recyclerCompleted.visibility=View.GONE
        recyclerPending.visibility=View.GONE

        currentTab.setOnClickListener {
            recyclerCurrent!!.layoutManager = LinearLayoutManager(activity)
            homeAdapter = OrderAdapter(requireActivity())
            recyclerCurrent?.adapter = homeAdapter

            currentTab.setBackgroundResource(R.drawable.txt_back_skyblue)
            currentTab.setTextColor(Color.WHITE)
            recyclerCurrent.visibility=View.VISIBLE

            topHeadingPast.visibility=View.GONE
            topHeading.visibility=View.VISIBLE

            recyclerCancelled.visibility=View.GONE
            recyclerCompleted.visibility=View.GONE
            recyclerPending.visibility=View.GONE

            cancelledTab.setBackgroundResource(R.drawable.txt_back_whiteround)
            cancelledTab.setTextColor(Color.BLACK)

            pendingTab.setBackgroundResource(R.drawable.txt_back_whiteround)
            pendingTab.setTextColor(Color.BLACK)

            completedTab.setBackgroundResource(R.drawable.txt_back_whiteround)
            completedTab.setTextColor(Color.BLACK)
//            btnChat.visibility= View.VISIBLE
//            btnStart.visibility= View.VISIBLE
//            btnCompleted.visibility= View.GONE
//            btnCancelled.visibility= View.GONE
//            btnAccept.visibility= View.GONE
//            btnCancel.visibility= View.GONE

        }

        pendingTab.setOnClickListener {
            recyclerPending!!.layoutManager = LinearLayoutManager(activity)
            homeAdapter1 = OrderAdapter1(requireActivity())
            recyclerPending?.adapter = homeAdapter1

            pendingTab.setBackgroundResource(R.drawable.txt_back_skyblue)
            pendingTab.setTextColor(Color.WHITE)

            currentTab.setBackgroundResource(R.drawable.txt_back_whiteround)
            currentTab.setTextColor(Color.BLACK)
            recyclerPending.visibility=View.VISIBLE

            topHeadingPast.visibility=View.GONE
            topHeading.visibility=View.VISIBLE

            recyclerCancelled.visibility=View.GONE
            recyclerCompleted.visibility=View.GONE
            recyclerCurrent.visibility=View.GONE

            cancelledTab.setBackgroundResource(R.drawable.txt_back_whiteround)
            cancelledTab.setTextColor(Color.BLACK)

            completedTab.setBackgroundResource(R.drawable.txt_back_whiteround)
            completedTab.setTextColor(Color.BLACK)

//            btnChat.visibility= View.GONE
//            btnStart.visibility= View.GONE
//
//            btnAccept.visibility= View.VISIBLE
//            btnCancelled.visibility= View.GONE
//            btnCompleted.visibility=View.GONE
//            btnCancel.visibility= View.VISIBLE
        }

        cancelledTab.setOnClickListener {
            recyclerCancelled!!.layoutManager = LinearLayoutManager(activity)
            homeAdapter2 = OrderAdapter2(requireActivity())
            recyclerCancelled?.adapter = homeAdapter2

            cancelledTab.setBackgroundResource(R.drawable.txt_back_skyblue)
            cancelledTab.setTextColor(Color.WHITE)

            currentTab.setBackgroundResource(R.drawable.txt_back_whiteround)
            currentTab.setTextColor(Color.BLACK)
            recyclerCancelled.visibility=View.VISIBLE

            topHeadingPast.visibility=View.GONE
            topHeading.visibility=View.VISIBLE

            recyclerPending.visibility=View.GONE
            recyclerCompleted.visibility=View.GONE
            recyclerCurrent.visibility=View.GONE

            completedTab.setBackgroundResource(R.drawable.txt_back_whiteround)
            completedTab.setTextColor(Color.BLACK)

            pendingTab.setBackgroundResource(R.drawable.txt_back_whiteround)
            pendingTab.setTextColor(Color.BLACK)

//            btnChat.visibility= View.GONE
//            btnStart.visibility= View.GONE
//            btnCompleted.visibility= View.GONE
//            btnCancelled.visibility= View.VISIBLE
//            btnAccept.visibility= View.GONE
//            btnCancel.visibility= View.GONE
        }

        completedTab.setOnClickListener{
            recyclerCompleted!!.layoutManager = LinearLayoutManager(activity)
            homeAdapter3 = OrderAdapter3(requireActivity())
            recyclerCompleted?.adapter = homeAdapter3

            completedTab.setBackgroundResource(R.drawable.txt_back_skyblue)
            completedTab.setTextColor(Color.WHITE)
            topHeadingPast.visibility=View.VISIBLE
            topHeading.visibility=View.GONE

            currentTab.setBackgroundResource(R.drawable.txt_back_whiteround)
            currentTab.setTextColor(Color.BLACK)
            recyclerCompleted.visibility=View.VISIBLE

            recyclerCancelled.visibility=View.GONE
            recyclerPending.visibility=View.GONE
            recyclerCurrent.visibility=View.GONE

            pendingTab.setBackgroundResource(R.drawable.txt_back_whiteround)
            pendingTab.setTextColor(Color.BLACK)

            cancelledTab.setBackgroundResource(R.drawable.txt_back_whiteround)
            cancelledTab.setTextColor(Color.BLACK)

//            btnChat.visibility= View.GONE
//            btnStart.visibility= View.GONE
//            btnAccept.visibility= View.GONE
//            btnCancel.visibility= View.GONE
//            btnCancelled.visibility=View.GONE
//            btnCompleted.visibility= View.VISIBLE
        }
    }
}

