package com.flexhelp.provider.fragments.settingsFragment

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.ViewModel
import com.flexhelp.MainActivity
import com.flexhelp.interfaces.HomeInterface
import com.flexhelp.networkcalls.RequestProcessor
import com.flexhelp.networkcalls.RetrofitApi
import com.flexhelp.networkcalls.RetrofitCall
import com.flexhelp.preference.PreferenceFile
import com.flexhelp.provider.activities.loginActivity.LoginActivity
import com.flexhelp.provider.activities.signupActivity.signuproviderresponse.SignupProviderResponse
import com.flexhelp.provider.fragments.chPassword.ChangePassFragment
import com.flexhelp.provider.fragments.fragmentSuggestion.SuggestionFragment
import com.flexhelp.utils.CommonMethods
import com.flexhelp.utils.CommonMethods.showToast
import com.flexhelpprovider.fragments.contactFragment.ContactFragment
import com.flexhelpprovider.views.termsCondition.TermsConditionActivity
import retrofit2.Response

class SettingsVM(val context: Context) : ViewModel(), HomeInterface {
    companion object {
        var HomeInterface: HomeInterface? = null
    }

    fun clicks(value: String) {
        when (value) {
            "termcondition" -> {
                (context as Activity).startActivity(
                    Intent(
                        context,
                        TermsConditionActivity::class.java
                    )
                )
            }
            "chgPass" -> {
                CommonMethods.loadFragment(context, ChangePassFragment())
            }
            "contact" -> {
                CommonMethods.loadFragment(context, ContactFragment())
            }
            "suggestion" -> {
                CommonMethods.loadFragment(
                    context, SuggestionFragment()
                )
            }
            "back" -> {
                HomeInterface!!.backpress()
            }
            "logout" -> {

               // logoutApi()
                showLogoutPopup()
            }
        }
    }


/*
    private fun logoutApi() {
        val token = PreferenceFile.retrieveAuthToken(context)
        try {
            RetrofitCall().callService(
                context,
                true,
                token!!,
                object : RequestProcessor<Response<SignupProviderResponse>> {
                    override suspend fun sendRequest(retrofitApi: RetrofitApi): Response<SignupProviderResponse> {
                        return retrofitApi.logout()
                    }

                    override fun onResponse(res: Response<SignupProviderResponse>) {
                        if (res.isSuccessful) {
                            val response = res.body()!!
                            PreferenceFile.clearPreference(context)
                            showToast(context, response.message)
                            //   (context as MainActivity).navController!!.navigate(R.id.action_more_to_loginEmail)
                        }
                    }

                    override fun onException(message: String?) {
                        Log.e("userException", "====$message")
                    }
                })

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
*/

    private fun showLogoutPopup() {
        AlertDialog.Builder(context).setTitle("Logout")
            .setMessage("Are you sure you want to logout ?")
            .setPositiveButton("Logout") { _, _ ->
               // logoutApi()
                (context).startActivity(
                    Intent(context, LoginActivity::class.java).putExtra("type",
                        "login"
                    )
                )
                (context as Activity).finishAffinity()
            }
            .setNeutralButton("Close", null)
            .show()
    }

    override fun backpress() {
        //onBackPressed()
    }

    override fun isVisible(isVisible: Boolean) {

    }

    override fun className(string: String) {
        TODO("Not yet implemented")
    }

    override fun homeToggle() {

    }

    override fun Navigation(boolean: Boolean) {
        TODO("Not yet implemented")
    }
}

