package com.flexhelp.provider.fragments.cancelBooking

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.flexhelp.databinding.FragmentCancelBookingBinding


class CancelBookingFragment:Fragment()
{
    private lateinit var vm: CancelBookProviderVM
    private lateinit var binding: FragmentCancelBookingBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View?
    {
        binding= FragmentCancelBookingBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getBinds()
    }
    fun getBinds()
    {
        vm= CancelBookProviderVM(requireContext())
        binding.viewModel=vm
    }
}