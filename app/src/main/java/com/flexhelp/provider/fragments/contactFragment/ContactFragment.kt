package com.flexhelpprovider.fragments.contactFragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.flexhelp.databinding.FragmentContactBinding
import com.flexhelp.provider.fragments.contactFragment.ContactVM


class ContactFragment : Fragment() {

    private lateinit var vm: ContactVM
    private lateinit var binding: FragmentContactBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentContactBinding.inflate(inflater)
        return binding.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getBinds()
    }

    private fun getBinds() {
        vm = ContactVM(context!!)
        binding.viewModel= vm
    }
}