package com.flexhelp.provider.fragments.chPassword

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.flexhelp.databinding.FragmentChangePassBinding


class ChangePassFragment : Fragment() {

    private lateinit var vm: ChangeVM
    private lateinit var binding: FragmentChangePassBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentChangePassBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getBinds()
    }

    fun getBinds() {
        vm = ChangeVM(requireContext())
        binding.viewModel = vm
    }
}