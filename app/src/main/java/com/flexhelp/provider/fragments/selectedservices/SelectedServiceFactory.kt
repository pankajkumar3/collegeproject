package com.flexhelp.provider.fragments.selectedservices

import android.content.Intent
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.flexhelp.views.profile.ProfilesVM

class SelectedServiceFactory(
    private val context: SelectedService,
    val intent: Intent
) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(SelectedServiceVM::class.java)
        ) {
            return SelectedServiceVM(context) as T
        }

        throw IllegalArgumentException("Error")
    }


}