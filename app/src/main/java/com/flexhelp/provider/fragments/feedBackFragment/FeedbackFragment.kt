package com.flexhelp.provider.fragments.feedBackFragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.flexhelp.databinding.FragmentFeedbackBinding
import com.flexhelp.provider.fragments.feedBackFragment.FeedbackVM

class FeedbackFragment : Fragment() {

    private lateinit var vm: FeedbackVM
    private lateinit var binding: FragmentFeedbackBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentFeedbackBinding.inflate(inflater)
        return binding.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getBinds()
    }

    private fun getBinds() {
        vm = FeedbackVM(requireContext())
        binding.viewModel= vm
    }
}