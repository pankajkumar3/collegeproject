package com.flexhelp.provider.fragments.fragmentSuggestion

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment
import com.flexhelp.databinding.FragmentSuggestionBinding
import kotlinx.android.synthetic.main.fragment_suggestion.*

class SuggestionFragment : Fragment() {

    private lateinit var vm: SuggestionVM
    private lateinit var binding: FragmentSuggestionBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentSuggestionBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getBinds()
        hideKeyboardFragment(this, tvSubmit)
    }

    fun getBinds() {
        vm = SuggestionVM(requireContext())
        binding.viewModel = vm
    }

    private fun hideKeyboardFragment(fragment: Fragment, view: View) {
        val imm: InputMethodManager =
            view.context?.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
        view.clearFocus()
    }
}