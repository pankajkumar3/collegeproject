package com.flexhelp.provider.fragments.selectedservices

import android.app.PendingIntent.getActivity
import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.View
import androidx.core.content.ContextCompat.startActivity
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.ViewModel
import com.flexhelp.R
import com.flexhelp.provider.activities.questionare.QuestionaireActivity
import com.flexhelp.recyclerAdapter.RecyclerAdapter
import com.flexhelp.recyclerAdapter.SubCatModel


class SelectedServiceVM(context: Context):ViewModel() {


    val adapter by lazy { RecyclerAdapter<SubCatModel>(R.layout.sub_cateroy_adapter) }


    init {
        var list = listOf(

            SubCatModel("Garden Cleaner",false, ResourcesCompat.getDrawable(context.resources,R.drawable.man_collects_leaves_cleans_park,context.theme)!!),
            SubCatModel("kitchen Cleaner",false, ResourcesCompat.getDrawable(context.resources,R.drawable.housekeeping_concept_with_young_woman,context.theme)!!),
            SubCatModel("Washing Cleaner",false, ResourcesCompat.getDrawable(context.resources,R.drawable.woman_gloves_with_rag_doing_cleaning_bathroom_clea,context.theme)!!),
            SubCatModel("Shop Cleaner",false, ResourcesCompat.getDrawable(context.resources,R.drawable.female_barista_cleaning_surface,context.theme)!!),
            SubCatModel("Building Cleaner",false, ResourcesCompat.getDrawable(context.resources,R.drawable.professional_industrial_cleaner_protective_uniform,context.theme)!!),
            SubCatModel("Snow Cleaning",false, ResourcesCompat.getDrawable(context.resources,R.drawable.man_shoveling_snow_backyard_after_blizzard_close_u,context.theme)!!)



        )

        adapter.addItems(
            list
        )


        adapter.setOnItemClick(object : RecyclerAdapter.OnItemClick {
            override fun onClick(view: View, position: Int, type: String) {
                when (type) {
                    "imageClick" -> {
                        list.map {
                            it.isSelected = it.adapterPosition == position
                            it
                        }
                        adapter.notifyDataSetChanged()

                      //  (context as MainActivity).navController.navigate(R.id.serviceDetailPage)
                    }
                }
                Log.e("adapterClick", "onClick: $position")
            }
        })

    }



    fun onClicks(type: String) {
        when (type) {

            "back" -> {



            }
            "Next" -> {



               // CommonMethods.loadFragment(this, CancelBookingFragment())

                (this as SelectedService).startActivity(Intent(this, QuestionaireActivity::class.java))

                // (context as MainActivity).navController.navigate(R.id.action_profiles_to_home2)
               /* if (validations()) {
                    //  updateProfile()
                    (context as MainActivity).navController.navigate(R.id.action_profiles_to_home2)


                }*/
            }

        }
    }


}