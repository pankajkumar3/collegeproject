package com.flexhelp.provider.fragments.homeFragment

import android.content.Context
import android.os.Bundle
import androidx.databinding.ObservableBoolean
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.flexhelp.interfaces.HomeInterface
import com.flexhelp.provider.fragments.chatFragment.ChatFragment
import com.flexhelp.provider.fragments.notificationFragment.NotificationFragment
import com.flexhelp.provider.fragments.orderDetail.OrderDetailFragment
import com.flexhelp.utils.CommonMethods

class HomeVM(val context: Context) : ViewModel() {

    var boolean = ObservableBoolean(false)
    var accept = ObservableBoolean(true)
    var booking = "requested"
    var lastAction = MutableLiveData<String>("")
    var accepted = ObservableBoolean(false)
    var acceptednext = ObservableBoolean(false)
    var acceptednextfurther = ObservableBoolean(false)
    var acceptednextchat = ObservableBoolean(false)
    var acceptedcircle = ObservableBoolean(false)
    var cancelOrder = ObservableBoolean(true)

    companion object {
        var HomeInterface: HomeInterface? = null
    }

    fun click(value: String) {
        lastAction.value = value
        when (value) {
            "drawer" -> {
                HomeInterface!!.homeToggle()
            }
            "noti" -> {
                CommonMethods.loadFragment(
                    context,
                    NotificationFragment()
                )
            }
            "accept" -> {
                accept.set(false)
                accepted.set(true)
            }
            "start" -> {
                accept.set(false)
                accepted.set(false)
                acceptednext.set(true)
                acceptednextchat.set(true)
                acceptedcircle.set(true)
            }
            "way" -> {
                accept.set(false)
                accepted.set(false)
                acceptednext.set(false)
                acceptednextchat.set(true)
                acceptedcircle.set(true)
                acceptednextfurther.set(true)
            }
            "chat" -> {
                CommonMethods.loadFragment(context, ChatFragment())
            }
            "next" -> {
                val fragment = OrderDetailFragment()
                val bundle = Bundle()
                bundle.putString("type", booking)
                fragment.arguments = bundle
                CommonMethods.loadFragment(context, fragment)
            }

        }

    }
}
