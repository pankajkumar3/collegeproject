package com.flexhelpprovider.fragments.chatFragment.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.flexhelp.R
import kotlinx.android.synthetic.main.adapter_chat_layout.view.*

class ChatAdapter(val context: Context): RecyclerView.Adapter<ChatAdapter.MyViewModel>() {
    class MyViewModel(item: View): RecyclerView.ViewHolder(item) {

        var llReceive = itemView.llReceive
        var tvTime = itemView.tvTime
        var llSent = itemView.llSent
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChatAdapter.MyViewModel {
        val layout= LayoutInflater.from(context).inflate(R.layout.adapter_chat_layout,parent,false)
        return MyViewModel(layout)
    }

    override fun getItemCount(): Int {
        return 2
    }

    override fun onBindViewHolder(holder: ChatAdapter.MyViewModel, position: Int) {

        when(position)
        {
            0->
            {
                holder.llReceive.visibility = View.GONE
                holder.llSent.visibility = View.VISIBLE
            }
            1 -> {
                holder.tvTime.visibility = View.GONE
                holder.llReceive.visibility = View.VISIBLE
                holder.llSent.visibility = View.GONE
            }
        }
    }
}