package com.flexhelp.provider.fragments.chatFragment

import android.content.Context
import android.content.Intent
import android.widget.ImageView
import android.widget.PopupMenu
import androidx.lifecycle.ViewModel
import com.flexhelp.R
import com.flexhelp.interfaces.HomeInterface
import com.flexhelp.provider.fragments.cancelBooking.CancelBookingFragment
import com.flexhelpprovider.fragments.chatFragment.adapter.ChatAdapter
import com.flexhelpprovider.fragments.contactFragment.ContactFragment
import com.flexhelp.provider.activities.bankAccount.BankAccountActivity
import com.flexhelp.utils.CommonMethods

class ChatVM(val context: Context):ViewModel() {
    companion object{
        var HomeInterface: HomeInterface?=null
    }
    val chatAdapter=ChatAdapter(context)
    fun clicks(value:String)
    {
        when(value)
        {
            "back"->
            {
                HomeInterface!!.backpress()
               // chatOptions(imageView = ImageView(context))

                    }
            "next"->
            {
                context.startActivity(Intent(context, BankAccountActivity::class.java))
            }
            "contact"->
            {
                CommonMethods.loadFragment(context,ContactFragment())
            }
        }
    }
    fun chatOptions(imageView: ImageView)
    {
        val popupMenu = PopupMenu(context, imageView)
        popupMenu.menuInflater.inflate(R.menu.chat_options, popupMenu.menu)
        popupMenu.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.cancel -> {
                    CommonMethods.loadFragment(context, CancelBookingFragment())
                }
                R.id.report -> {

                }
            }
            true
        }

       popupMenu.show()

    }
}