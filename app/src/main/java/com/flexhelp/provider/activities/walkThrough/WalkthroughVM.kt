package com.flexhelpprovider.views.walkThrough

import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import androidx.viewpager.widget.ViewPager
import com.flexhelp.R
import com.flexhelp.databinding.ActivityWalkthroughBinding
import com.flexhelp.provider.activities.loginActivity.LoginActivity


class WalkthroughVM(val context: Context, walkThroughBinding: ActivityWalkthroughBinding):ViewModel() {
    var mContext = context

    var codeBooleanImg1 = ObservableBoolean(true)
    var codeBooleanImg2 = ObservableBoolean(true)
    var codeBooleanImg3 = ObservableBoolean(true)
    var btName = ObservableField("Next")
    var arrayList = arrayListOf(
        R.layout.walkthrough_1,
        R.layout.walkthrough_2,
        R.layout.walkthrough_3
    )
    var wtViewPager =
        WalkThroughVP(
            context,
            arrayList
        )
    var pagerListener = walkThroughBinding.vpWalkThrough.addOnPageChangeListener(object :
        ViewPager.OnPageChangeListener {
        override fun onPageScrolled(
            position: Int,
            positionOffset: Float,
            positionOffsetPixels: Int
        ) {
            when (position) {
                1 -> {

                    btName.set("Next")
//                    codeBooleanImg2.set(true)
//                    codeBooleanImg3.set(true)
                }
                2 -> {

                    btName.set("Next")
//                    codeBooleanImg1.set(false)
//                    codeBooleanImg2.set(false)
//                    codeBooleanImg3.set(true)
                }
                3 -> {

                    btName.set("Next")
//                    codeBooleanImg1.set(false)
//                    codeBooleanImg2.set(true)
//                    codeBooleanImg3.set(false)
                }


            }
        }

        override fun onPageSelected(position: Int) {}
        override fun onPageScrollStateChanged(state: Int) {}
    })

    init {
        pagerListener
    }

    fun pagerSkip() {
        (mContext as WalkthroughActivity).startActivity(Intent(
                mContext,
                LoginActivity::class.java
            )
        )
        (mContext as WalkthroughActivity).finish()
    }

    fun pagerNext(viewPager: ViewPager) {
        val selected = viewPager.currentItem
        Log.e("selected", (selected + 1).toString())
        when (selected) {
            2 -> {
                pagerSkip()
                codeBooleanImg2.set(false)
                codeBooleanImg1.set(false)

            }
            else -> {
                viewPager.currentItem = selected + 1
                codeBooleanImg3.set(false)
                codeBooleanImg2.set(true)
                codeBooleanImg1.set(false)
            }
        }
    }
}