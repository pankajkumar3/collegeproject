package com.flexhelpprovider.views.termsCondition

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.lifecycle.ViewModel


class PrivacyVM(val context: Context,val intent: Intent):ViewModel() {
    fun clicks(value:String) {
        when (value) {
            "back" -> {
                (context as Activity).onBackPressed()
            }

        }
    }
}