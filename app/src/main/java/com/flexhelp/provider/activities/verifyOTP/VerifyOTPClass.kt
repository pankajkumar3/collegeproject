package com.flexhelpprovider.views.verifyOTP

import android.content.Context
import android.content.Intent
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.flexhelp.provider.activities.verifyOTP.VerifyotpVM

class VerifyOTPClass (val context: Context, val intent: Intent): ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(VerifyotpVM::class.java)) {
            return VerifyotpVM(context, intent) as T
        }
        throw IllegalArgumentException("")
    }
}