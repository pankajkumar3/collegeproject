package com.flexhelpprovider.views.splashActivity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import com.flexhelp.R
import com.flexhelpprovider.views.walkThrough.WalkthroughActivity

class SplashActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        Handler(Looper.getMainLooper()).postDelayed({

            startActivity(
                Intent(this, WalkthroughActivity::class.java)
            )
            finish()

        }, 3000)
    }
}