package com.flexhelpprovider.views.walkThrough

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.flexhelp.databinding.ActivityWalkthroughBinding

class WalkthroughFactory (val context: Context, val walkThroughBinding: ActivityWalkthroughBinding) : ViewModelProvider.Factory
{

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(WalkthroughVM::class.java)) {
            return WalkthroughVM(context, walkThroughBinding) as T
        }
        throw IllegalArgumentException("")
    }
}