package com.flexhelp.provider.activities.loginActivity

import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.flexhelp.R
import com.flexhelp.networkcalls.RequestProcessor
import com.flexhelp.networkcalls.RetrofitApi
import com.flexhelp.networkcalls.RetrofitCall
import com.flexhelp.preference.PreferenceFile
import com.flexhelp.provider.MainAct
import com.flexhelp.provider.activities.forgotActivity.ForgotActivity
import com.flexhelp.provider.activities.loginActivity.loginresponse.ProviderLoginResponse
import com.flexhelp.provider.activities.signupActivity.SignupActivity
import com.flexhelp.utils.CommonMethods
import com.flexhelp.utils.CommonMethods.showToast
import com.google.gson.JsonObject
import retrofit2.Response

class LoginVM(val context: Context, val intent: Intent) : ViewModel() {

    val codeBool = ObservableBoolean(true)
    var isChecked = ObservableBoolean(false)
    var phoneNumber = ObservableField("")

    var RememberMe = ObservableField("")
    var password = ObservableField("")
    var email = ObservableField("")
    var phone = ObservableField("")

    var countryCode = ObservableField("+91")

    var isEmail = ObservableBoolean(true)

    fun click(value: String) {
        when (value) {

            "email" -> {
                if (!isEmail.get())
                    isEmail.set(true)
                else
                    isEmail.set(false)
                // (context as MainActivity).navController!!.navigate(R.id.home)
            }
            "login" -> {
                if (CommonMethods.isNetworkAvailable(context)) {
                    if (validations()) {
                        providerLogin()
                    }
                } else {
                    showToast(context, "Please check your internet connection first")
                }

                // context.startActivity(Intent(context, MainAct::class.java))
            }
            "signupFromLogin" -> {


                context.startActivity(Intent(context, SignupActivity::class.java))
            }
            /* "forgot" -> {
                 context.startActivity(Intent(context, ForgotActivity::class.java))
             }*/
            /*"continuePhone" -> {
                codeBool.set(false)
            }
            "continueEmail" -> {
                codeBool.set(true)
            }*/

            "forgotpassFromLogin" -> {
                var key = ""
                key = if (isEmail.get()) {
                    "email"
                } else {
                    "phone"
                }
                (context as LoginActivity).startActivity(
                    Intent(context, ForgotActivity::class.java).putExtra("key", key).putExtra("comes", "forgot")
                )
            }

        }
    }

    /*
        fun onClicks(type: String) {
            when (type) {
                "email" -> {
                    if (!isEmail.get())
                        isEmail.set(true)
                    else
                        isEmail.set(false)
                    // (context as MainActivity).navController!!.navigate(R.id.home)
                }
                "loginFromMainActivity" -> {


                    if (CommonMethods.isNetworkAvailable(context)) {
                        if (validations()) {
                            providerLogin()
                        }

                    }
                }
                "signupFromLogin" -> {

                    (context as SignIn).startActivity(Intent(context, SignupActivity::class.java))
                }
                "forgotpassFromLogin" -> {
                    var key = ""
                    key = if (isEmail.get()) {
                        "email"
                    } else {
                        "phone"
                    }
                    (context as LoginActivity).startActivity(Intent(context, ForgotPasswords::class.java).putExtra("key", key).putExtra("comes", "forgot")
                    )
                }
    */
/*            "phone" -> {
                isEmail.set(false)
            }*//*

            */
/* "googleLogin" -> {
                 //   googleOnClick.value = true
             }*//*

        }
    }
*/
    private fun validations(): Boolean {

        when {
            email.get()!!.isEmpty() && isEmail.get() -> {
                showToast(context, context.getString(R.string.pleaseEnterEmail))
                return false
            }
            phone.get()!!.isEmpty() && !isEmail.get() -> {
                showToast(context, context.getString(R.string.pleaseEnterPhone))
                return false
            }


            password.get()!!.isEmpty() -> {
                showToast(context, context.getString(R.string.pleaseEnterPassword))
                return false
            }
            isChecked.get() -> {
                showToast(context, context.getString(R.string.pleaseAcceptRememberMe))
                return true
            }
            else -> {
                return true
            }
        }
    }


    private fun providerLogin() {

        /* val hashMap = HashMap<String, RequestBody?>()

          if (isEmail.get()) {
              hashMap["email"] = getPartRequestBody(email.get())
              hashMap["password"] = getPartRequestBody(password.get())

          } else {
              hashMap["phone"] = getPartRequestBody(phone.get())
              hashMap["countryCode"] = getPartRequestBody(countryCode.get())
              hashMap["password"] = getPartRequestBody(password.get())

          }

          hashMap["password"] = getPartRequestBody(password.get())
  */


        val jsonElement = JsonObject()
        if (isEmail.get()) {
            jsonElement.addProperty("email", email.get())
        } else {
            jsonElement.addProperty("phone", phone.get())
            jsonElement.addProperty("countryCode", countryCode.get())
        }
        jsonElement.addProperty("password", password.get())
        try {
            RetrofitCall().callService(
                context,
                true,
                "",
                object : RequestProcessor<Response<ProviderLoginResponse>> {
                    override suspend fun sendRequest(retrofitApi: RetrofitApi): Response<ProviderLoginResponse> {
                        return retrofitApi.providerLogin(jsonElement)
                    }

                    override fun onResponse(res: Response<ProviderLoginResponse>) {
                        val providerresponse = res.body()!!
                        if (res.isSuccessful) {
                            if (providerresponse.success) {
                                PreferenceFile.storeProviderLoginData(context, providerresponse)
                                PreferenceFile.storeAuthToken(
                                    context,
                                    providerresponse.data.accessToken
                                )
                                (context as LoginActivity).startActivity(
                                    Intent(
                                        context,
                                        MainAct::class.java
                                    )
                                )
                                context.finish()
                            } else {
                                showToast(context, providerresponse.message)
                            }
                        } else {
                            showToast(
                                context, context.getString(R.string.backendError)
                            )
                        }
                    }

                    override fun onException(message: String?) {
                        Log.e("signException", "====$message")
                    }

                })

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


}