package com.flexhelp.provider.activities.signupActivity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.WindowManager
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.flexhelp.R
import com.flexhelp.databinding.ActivitySignupBinding
import com.flexhelpprovider.views.signupActivity.SignupFactory

class SignupActivity : AppCompatActivity() {
    private var signUp:ActivitySignupBinding?=null
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        signUp = DataBindingUtil.setContentView(this, R.layout.activity_signup)
        val factory = SignupFactory(this,intent)
        val viewModel = ViewModelProvider(this, factory).get(SignupVM::class.java)
        signUp?.viewModel=viewModel
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)

    }
}