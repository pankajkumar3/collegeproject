package com.flexhelp.provider.activities.upLoadDocument

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.lifecycle.ViewModel
import com.flexhelp.provider.activities.addServicesList.AddServicesListActivity
import com.flexhelpprovider.views.upLoadDocument.adapter.AdapterDocument

class DocumentVM(val context: Context,val intent: Intent):ViewModel() {
    val adapter=AdapterDocument(context)
    fun click(value:String)
    {
        when(value)
        {
            "back"->
            {
                (context as Activity).onBackPressed()
            }
            "next"->
            context.startActivity(Intent(context, AddServicesListActivity::class.java).putExtra("document","add"))
        }
    }
}