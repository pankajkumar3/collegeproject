package com.flexhelp.provider.activities.verifyOTP.verifyotpproviderresponse

data class VerifyOTPProviderResponse(
    val `data`: Data,
    val message: String,
    val success: Boolean
)