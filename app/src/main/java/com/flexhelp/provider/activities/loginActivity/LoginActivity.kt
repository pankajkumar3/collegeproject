package com.flexhelp.provider.activities.loginActivity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.WindowManager
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.flexhelp.R
import com.flexhelp.databinding.ActivityLoginBinding
import com.flexhelpprovider.views.loginActivity.LoginFactory

class LoginActivity : AppCompatActivity() {
    private var signUp: ActivityLoginBinding?=null
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        signUp = DataBindingUtil.setContentView(this, R.layout.activity_login)
        val factory = LoginFactory(this,intent)
        val viewModel = ViewModelProvider(this, factory).get(LoginVM::class.java)
        signUp?.viewModel=viewModel
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)

    }
}