package com.flexhelp.provider.activities.welcomeActivity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import com.flexhelp.MainActivity
import com.flexhelp.R
import com.flexhelp.provider.MainAct

class WelcomeActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome)
        Handler(Looper.getMainLooper()).postDelayed({

            startActivity(
                Intent(this, MainAct::class.java)
            )
            finish()

        }, 3000)
    }
}