package com.flexhelp.provider.activities.bankAccount

import android.content.Context
import android.content.Intent
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class BankAccountFactory(val context: Context, val intent: Intent): ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(BankAccountVM::class.java)) {
            return BankAccountVM(context, intent) as T
        }
        throw IllegalArgumentException("")
    }
}