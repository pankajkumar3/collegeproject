package com.flexhelpprovider.views.signupActivity

import android.content.Context
import android.content.Intent
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.flexhelp.provider.activities.signupActivity.SignupVM

class SignupFactory (val context: Context, val intent: Intent): ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(SignupVM::class.java)) {
            return SignupVM(context, intent) as T
        }
        throw IllegalArgumentException("")
    }
}