package com.flexhelpprovider.views.questionare

import android.content.Context
import android.content.Intent
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class QuestionFactory (val context: Context, val intent: Intent): ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(QuestionVM::class.java)) {
            return QuestionVM(context, intent) as T
        }
        throw IllegalArgumentException("")
    }
}