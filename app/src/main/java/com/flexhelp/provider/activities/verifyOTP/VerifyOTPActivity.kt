package com.flexhelp.provider.activities.verifyOTP

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.WindowManager
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.flexhelp.R
import com.flexhelp.databinding.ActivityVerifyOTPBinding
import com.flexhelp.utils.CommonMethods
import com.flexhelpprovider.views.verifyOTP.VerifyOTPClass
import kotlinx.android.synthetic.main.activity_verify_o_t_p.*

class VerifyOTPActivity : AppCompatActivity() {
    private var signUp: ActivityVerifyOTPBinding?=null
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        signUp = DataBindingUtil.setContentView(this, R.layout.activity_verify_o_t_p)
        val factory = VerifyOTPClass(this,intent)
        val viewModel = ViewModelProvider(this, factory).get(VerifyotpVM::class.java)
        signUp?.viewModel=viewModel
       // window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)

    }

}

