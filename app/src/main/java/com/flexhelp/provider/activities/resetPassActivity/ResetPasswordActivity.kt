package com.flexhelp.provider.activities.resetPassActivity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.flexhelp.R
import com.flexhelp.databinding.ActivityResetPasswordBinding
import com.flexhelpprovider.views.resetPassActivity.ResetFactory

class ResetPasswordActivity : AppCompatActivity() {
    private var signUp: ActivityResetPasswordBinding?=null
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        signUp = DataBindingUtil.setContentView(this, R.layout.activity_reset_password)
        val factory = ResetFactory(this,intent)
        val viewModel = ViewModelProvider(this, factory).get(ResetPasswordVM::class.java)
        signUp?.viewModel=viewModel
    }
}