package com.flexhelpprovider.views.termsCondition

import android.content.Context
import android.content.Intent
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class TermsFactory(val context: Context, val intent: Intent): ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(TermsVM::class.java)) {
            return TermsVM(context, intent) as T
        }
        throw IllegalArgumentException("")
    }
}
