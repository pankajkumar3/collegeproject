package com.flexhelp.provider.activities.verifyOTP.verifyotpproviderresponse

data class Location(
    val coordinates: List<Double>,
    val type: String
)