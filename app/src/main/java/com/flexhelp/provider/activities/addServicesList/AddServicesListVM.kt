package com.flexhelp.provider.activities.addServicesList

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.View
import androidx.core.content.res.ResourcesCompat
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.flexhelp.MainActivity
import com.flexhelp.R
import com.flexhelpprovider.views.addServicesList.adapter.Adapterservicelist
import com.flexhelp.provider.activities.questionare.QuestionaireActivity
import com.flexhelp.recyclerAdapter.RecyclerAdapter
import com.flexhelp.recyclerAdapter.SubCatModel

class AddServicesListVM(val context: Context, val intent: Intent) : ViewModel() {
    companion object

    var addSubmit = ObservableField("Submit")
    val adapter by lazy { RecyclerAdapter<SubCatModel>(R.layout.sub_cateroy_adapter) }
    var type = "add"

    init {

        adapter.setOnItemClick(object : RecyclerAdapter.OnItemClick {
            override fun onClick(view: View, position: Int, type: String) {
                when (type) {
                    "imageClick" -> {
                        val list= adapter.items
                        list.map {
                            it.isSelected = it.adapterPosition == position
                            it
                        }
                        adapter.notifyDataSetChanged()



                    }
                }
                Log.e("adapterClick", "onClick: $position")
            }
        })


        var list = listOf(

            SubCatModel("Garden Cleaner",false, ResourcesCompat.getDrawable(context.resources,R.drawable.man_collects_leaves_cleans_park,context.theme)!!),
            SubCatModel("kitchen Cleaner",false, ResourcesCompat.getDrawable(context.resources,R.drawable.housekeeping_concept_with_young_woman,context.theme)!!),
            SubCatModel("Washing Cleaner",false, ResourcesCompat.getDrawable(context.resources,R.drawable.woman_gloves_with_rag_doing_cleaning_bathroom_clea,context.theme)!!),
            SubCatModel("Shop Cleaner",false, ResourcesCompat.getDrawable(context.resources,R.drawable.female_barista_cleaning_surface,context.theme)!!),
            SubCatModel("Building Cleaner",false, ResourcesCompat.getDrawable(context.resources,R.drawable.professional_industrial_cleaner_protective_uniform,context.theme)!!),
            SubCatModel("Snow Cleaning",false, ResourcesCompat.getDrawable(context.resources,R.drawable.man_shoveling_snow_backyard_after_blizzard_close_u,context.theme)!!)



        )

        adapter.addItems(
            list
        )




        if (intent.hasExtra("document")) {

            if (intent.getStringExtra("document") == "add") {
                type = "add"
                addSubmit.set("Next")
            } else {
                type = "btnSubmit"
                addSubmit.set("Submit")
            }

        }
        if (intent.hasExtra("manage")) {
            if (intent.getStringExtra("manage") == "btnSubmit") {
                type = "btnSubmit"
                addSubmit.set("Submit")
            } else {
                type = "add"
                addSubmit.set("Next")
            }

        }
    }

    fun click(value: String) {
        when (value) {
            "back" -> {
                (context as Activity).onBackPressed()
            }

            "next" -> {
                getData()
            }

        }
    }

    private fun getData() {
        if (intent.hasExtra("document")) {
            context.startActivity(
                Intent(
                    context,
                    QuestionaireActivity::class.java
                ).putExtra("questionnaire", "add")
            )
        }
        if (intent.hasExtra("manage")) {
            (context as Activity).onBackPressed()
        }
    }
}

