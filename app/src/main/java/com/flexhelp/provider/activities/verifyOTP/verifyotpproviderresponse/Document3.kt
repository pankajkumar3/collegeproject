package com.flexhelp.provider.activities.verifyOTP.verifyotpproviderresponse

data class Document3(
    val image: String,
    val status: Boolean
)