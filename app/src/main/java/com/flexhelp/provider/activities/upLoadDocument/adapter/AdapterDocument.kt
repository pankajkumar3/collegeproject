package com.flexhelpprovider.views.upLoadDocument.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.flexhelp.R

class AdapterDocument(val context: Context):RecyclerView.Adapter<AdapterDocument.MyViewModel>() {
    class MyViewModel(item: View):RecyclerView.ViewHolder(item) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapterDocument.MyViewModel {
        val layout=LayoutInflater.from(context).inflate(R.layout.adapter_document_single_row,parent,false)
        return MyViewModel(layout)
    }

    override fun getItemCount(): Int {
    return 5
    }
    override fun onBindViewHolder(holder: AdapterDocument.MyViewModel, position: Int) {
    }
}