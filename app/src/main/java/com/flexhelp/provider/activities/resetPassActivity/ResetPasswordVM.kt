package com.flexhelp.provider.activities.resetPassActivity

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.flexhelp.R
import com.flexhelp.networkcalls.RequestProcessor
import com.flexhelp.networkcalls.RetrofitApi
import com.flexhelp.networkcalls.RetrofitCall
import com.flexhelp.provider.activities.loginActivity.LoginActivity
import com.flexhelp.utils.CommonMethods
import com.flexhelp.utils.CommonMethods.isNetworkAvailable
import com.flexhelp.utils.CommonMethods.showToast
import com.flexhelp.views.resetpasswords.resetpasswordresponse.ResetPasswordResponse
import retrofit2.Response

class ResetPasswordVM(val context: Context,val intent: Intent):ViewModel() {


    var password = ObservableField("")
    var confirmPassword = ObservableField("")
    var authorization=ObservableField("")


    fun click(value:String)
    {
     when(value)
     {
         "back"->
         {
             (context as Activity).onBackPressed()
         }
         "save"->
         {
             if(isNetworkAvailable(context)){

                 if (validations()){
                  resetPasswordProvider()

                 }
             }



             context.startActivity(Intent(context, LoginActivity::class.java).putExtra("resettype","reset"))
         }
     }
    }

    private fun validations(): Boolean {

        when {
            password.get()!!.isEmpty() -> {

                CommonMethods.showToast(context, context.getString(R.string.pleaseEnterPassword))
                return false


            }
            confirmPassword.get()!!.isEmpty() -> {
                CommonMethods.showToast(context, context.getString(R.string.pleaseEnterNewPassword))
                return false
            }

            !CommonMethods.isValidPassword(password.get()) && password.get().toString().length < 8 -> {

                CommonMethods.showToast(context, "please enter valid password ")

                return false
            }

            confirmPassword.get()!!.isEmpty() -> {
                CommonMethods.showToast(
                    context,
                    context.getString(R.string.pleaseEnterConfirmPassword)
                )
                return false
            }

            !password.get().equals(confirmPassword.get()!!) || confirmPassword.get()!!.isEmpty() -> {
                CommonMethods.showToast(context, context.getString(R.string.passwordNotMatch))
                return false
            }
            else->{
                return true

            }

        }
    }


    private fun resetPasswordProvider() {
        try {
            RetrofitCall().callService(
                context,
                true,
                "",
                object : RequestProcessor<Response<ResetPasswordResponse>> {
                    override suspend fun sendRequest(retrofitApi: RetrofitApi): Response<ResetPasswordResponse> {
                        return retrofitApi.resetPasswordProvider(
                            //PreferenceKeys.Bearer + PreferenceFile.retrieveAuthToken(context)!!,
                            authorization.get()!!,
                            password.get()!!,
                            confirmPassword.get()!!
                            )

                    }

                    override fun onResponse(res: Response<ResetPasswordResponse>) {
                        if (res.body()?.success == true) {
//                            val response = res.body()!!
                            showToast(context, "Password Changed Successfully")
                        }
                    }

                    override fun onException(message: String?) {
                        Log.e("userException", "====$message")
                    }
                })

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }




}