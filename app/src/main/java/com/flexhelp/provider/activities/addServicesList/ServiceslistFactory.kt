package com.flexhelp.provider.activities.addServicesList

import android.content.Context
import android.content.Intent
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class ServiceslistFactory(val context: Context, val intent: Intent): ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(AddServicesListVM::class.java)) {
            return AddServicesListVM(context, intent) as T
        }
        throw IllegalArgumentException("")
    }
}