package com.flexhelp.provider.activities.questionare

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.flexhelp.R
import com.flexhelp.databinding.ActivityQuestionaireBinding
import com.flexhelpprovider.views.questionare.QuestionFactory
import com.flexhelpprovider.views.questionare.QuestionVM


class QuestionaireActivity : AppCompatActivity() {
    private var signUp: ActivityQuestionaireBinding?=null
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        signUp = DataBindingUtil.setContentView(this, R.layout.activity_questionaire)
        val factory = QuestionFactory(this,intent)
        val viewModel = ViewModelProvider(this, factory).get(QuestionVM::class.java)
        signUp?.viewModel=viewModel
    }

}