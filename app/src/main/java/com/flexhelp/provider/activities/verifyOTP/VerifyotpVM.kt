package com.flexhelp.provider.activities.verifyOTP

import android.content.Context
import android.content.Intent
import android.os.CountDownTimer
import android.util.Log
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.flexhelp.networkcalls.RequestProcessor
import com.flexhelp.networkcalls.RetrofitApi
import com.flexhelp.networkcalls.RetrofitCall
import com.flexhelp.provider.activities.bankAccount.BankAccountActivity
import com.flexhelp.provider.activities.loginActivity.LoginActivity
import com.flexhelp.provider.activities.resetPassActivity.ResetPasswordActivity
import com.flexhelp.provider.activities.verifyOTP.verifyotpproviderresponse.VerifyOTPProviderResponse
import com.flexhelp.utils.CommonMethods.showToast
import com.flexhelp.views.otpverify.OtpVerification
import com.google.gson.JsonObject
import com.mukesh.mukeshotpview.completeListener.MukeshOtpCompleteListener
import retrofit2.Response

class VerifyotpVM(val context: Context, val intent: Intent) : ViewModel() {
   var otpis=ObservableField("")

    var timer = ObservableField("")
    val setText = ObservableField("")
    var timerBoolean = ObservableBoolean(false)
    var typee = ""
    var comes = ObservableField<String>("")

    var phone = ObservableField("")
    var countryCode = ObservableField("+91")
    var otp = ObservableField("")
    var type = ObservableField("email")
    var email = ObservableField("")



    val otpListener by lazy {
        object : MukeshOtpCompleteListener {
            override fun otpCompleteListener(otp: String?) {
                otpis.set(otp)
            }
        }
    }


    init {

        if (intent.hasExtra("resettype")) {
            typee = intent.getStringExtra("resettype")!!
        }
        if (intent.hasExtra("phone")) {
            if (intent.getStringExtra("phone") == "verify") {
                typee = "verify"
                setText.set("7886766565")
            } else {
                setText.set("apptunix@gmail.com")
            }
        } else {
            setText.set("apptunix@gmail.com")
        }
    }

    private fun timer() {
        timerBoolean.set(true)
        object : CountDownTimer(30000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                timer.set("Resend in 00:${millisUntilFinished / 1000}")
            }

            override fun onFinish() {
                timerBoolean.set(false)
            }
        }.start()
    }

    fun onClicks(type: String) {
        when (type) {
            "fromOtpverification" -> {

                if (comes.get()!!.isNotEmpty()) {
                   callProviderVerifyOtp()
                }
            }
            "backfromVerification" -> {
                (context as OtpVerification).onBackPressed()
            }
        }
    }


    private fun getData() {
        if (intent.hasExtra("comes")) {

            context.startActivity(
                Intent(context, ResetPasswordActivity::class.java).putExtra("resettype", "reset"))
        }
        else
        {
            context.startActivity(Intent(context, BankAccountActivity::class.java).putExtra("profiletype", "profile")
            )

        }
    }

    fun click(value: String) {
        when (value) {
            "resend" -> {
                timer()
            }
            "verify" -> {

                if (comes.get()!!.isNotEmpty()) {
                    callProviderVerifyOtp()
                }

                /*commented by vikas*/
                 getData()
            }
        }
    }


    private fun callProviderVerifyOtp() = try {

        val objectJson = JsonObject()
        objectJson.addProperty("phone", phone.get())
        objectJson.addProperty("countryCode", countryCode.get())
        objectJson.addProperty("type", type.get())
        objectJson.addProperty("otp", otp.get()!!)
        objectJson.addProperty("email", email.get()!!)
        RetrofitCall().callService(
            context,
            true,
            "",
            object : RequestProcessor<Response<VerifyOTPProviderResponse>> {
                override suspend fun sendRequest(retrofitApi: RetrofitApi): Response<VerifyOTPProviderResponse> {
                    return retrofitApi.callProviderVerifyOtp(objectJson)
                }

                override fun onResponse(res: Response<VerifyOTPProviderResponse>) {
                    val response = res.body()!!
                    if (res.isSuccessful) {
                        if (response.success) {
                            when (comes.get()) {
                                "forgot" -> {

                                 //   PreferenceFile.storeProviderLoginData(context,Signup)
                                    (context as OtpVerification).startActivity(Intent(context, LoginActivity::class.java))
                                    context.finish()
                                }
                                "signUp" -> {
                                    //  PreferenceFile.storeLoginData(context, response)
                                    //  PreferenceFile.storeAuthToken(context, response.data.accessToken)
                                    (context as OtpVerification).startActivity(Intent(context, BankAccountActivity::class.java))
                                    context.finish()
                                }
                            }
                        } else { showToast(context, response.message)
                        }
                    } else {
                        showToast(context, res.errorBody().toString())
                    }
                }

                override fun onException(message: String?) {
                    Log.e("signException", "====$message")
                }
            })

    } catch (e: Exception) {
        e.printStackTrace()
    }

}