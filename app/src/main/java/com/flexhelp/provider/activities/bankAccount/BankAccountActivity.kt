package com.flexhelp.provider.activities.bankAccount

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.flexhelp.R
import com.flexhelp.databinding.ActivityBankAccountBinding

class BankAccountActivity : AppCompatActivity() {
    private var signUp: ActivityBankAccountBinding? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        signUp = DataBindingUtil.setContentView(this, R.layout.activity_bank_account)
        val factory = BankAccountFactory(this, intent)
        val viewModel = ViewModelProvider(this, factory).get(BankAccountVM::class.java)
        signUp?.viewModel = viewModel
    }
}