package com.flexhelp.provider.activities.forgotActivity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.WindowManager
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.flexhelp.R
import com.flexhelp.databinding.ActivityForgotBinding
import com.flexhelpprovider.views.forgotActivity.ForgotFactory

class ForgotActivity : AppCompatActivity() {
    private var signUp: ActivityForgotBinding?=null
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        signUp = DataBindingUtil.setContentView(this, R.layout.activity_forgot)
        val factory = ForgotFactory(this,intent)
        val viewModel = ViewModelProvider(this, factory).get(ForgotVM::class.java)
        signUp?.viewModel=viewModel
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)

    }
}