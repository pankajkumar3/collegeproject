package com.flexhelp.provider.activities.signupActivity.signuproviderresponse

data class SignupProviderResponse(
    val `data`: Data,
    val message: String,
    val success: Boolean
)