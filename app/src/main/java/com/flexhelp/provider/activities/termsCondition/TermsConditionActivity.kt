package com.flexhelpprovider.views.termsCondition

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.flexhelp.R
import com.flexhelp.databinding.ActivityTermsConditionBinding

class TermsConditionActivity : AppCompatActivity() {
    private var signUp: ActivityTermsConditionBinding?=null
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        signUp = DataBindingUtil.setContentView(this, R.layout.activity_terms_condition)
        val factory = TermsFactory(this,intent)
        val viewModel = ViewModelProvider(this, factory).get(TermsVM::class.java)
        signUp?.viewModel=viewModel
    }
}