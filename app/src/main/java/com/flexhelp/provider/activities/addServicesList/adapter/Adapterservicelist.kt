package com.flexhelpprovider.views.addServicesList.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.flexhelp.R
import kotlinx.android.synthetic.main.adapter_add_services.view.*

class Adapterservicelist (val context: Context): RecyclerView.Adapter<Adapterservicelist.MyViewModel>() {
    var index=-1
    class MyViewModel(item: View) : RecyclerView.ViewHolder(item) {
        val cardLayout = item.layout1
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): Adapterservicelist.MyViewModel {
        val layout =
            LayoutInflater.from(context).inflate(R.layout.adapter_add_services, parent, false)
        return MyViewModel(layout)
    }

    override fun getItemCount(): Int {
        return 8
    }

    override fun onBindViewHolder(holder: Adapterservicelist.MyViewModel, position: Int)
    {
        holder.cardLayout.setOnClickListener {
            index=position
            notifyDataSetChanged()
        }
        if(index==position)
        {
            holder.cardLayout.setBackgroundResource(R.drawable.round_btn_sky_blue)
        }
        else
        {
            holder.cardLayout.setBackgroundResource(R.drawable.button_round_blue)
        }
    }
}