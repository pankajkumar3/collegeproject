package com.flexhelpprovider.views.walkThrough

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.flexhelp.R
import com.flexhelp.databinding.ActivityWalkthroughBinding

class WalkthroughActivity : AppCompatActivity() {
    private var walkThroughBinding: ActivityWalkthroughBinding? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        walkThroughBinding = DataBindingUtil.setContentView(this, R.layout.activity_walkthrough)
        val factory = WalkthroughFactory(this, walkThroughBinding!!)
        val viewModel = ViewModelProvider(this, factory).get(WalkthroughVM::class.java)
        walkThroughBinding?.walkThroughVM = viewModel

//        PreferenceFile.storeForeverKey(this, PreferenceKeys.intro, "yes")
    }
}
