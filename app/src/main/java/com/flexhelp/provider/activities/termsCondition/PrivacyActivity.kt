package com.flexhelpprovider.views.termsCondition

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.flexhelp.R
import com.flexhelp.databinding.ActivityPrivacyBinding

class PrivacyActivity : AppCompatActivity() {
    private var signUp: ActivityPrivacyBinding?=null
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        signUp = DataBindingUtil.setContentView(this, R.layout.activity_privacy)
        val factory = PrivactFactory(this,intent)
        val viewModel = ViewModelProvider(this, factory).get(PrivacyVM::class.java)
        signUp?.viewModel=viewModel
    }
}