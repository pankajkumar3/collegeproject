package com.flexhelp.provider.activities.signupActivity

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.RadioButton
import androidx.appcompat.app.AlertDialog
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.flexhelp.R
import com.flexhelp.networkcalls.RequestProcessor
import com.flexhelp.networkcalls.RetrofitApi
import com.flexhelp.networkcalls.RetrofitCall
import com.flexhelp.provider.activities.loginActivity.LoginActivity
import com.flexhelp.provider.activities.signupActivity.signuproviderresponse.SignupProviderResponse
import com.flexhelp.provider.activities.verifyOTP.VerifyOTPActivity
import com.flexhelp.utils.CommonMethods
import com.flexhelp.utils.CommonMethods.isNetworkAvailable
import com.flexhelp.utils.CommonMethods.isValidEmail
import com.flexhelp.utils.CommonMethods.isValidNumber
import com.flexhelp.utils.CommonMethods.showToast
import com.flexhelp.views.otpverify.OtpVerification
import com.flexhelp.views.signuptwo.SignUpSecond
import com.flexhelpprovider.views.termsCondition.PrivacyActivity
import com.flexhelpprovider.views.termsCondition.TermsConditionActivity
import retrofit2.Response

class SignupVM(val context: Context, val intent: Intent) : ViewModel() {
    var typeCheck = ""

    var acceptTerms = ObservableField("")

    var ischecked = ObservableBoolean(false)
    var fullName = ObservableField("")
    var email = ObservableField("")
    var phone = ObservableField("")
    var password = ObservableField("")
    var confirmPassword = ObservableField("")
    var verificationType = ObservableField("phone")
    var countryCode = ObservableField("+91")
    var lat = ObservableField("30.7130")
    var lng = ObservableField("76.7093")
    var address = ObservableField("")

    private fun showVerificationDialog() {
        val mDialogView = LayoutInflater.from(context).inflate(R.layout.choose_verify_dialog, null)
        val mBuilder = AlertDialog.Builder(context).setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.setCanceledOnTouchOutside(true)
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val phoneNumber: RadioButton =
            mDialogView.findViewById<View>(R.id.phoneNumbner) as RadioButton
        val email2: RadioButton = mDialogView.findViewById<View>(R.id.email) as RadioButton

        phoneNumber.isChecked = true
        phoneNumber.setOnClickListener {
            if (phoneNumber.isChecked) {
                typeCheck = "phone"
                providerSignUp()

            } else if (email2.isChecked) {
                typeCheck = "email"
                providerSignUp()

            }
            //  context.startActivity(Intent(context, VerifyOTPActivity::class.java).putExtra("phone", "verify"))
            //   mAlertDialog.dismiss()
        }

        email2.isChecked = true
        email2.setOnClickListener {
            if (email2.isChecked) {
                typeCheck = "email"
                providerSignUp()

            } else if (phoneNumber.isChecked) {
                typeCheck = "phone"
                providerSignUp()

            }
            context.startActivity(Intent(context, VerifyOTPActivity::class.java))
            mAlertDialog.dismiss()
        }
    }

    fun click(value: String) {
        when (value) {
            "back" -> {
                (context as Activity).onBackPressed()
            }
            "signup" -> {
                if (isNetworkAvailable(context)) {
                    if (validations()) {
                        providerSignUp()
                        showVerificationDialog()
                    }
                } else {
                    showToast(context, "please check your internet connection first")
                }
            }
            "login" -> {
                context.startActivity(Intent(context, LoginActivity::class.java))
            }

            "terms" -> {

                context.startActivity(Intent(context, TermsConditionActivity::class.java))
            }
            "policy" -> {
                context.startActivity(Intent(context, PrivacyActivity::class.java))
            }

            "check" -> {

                if (ischecked.get()) {
                    ischecked.set(false)
                } else {
                    ischecked.set(true)
                }
            }

        }
    }


    private fun validations(): Boolean {


        when {
            fullName.get()!!.isEmpty() -> {
                showToast(context, context.getString(R.string.pleaseEnterFullName))
                return false
            }
            email.get()!!.isEmpty() -> {
                showToast(context, context.getString(R.string.pleaseEnterEmail))
                return false
            }

            !isValidEmail(email.get()!!.toString().trim()) -> {

                showToast(context, context.getString(R.string.pleaseEnterValidEmail))
                return false
            }

            phone.get()!!.isEmpty() -> {
                showToast(context, context.getString(R.string.pleaseEnterPhone))
                return false
            }

            !isValidNumber(phone.get()) && phone.get().toString().length < 8 -> {
                showToast(context, "phone number must be atleast 8 digits long")
                return false
            }


            password.get()!!.isEmpty() -> {
                showToast(context, context.getString(R.string.pleaseEnterPassword))
                return false
            }
            confirmPassword.get()!!.isEmpty() -> {
                showToast(context, context.getString(R.string.pleaseEnterConfirmPassword))
                return false
            }

            !CommonMethods.isValidPassword(password.get()) && password.toString().length < 8 -> {
                showToast(context, "phone number must be atleast 8 digits long ")
                return false
            }

            !password.get().equals(confirmPassword.get()!!) || confirmPassword.get()!!
                .isEmpty() -> {
                showToast(context, context.getString(R.string.passwordNotMatch))
                return false
            }


            address.get()!!.isEmpty() -> {
                showToast(context, context.getString(R.string.pleaseEnterLocation))
                return false
            }

            !ischecked.get() -> {
                showToast(
                    context, context.getString(R.string.pleaseSelectTermCondition)
                )
                return false
            }

            else -> {
                return true
            }
        }
    }

    private fun providerSignUp() {
        try {
            RetrofitCall().callService(
                context,
                true,
                "",
                object : RequestProcessor<Response<SignupProviderResponse>> {
                    override suspend fun sendRequest(retrofitApi: RetrofitApi): Response<SignupProviderResponse> {
                        return retrofitApi.providerSignUp(
                            fullName = fullName.get()!!,
                            email = email.get()!!,
                            phone = phone.get()!!,
                            password = password.get()!!,
                            confirmPassword = confirmPassword.get()!!,
                            verificationType = verificationType.get()!!,
                            countryCode = countryCode.get()!!,
                            lat = lat.get()!!,
                            lng = lng.get()!!,
                            address = address.get()!!
                        )
                    }

                    override fun onResponse(res: Response<SignupProviderResponse>) {
                        val response = res.body()!!
                        if (res.isSuccessful) {
                            if (response.success) {
                                if (verificationType.get() == "verificationType") {
                                    (context as SignUpSecond).startActivity(
                                        Intent(context, OtpVerification::class.java)
                                            .putExtra("key", "email")
                                            .putExtra("comes", "signUp")
                                            .putExtra("data", email.get())
                                    )
                                } else {
                                    (context as SignUpSecond).startActivity(
                                        Intent(context, OtpVerification::class.java)
                                            .putExtra("key", "phone")
                                            .putExtra("comes", "signUp")
                                            .putExtra("data", phone.get())
                                    )

                                }
                                //finish dont want to go back....
                                context.finish()
                            } else {
                                showToast(context, response.message)

                            }
                        } else {
                            showToast(context, res.errorBody().toString())
                        }
                    }

                    override fun onException(message: String?) {
                        Log.e("signException", "====$message")
                    }
                })

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


}