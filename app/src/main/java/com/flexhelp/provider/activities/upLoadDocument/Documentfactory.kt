package com.flexhelpprovider.views.upLoadDocument

import android.content.Context
import android.content.Intent
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.flexhelp.provider.activities.upLoadDocument.DocumentVM

class Documentfactory (val context: Context, val intent: Intent): ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(DocumentVM::class.java)) {
            return DocumentVM(context, intent) as T
        }
        throw IllegalArgumentException("")
    }
}