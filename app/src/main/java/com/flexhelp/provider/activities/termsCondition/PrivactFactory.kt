package com.flexhelpprovider.views.termsCondition

import android.content.Context
import android.content.Intent
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class PrivactFactory (val context: Context, val intent: Intent): ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(PrivacyVM::class.java)) {
            return PrivacyVM(context, intent) as T
        }
        throw IllegalArgumentException("")
    }
}