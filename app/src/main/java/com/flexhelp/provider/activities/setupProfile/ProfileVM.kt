package com.flexhelp.provider.activities.setupProfile

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.flexhelp.R
import com.flexhelp.provider.activities.bankAccount.BankAccountActivity
import com.flexhelp.utils.CommonMethods

class ProfileVM(val context: Context, val intent: Intent) : ViewModel() {

    var fullName = ObservableField("")
    var email = ObservableField("")
    var phone = ObservableField("")
    var location = ObservableField("")
    var password = ObservableField("")


    fun click(value: String) {
        when (value) {
            "back" -> {
                (context as Activity).onBackPressed()
            }
            "next" -> {
                context.startActivity(Intent(context, BankAccountActivity::class.java))
            }
        }
    }


    private fun validations(): Boolean {


        when {
            fullName.get()!!.isEmpty() -> {
                CommonMethods.showToast(context, context.getString(R.string.pleaseEnterFullName))
                return false
            }
            email.get()!!.isEmpty() -> {
                CommonMethods.showToast(context, context.getString(R.string.pleaseEnterEmail))
                return false
            }

            !CommonMethods.isValidEmail(email.get()!!.toString().trim()) -> {

                CommonMethods.showToast(context, context.getString(R.string.pleaseEnterValidEmail))
                return false
            }

            phone.get()!!.isEmpty() -> {
                CommonMethods.showToast(context, context.getString(R.string.pleaseEnterPhone))
                return false
            }

            !CommonMethods.isValidNumber(phone.get()) && phone.get().toString().length < 8 -> {
                CommonMethods.showToast(context, "phone number must be atleast 8 digits long")
                return false
            }

            location.get()!!.isEmpty() -> {
                CommonMethods.showToast(context, context.getString(R.string.pleaseEnterLocation))
                return false
            }
            else -> {
                return true
            }
        }
    }


}