package com.flexhelpprovider.views.questionare.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.flexhelp.R
import kotlinx.android.synthetic.main.adapter_add_services.view.*
import kotlinx.android.synthetic.main.adapter_questionaire_list.view.*

class AdapterQuestionaire (val context: Context): RecyclerView.Adapter<AdapterQuestionaire.MyViewModel>() {
    class MyViewModel(item: View): RecyclerView.ViewHolder(item) {
        val textIncrement=item.txtOne
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapterQuestionaire.MyViewModel {
        val layout= LayoutInflater.from(context).inflate(R.layout.adapter_questionaire_list,parent,false)
        return MyViewModel(layout)
    }

    override fun getItemCount(): Int {
        return 4
    }

    override fun onBindViewHolder(holder: AdapterQuestionaire.MyViewModel, position: Int) {
        if(position==0)
        {
            holder.textIncrement.setText("1.")
        }
        if(position==1)
        {
            holder.textIncrement.setText("2.")
        }
      if(position==2)
        {
            holder.textIncrement.setText("3.")
        }
        if(position==3)
        {
            holder.textIncrement.setText("4.")
        }
    }
}