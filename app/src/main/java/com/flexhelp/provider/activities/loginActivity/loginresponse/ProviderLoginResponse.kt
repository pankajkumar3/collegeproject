package com.flexhelp.provider.activities.loginActivity.loginresponse

data class ProviderLoginResponse(
    val `data`: Data,
    val message: String,
    val success: Boolean
)