package com.flexhelp.provider.activities.forgotActivity

import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.flexhelp.R
import com.flexhelp.networkcalls.RequestProcessor
import com.flexhelp.networkcalls.RetrofitApi
import com.flexhelp.networkcalls.RetrofitCall
import com.flexhelp.provider.activities.verifyOTP.VerifyOTPActivity
import com.flexhelp.utils.CommonMethods
import com.flexhelp.views.forgotpassword.ForgotPasswords
import com.flexhelp.views.forgotpassword.forgotresponse.ForgotPasswordsResponse
import com.flexhelp.views.otpverify.OtpVerification
import com.google.gson.JsonObject
import retrofit2.Response

class ForgotVM(val context: Context, val intent: Intent) : ViewModel() {


    var key = ObservableField<String>("")
    var comes = ObservableField("")
    var otpIss = ObservableField("")
    var otp = ObservableField("")
    var email = ObservableField("")
    var phone = ObservableField("")
    var countryCode = ObservableField("+91")
    var type = ObservableField("phone")



    fun clicks(value: String) {
        when (value) {
            "submit" -> {
                context.startActivity(
                    Intent(
                        context,
                        VerifyOTPActivity::class.java
                    ).putExtra("comes", "reset")
                )
            }
        }
    }


    private fun validation(): Boolean{

        return when {
            email.get()!!.isEmpty() && comes.get() == "email" -> {
                CommonMethods.showToast(context, context.getString(R.string.pleaseEnterEmail))
                false
            }


            phone.get()!!.isEmpty() && comes.get() == "phone" -> {
                CommonMethods.showToast(context, context.getString(R.string.pleaseEnterPhone))
                false
            }
            else -> {
                return true
            }
        }
    }

    private fun callForgotPassword() {
        try {
            val jsonElement = JsonObject()
            /*      if (comes.get() == "email") {
                      jsonElement.addProperty("email", email.get())
                   //   jsonElement.put("",email.get())
                  } else {*/
            jsonElement.addProperty("phone", phone.get())
            jsonElement.addProperty("countryCode", countryCode.get())
            // jsonElement.addProperty("otp",otp.get())
            //    jsonElement.addProperty("type", phone.get())


            // }


            RetrofitCall().callService(
                context,
                true,
                "",
                object : RequestProcessor<Response<ForgotPasswordsResponse>> {
                    override suspend fun sendRequest(retrofitApi: RetrofitApi): Response<ForgotPasswordsResponse> {
                        return retrofitApi.forgotPassword(jsonElement)
                    }

                    override fun onResponse(res: Response<ForgotPasswordsResponse>) {
                        val response = res.body()!!
                        if (res.isSuccessful) {
                            if (response.success) {
                                if (comes.get() == "email") {
                                    (context as ForgotPasswords).startActivity(
                                        Intent(context, OtpVerification::class.java)
                                            .putExtra("key", "email")
                                            .putExtra("comes", "forgot")
                                            .putExtra("data", phone.get())
                                            .putExtra("countryCode", countryCode.get())
                                            .putExtra("otp", otpIss.get())
                                    )
                                } else {
                                    (context as ForgotPasswords).startActivity(
                                        Intent(context, OtpVerification::class.java)
                                            .putExtra("key", "phone")
                                            .putExtra("comes", "forgot")
                                            .putExtra("data", phone.get())
                                            .putExtra("countryCode", countryCode.get())
                                            .putExtra("otp", otpIss.get())
                                    )
                                }

                            } else {
                                CommonMethods.showToast(context, response.message)
                            }
                        } else {
                            CommonMethods.showToast(
                                context,
                                context.getString(R.string.backendError)
                            )
                        }
                    }

                    override fun onException(message: String?) {
                        Log.e("signException", "====$message")
                    }
                })

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


}