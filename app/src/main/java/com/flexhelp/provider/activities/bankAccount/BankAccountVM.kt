package com.flexhelp.provider.activities.bankAccount

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.lifecycle.ViewModel
import com.flexhelp.provider.activities.upLoadDocument.DocumentActivity

class BankAccountVM(val context: Context,val intent: Intent):ViewModel() {
    fun click(value:String)

    {
        when(value)
        {
            "back"->
            {
                (context as Activity).onBackPressed()
            }
            "next"->
                context.startActivity(Intent(context, DocumentActivity::class.java))
        }
    }
}