package com.flexhelp.provider.activities.setupProfile

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.flexhelp.R
import com.flexhelp.databinding.ActivityProfileSetupBinding
import com.flexhelpprovider.views.setupProfile.ProfileFactory


class ProfileSetupActivity : AppCompatActivity() {
    private var signUp: ActivityProfileSetupBinding? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        signUp = DataBindingUtil.setContentView(this, R.layout.activity_profile_setup)
        val factory = ProfileFactory(this, intent)
        val viewModel = ViewModelProvider(this, factory).get(ProfileVM::class.java)
        signUp?.viewModel = viewModel
    }
}