package com.flexhelp.provider.activities.loginActivity.loginresponse

data class Location(
    val coordinates: List<Double>,
    val type: String
)