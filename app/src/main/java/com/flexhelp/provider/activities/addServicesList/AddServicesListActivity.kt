package com.flexhelp.provider.activities.addServicesList

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.flexhelp.R
import com.flexhelp.databinding.ActivityAddServicesListBinding

class AddServicesListActivity : AppCompatActivity() {
    private var activity: ActivityAddServicesListBinding?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity= DataBindingUtil.setContentView(this, R.layout.activity_add_services_list)
        val factory= ServiceslistFactory(this,intent)
        val viewModel= ViewModelProvider(this,factory).get(AddServicesListVM::class.java)
        activity?.viewModel=viewModel
    }
}