package com.flexhelp.provider.activities.upLoadDocument

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.flexhelp.R
import com.flexhelp.databinding.ActivityDocumentBinding
import com.flexhelpprovider.views.upLoadDocument.Documentfactory

class DocumentActivity : AppCompatActivity() {
    private var signUp: ActivityDocumentBinding?=null
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        signUp = DataBindingUtil.setContentView(this, R.layout.activity_document)
        val factory = Documentfactory(this,intent)
        val viewModel = ViewModelProvider(this, factory).get(DocumentVM::class.java)
        signUp?.viewModel=viewModel
    }
}