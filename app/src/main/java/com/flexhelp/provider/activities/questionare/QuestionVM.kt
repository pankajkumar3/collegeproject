package com.flexhelpprovider.views.questionare

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.lifecycle.ViewModel
import com.flexhelpprovider.views.questionare.adapter.AdapterQuestionaire
import com.flexhelp.provider.activities.welcomeActivity.WelcomeActivity

class QuestionVM(val context:Context,val intent: Intent):ViewModel() {
    val adapter=AdapterQuestionaire(context)
    fun clicks(value:String)
    {
        when(value)
        {
            "back"->
            {
                (context as Activity).onBackPressed()
            }
            "next"->
            {
                context.startActivity(Intent(context, WelcomeActivity::class.java))
            }
        }
    }
}