package com.flexhelp.provider

import android.os.Bundle
import android.view.Gravity
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import com.flexhelp.R
import com.flexhelp.interfaces.HomeInterface
import com.flexhelp.provider.fragments.accountfragment.AccountVM
import com.flexhelp.provider.fragments.cancelBooking.CancelBookProviderVM
import com.flexhelp.provider.fragments.chPassword.ChangeVM
import com.flexhelp.provider.fragments.chatFragment.ChatVM
import com.flexhelp.provider.fragments.contactFragment.ContactVM
import com.flexhelp.provider.fragments.feedBackFragment.FeedbackVM
import com.flexhelp.provider.fragments.fragmentSuggestion.SuggestionVM
import com.flexhelp.provider.fragments.homeFragment.HomeFragment
import com.flexhelp.provider.fragments.homeFragment.HomeVM
import com.flexhelp.provider.fragments.manageServices.ManageServicesFragment
import com.flexhelp.provider.fragments.manageServices.ManageServicesVM
import com.flexhelp.provider.fragments.notificationFragment.NotificationVM
import com.flexhelp.provider.fragments.orderFragment.OrderFragment
import com.flexhelp.provider.fragments.pastBooking.PastBookVM
import com.flexhelp.provider.fragments.ratingFragment.RatingVM
import com.flexhelp.provider.fragments.settingsFragment.SettingsFragment
import com.flexhelp.provider.fragments.settingsFragment.SettingsVM
import com.flexhelp.utils.CommonMethods
import com.flexhelp.provider.fragments.accountfragment.AccountFragment
import com.flexhelpprovider.fragments.ratingFragment.RatingFragment
import kotlinx.android.synthetic.main.header_layout.*
import kotlinx.android.synthetic.main.main_act_two.*

class MainAct : AppCompatActivity(), HomeInterface {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_act_two)
        CommonMethods.loadFragment(this, HomeFragment())
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)

        clicks()
        homeIntialization()

    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount == 1) {
            finish()
        } else {

            super.onBackPressed()
        }
    }


    fun homeIntialization() {
        ChatVM.HomeInterface = this
        SettingsVM.HomeInterface = this
        RatingVM.HomeInterface = this
        ContactVM.HomeInterface = this
        PastBookVM.HomeInterface = this
        HomeVM.HomeInterface = this
        ManageServicesVM.HomeInterface = this
        AccountVM.HomeInterface = this
        NotificationVM.HomeInterface = this
        SuggestionVM.HomeInterface = this
        ChangeVM.HomeInterface = this
        FeedbackVM.HomeInterface = this
        CancelBookProviderVM.HomeInterface = this
    }

    fun clicks() {
        txtOrderservice!!.setOnClickListener {
            CommonMethods.loadFragment(this, OrderFragment())
            drawer_layout!!.closeDrawer(Gravity.LEFT)
        }
        txtManageServices!!.setOnClickListener {
            CommonMethods.loadFragment(this, ManageServicesFragment())
            drawer_layout!!.closeDrawer(Gravity.LEFT)
        }

        txtRating!!.setOnClickListener {
            CommonMethods.loadFragment(this, RatingFragment())
            drawer_layout!!.closeDrawer(Gravity.LEFT)
        }
        txtAccount!!.setOnClickListener {
            CommonMethods.loadFragment(this, AccountFragment())
            drawer_layout!!.closeDrawer(Gravity.LEFT)
        }


        txtSettings!!.setOnClickListener {
            CommonMethods.loadFragment(this, SettingsFragment())
            drawer_layout!!.closeDrawer(Gravity.LEFT)
        }



        img_profile!!.setOnClickListener {
            CommonMethods.loadFragment(this, AccountFragment())
            drawer_layout!!.closeDrawer(Gravity.LEFT)
        }

    }

    private fun openCloseDrawer() {
        if (drawer_layout!!.isDrawerOpen(GravityCompat.START)) {
            drawer_layout!!.closeDrawer(GravityCompat.START)
        } else {
            drawer_layout!!.openDrawer(GravityCompat.START)
        }
    }

    override fun backpress() {
        onBackPressed()
    }

    override fun isVisible(isVisible: Boolean) {

    }

    override fun className(string: String) {
        openCloseDrawer()
    }

    override fun homeToggle() {
        openCloseDrawer()
    }

    override fun Navigation(boolean: Boolean) {
    }


}