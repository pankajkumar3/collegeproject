package com.flexhelp.interfaces

interface HomeInterface {
    fun backpress()
    fun isVisible(isVisible :Boolean)
    fun className(string: String)
    fun homeToggle()

    fun Navigation(boolean: Boolean)
}