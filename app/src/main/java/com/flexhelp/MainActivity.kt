package com.flexhelp

import android.content.Context
import android.os.Bundle
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.findNavController
import com.flexhelp.databinding.ActivityMainBinding
import com.flexhelp.factory.Factory
import com.flexhelp.interfaces.HomeInterface
import java.lang.ref.WeakReference

class MainActivity : AppCompatActivity(), HomeInterface {
    private var viewModel: MainVM? = null
    var binding: ActivityMainBinding? = null
    lateinit var navController: NavController
    var className = "home"

    companion object {
        var HomeInterface: HomeInterface? = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        val factory = Factory(WeakReference<Context>(this), supportFragmentManager)
        viewModel = ViewModelProvider(this, factory).get(MainVM::class.java)
        binding!!.vm = viewModel
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
        navController = findNavController(R.id.flHome)
        HomeInterface = this
        setBottomNav()
    }

    private fun setBottomNav() {
        binding?.bottomNav?.setItemSelected(R.id.home, true)
        binding!!.bottomNav.setOnItemSelectedListener {
            when (it) {
                R.id.home -> {
                    className = "home"
//                    binding!!.bottomNav.setItemSelected(R.id.home,true)
                    navController.navigate(R.id.home2)

                }
                R.id.payment -> {
//                    className = "payment"
                    className = "home"
                    navController.navigate(R.id.payment2)
                    //  CommonMethods.loadFragment(this, Payment())

                }
                R.id.booking -> {

//                    className = "booking"
                    className = "home"
                    navController.navigate(R.id.myBookings)

                    // CommonMethods.loadFragment(this, MyBookings())

                }
                R.id.setting -> {
//                    className = "setting"
                    className = "home"
                    navController.navigate(R.id.settingsS)
                    //CommonMethods.loadFragment(this, Settings())
                }
            }
        }
    }


    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount == 1) {
            finishAffinity()
        } else {
            super.onBackPressed()
        }
        /*if (className == "orderPlaced" || className == "eventDetail") {
            startActivity(Intent(this, MainActivity::class.java))
            finishAffinity()
        }
        if (className == "bookingScreen") {
            super.onBackPressed()
            super.onBackPressed()
        } else {

        }*/
    }

    override fun backpress() {


    }

    override fun isVisible(isVisible: Boolean) {
        if (isVisible)
            binding!!.bottomNav.visibility = VISIBLE
        else
            binding!!.bottomNav.visibility = GONE

    }


    override fun onResume() {
        super.onResume()


        fun setBottomNav() {
            binding?.bottomNav?.setItemSelected(R.id.home, true)
            binding!!.bottomNav.setOnItemSelectedListener {
                when (it) {
                    R.id.home -> {
                        className = "home"
                        binding!!.bottomNav.setItemSelected(R.id.home, true)
                        //         navController.navigate(R.id.home2)

                    }
                    R.id.payment -> {
                        className = "payment"
                        className = "home"
                        //         navController.navigate(R.id.payment2)
                        //  CommonMethods.loadFragment(this, Payment())

                    }
                    R.id.booking -> {

                        className = "booking"
                        className = "home"
                        //             navController.navigate(R.id.myBookings)

                        // CommonMethods.loadFragment(this, MyBookings())

                    }
                    R.id.setting -> {
                        className = "setting"
                        className = "home"
                        //                navController.navigate(R.id.settingsS)
                        //CommonMethods.loadFragment(this, Settings())
                    }
                }
            }
        }

        HomeInterface?.isVisible(isVisible = true)


    }


    override fun className(string: String) {
    }

    override fun homeToggle() {
    }

    override fun Navigation(boolean: Boolean) {
    }


}