package com.flexhelp.utils

import android.net.Uri
import androidx.databinding.BindingAdapter
import androidx.fragment.app.FragmentPagerAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import coil.load
import com.flexhelp.R
import com.flexhelp.networkcalls.WebApiKeys
import com.google.android.material.tabs.TabLayout
import com.mukesh.mukeshotpview.completeListener.MukeshOtpCompleteListener
import com.mukesh.mukeshotpview.mukeshOtpView.MukeshOtpView
import de.hdodenhof.circleimageview.CircleImageView

object
BindingAdapters {


    @BindingAdapter(value = ["otpListener"], requireAll = false)
    @JvmStatic
    fun otpListener(
        otpView: MukeshOtpView,
        listener: MukeshOtpCompleteListener
    ) {
        otpView.setOtpCompletionListener(listener)
    }

    @BindingAdapter(value = ["setCircleIamge"], requireAll = false)
    @JvmStatic
    fun setCircleIamge(
        circleImage: CircleImageView,
        imageUrl: String
    ) {
        circleImage.load(WebApiKeys.BASE_URL + imageUrl)
       // {  placeholder(R.drawable.imageprofile)}
    }


    @BindingAdapter(value = ["viewPagerAdapter", "setupDotsIndicator"], requireAll = false)
    @JvmStatic
    fun setViewPagerAdapter(viewPager: ViewPager, adapter: PagerAdapter, dotsIndicator: TabLayout)

    {
        viewPager.clipToPadding = false
        viewPager.setPageTransformer(false)
        {
                page, position ->
            page.translationX = -(0.25f * position)
            page.scaleY = 1 - (0.25f * kotlin.math.abs(position))
        }
        viewPager.adapter = adapter
        dotsIndicator.setupWithViewPager(viewPager)
    }


    @BindingAdapter(value = ["setRecyclerAdapter"], requireAll = false)
    @JvmStatic
    fun setRecyclerAdapter(recyclerView: RecyclerView, adapter: RecyclerView.Adapter<*>) {
        recyclerView.adapter = adapter
    }

    @BindingAdapter(value = ["setViewPagerAdapter"], requireAll = false)
    @JvmStatic
    fun setViewPagerAdapter(vp: ViewPager, adapter: PagerAdapter) {
        vp.adapter = adapter
    }

    @BindingAdapter(value = ["setViewPagerAdapterWithTab", "setTapLayout"], requireAll = false)
    @JvmStatic
    fun setViewPagerAdapterWithTab(vp: ViewPager, adapter: FragmentPagerAdapter, tab: TabLayout?) {
        vp.adapter = adapter
        tab?.setupWithViewPager(vp)
    }

/*
    @BindingAdapter(value = ["setImage"], requireAll = false)
    @JvmStatic
    fun setImage(image: CircleImageView, imageName: String) {
        Glide.with(image.context).load(imageName).into(image)
    }
*/


    @BindingAdapter(value = ["setUri"], requireAll = false)
    @JvmStatic
    fun setUri(image: CircleImageView, imageName: Uri) {
        if (imageName != null) {
            image.setImageURI(imageName)
        }
    }

}