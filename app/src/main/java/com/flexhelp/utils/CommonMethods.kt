package com.flexhelp.utils

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.ConnectivityManager
import android.os.Build
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.flexhelp.R
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import java.util.regex.Matcher
import java.util.regex.Pattern
import java.util.regex.Pattern.compile


object CommonMethods {




    fun getFormattedCountDownTimer(millisUntilFinished: Long): String {
        return String.format(
            "%02d:%02d",
            TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(
                TimeUnit.MILLISECONDS.toHours(
                    millisUntilFinished
                )
            ),
            TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(
                TimeUnit.MILLISECONDS.toMinutes(
                    millisUntilFinished
                )
            )
        )
    }


    fun isValidEmail(email: String): Boolean {
        val EMAIL_PATTERN = "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                "\\@" +
                "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                "(" +
                "\\." +
                "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                ")+"
        val pattern = compile(EMAIL_PATTERN)
        val matcher = pattern.matcher(email.toString())
        return matcher.matches()
    }

    fun loadFragment(context: Context, fragment: Fragment) {
        (context as FragmentActivity)
        val manager = context.supportFragmentManager
        val ft: FragmentTransaction = manager.beginTransaction()
        val list = context.supportFragmentManager.fragments
        if (list.contains(fragment)) {
            manager.popBackStack(fragment.javaClass.name, FragmentManager.POP_BACK_STACK_INCLUSIVE)
        }
        ft.replace(R.id.frame, fragment)
            .addToBackStack(fragment.javaClass.name)
            .commit()
    }

    fun isNetworkAvailable(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        return connectivityManager.activeNetworkInfo != null && connectivityManager.activeNetworkInfo!!
            .isConnected
    }


    /**string to part request body*/
    fun getPartRequestBody(string: String?): RequestBody? {
        return string?.trim()?.toRequestBody("multipart/form-data".toMediaTypeOrNull())
    }

    fun showToast(context: Context, message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    fun convertUTC(input: Date): String {

        var output = ""
        try {
            val simpleDateFormat1 = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
            simpleDateFormat1.timeZone = TimeZone.getTimeZone("UTC")
            output = simpleDateFormat1.format(input)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return output

    }

    fun coverDateTime(input: Date): String {
        var output = ""
        try {

            val simpleDateFormat1 = SimpleDateFormat("MMM dd, yyyy hh:mm:ss a")
            output = simpleDateFormat1.format(input)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return output

    }


    fun isValidNumber(number: String?): Boolean {
        val pattern: Pattern
        val matcher: Matcher
        val NUMBER_PATTERN =
            "^\\+?\\(?[0-9]{1,3}\\)? ?-?[0-9]{1,3} ?-?[0-9]{3,5} ?-?[0-9]{4}( ?-?[0-9]{3})?"
        pattern = compile(NUMBER_PATTERN)
        matcher = pattern.matcher(number.toString())
        return matcher.matches()
    }


    fun getFormattedTimeOrDateSend(data: String, patternFrom: String, patternTo: String): String {
        var d: Date? = null
        val sdf = SimpleDateFormat(patternFrom, Locale.ENGLISH)
        try {
            d = sdf.parse(data)
        } catch (ex: ParseException) {
            Log.e("exp", "" + ex.message)
        }
        sdf.applyPattern(patternTo)
        return "" + sdf.format(d)
    }

    fun Activity.hideSoftKeyboard() {
        currentFocus?.let {
            val inputMethodManager =
                ContextCompat.getSystemService(this, InputMethodManager::class.java)!!
            inputMethodManager.hideSoftInputFromWindow(it.windowToken, 0)
        }
    }


    fun hideKeyBoard(activity: Context, view: View) {
        val imm = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun convertRequestBodyFromMap(map: Map<String, Any>): Map<String, RequestBody> {
        val map1 = HashMap<String, RequestBody>()
        val size = map.size
        for (i in 0 until size) {
            try {
                val key = map.keys.toTypedArray()[i]
                val value = map[key].toString().toRequestBody("multipart/form-data".toMediaTypeOrNull())
                map1[key] = value
            } catch (ignored: Exception) {
                ignored.printStackTrace()
            }
        }
        return map1
    }

    fun saveBitmapToFile(file: File): File? {
        return try {

            // BitmapFactory options to downsize the image
            val o = BitmapFactory.Options()
            o.inJustDecodeBounds = true
            o.inSampleSize = 6
            // factor of downsizing the image
            var inputStream = FileInputStream(file)
            //Bitmap selectedBitmap = null;
            BitmapFactory.decodeStream(inputStream, null, o)
            inputStream.close()

            // The new size we want to scale to
            val REQUIRED_SIZE = 75

            // Find the correct scale value. It should be the power of 2.
            var scale = 1
            while (o.outWidth / scale / 2 >= REQUIRED_SIZE &&
                o.outHeight / scale / 2 >= REQUIRED_SIZE
            ) {
                scale *= 2
            }
            val o2 = BitmapFactory.Options()
            o2.inSampleSize = scale
            inputStream = FileInputStream(file)
            val selectedBitmap = BitmapFactory.decodeStream(inputStream, null, o2)
            inputStream.close()

            // here i override the original image file
            file.createNewFile()
            val outputStream = FileOutputStream(file)
            selectedBitmap!!.compress(Bitmap.CompressFormat.JPEG, 100, outputStream)
            file
        } catch (e: Exception) {
            null
        }
    }

    fun checkPermission(activity: Activity, permission: Array<String>): Int {
        var permissionNeeded = 0
        if (Build.VERSION.SDK_INT >= 23) {
            for (i in permission.indices) {
                val result = ContextCompat.checkSelfPermission(activity, permission[i])
                if (result != PackageManager.PERMISSION_GRANTED) {
                    permissionNeeded++
                }
            }
        }
        return permissionNeeded
    }

    fun initPlaceAutoCompleteActivity(activity: Activity, REQ: Int, country: String) {
        if (!Places.isInitialized()) {
            // Places.initialize(activity, activity.getString(R.string.google_maps_key))
        }
        val fields: List<Place.Field?>
        fields = listOf(
            Place.Field.ID,
            Place.Field.NAME,
            Place.Field.LAT_LNG,
            Place.Field.ADDRESS_COMPONENTS,
            Place.Field.ADDRESS
        )
        if (country != "") {
            val intent: Intent = Autocomplete.IntentBuilder(
                AutocompleteActivityMode.OVERLAY,
                fields
            ).setCountry(country).build(activity)
            activity.startActivityForResult(intent, REQ)
        } else {
            val intent: Intent = Autocomplete.IntentBuilder(
                AutocompleteActivityMode.OVERLAY,
                fields
            ).build(activity)
            activity.startActivityForResult(intent, REQ)
        }
    }

    fun isGooglePlayServicesAvailable(activity: Activity): Boolean {
        val googleApiAvailability = GoogleApiAvailability.getInstance()
        val status = googleApiAvailability.isGooglePlayServicesAvailable(activity)
        if (status != ConnectionResult.SUCCESS) {
            if (googleApiAvailability.isUserResolvableError(status)) {
                googleApiAvailability.getErrorDialog(activity, status, 2404).show()
            }
            return false
        }
        return true
    }

    fun isValidPassword(password: String?): Boolean {
        val pattern: Pattern
        val matcher: Matcher
        val PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{4,}$"
        pattern = compile(PASSWORD_PATTERN)
        matcher = pattern.matcher(password)
        return matcher.matches()
    }


    fun currentDate(): Date? {
        val c = Calendar.getInstance().time
        println("Current time => $c")

        val df = SimpleDateFormat("MM/dd/yyyy", Locale.getDefault())
        val formattedDate = df.format(c)
        val date = Date(formattedDate)
        return date
    }

    fun pastDateYear(year: Int = 50): Date? {
        val cal = Calendar.getInstance()
        cal.add(Calendar.YEAR, (-1 * year))
        val c = cal.time
        println("Current time => $c")
        val df = SimpleDateFormat("MM/dd/yyyy", Locale.getDefault())
        val formattedDate = df.format(c)
        val date = Date(formattedDate)
        return date
    }


    fun hideKeyboard(activity: Activity) {

        val imm: InputMethodManager =
            activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager

        var view = activity.currentFocus

        if (view == null) {
            view = View(activity)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }
}