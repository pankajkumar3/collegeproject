package com.flexhelp.preference

import android.content.Context
import android.content.SharedPreferences
import com.flexhelp.model.RememberMeData
import com.flexhelp.provider.activities.loginActivity.loginresponse.ProviderLoginResponse
import com.flexhelp.views.login.signinresponse.SigninResponse
import com.google.gson.Gson

object PreferenceFile {

    lateinit var sharedPreferences: SharedPreferences
    private lateinit var foreverSharedPreferences: SharedPreferences
    private lateinit var editor: SharedPreferences.Editor


    fun storeKey(context: Context, key: String, value: String) {
        sharedPreferences = context.getSharedPreferences(PreferenceKeys.preferenceName, Context.MODE_PRIVATE)
        editor = sharedPreferences.edit()
        editor.putString(key, value)
        editor.apply()
        editor.commit()
    }


    fun retrieveKey(context: Context, key: String): String? {
        sharedPreferences =
            context.getSharedPreferences(PreferenceKeys.preferenceName, Context.MODE_PRIVATE)
        return sharedPreferences.getString(key, null)
    }

    fun storeBoolean(context: Context, key: String, value: Boolean) {
        sharedPreferences =
            context.getSharedPreferences(PreferenceKeys.preferenceName, Context.MODE_PRIVATE)
        editor = sharedPreferences.edit()
        editor.putBoolean(key, value)
        editor.apply()
    }

    fun retrieveBooleanKey(context: Context, key: String): Boolean? {
        sharedPreferences =
            context.getSharedPreferences(PreferenceKeys.preferenceName, Context.MODE_PRIVATE)
        return sharedPreferences.getBoolean(key, false)
    }

    fun storeLoginData(context: Context, loginResponse: SigninResponse) {
        sharedPreferences = context.getSharedPreferences(PreferenceKeys.preferenceName, Context.MODE_PRIVATE)
        val prefsEditor: SharedPreferences.Editor = sharedPreferences.edit()
        prefsEditor.putString(PreferenceKeys.loginData, Gson().toJson(loginResponse))
        prefsEditor.apply()
        prefsEditor.commit()
    }




    fun storeAuthToken(context: Context, authToken: String) {
        sharedPreferences =
            context.getSharedPreferences(PreferenceKeys.preferenceName, Context.MODE_PRIVATE)
        val prefsEditor: SharedPreferences.Editor = sharedPreferences.edit()
        prefsEditor.putString(PreferenceKeys.token, authToken)
        prefsEditor.apply()
        prefsEditor.commit()
    }

    fun retrieveLoginData(context: Context): SigninResponse? {
        sharedPreferences =
            context.getSharedPreferences(PreferenceKeys.preferenceName, Context.MODE_PRIVATE)
        return Gson().fromJson(
            sharedPreferences.getString(PreferenceKeys.loginData, "")!!,
            SigninResponse::class.java
        )
    }


    fun storeRememberMeData(context: Context, rememberMeData: RememberMeData) {
        foreverSharedPreferences = context.getSharedPreferences(PreferenceKeys.foreverPreferenceName, Context.MODE_PRIVATE)
        val prefsEditor: SharedPreferences.Editor = foreverSharedPreferences.edit()
        prefsEditor.putString(PreferenceKeys.rememberMeData, Gson().toJson(rememberMeData))
        prefsEditor.apply()
        prefsEditor.commit()
    }

    fun retrieveRememberMe(context: Context): RememberMeData? {
        foreverSharedPreferences = context.getSharedPreferences(PreferenceKeys.foreverPreferenceName, Context.MODE_PRIVATE)
        return Gson().fromJson(foreverSharedPreferences.getString(PreferenceKeys.rememberMeData, "")!!, RememberMeData::class.java)
    }

    fun retrieveAuthToken(context: Context): String? {
        sharedPreferences =
            context.getSharedPreferences(PreferenceKeys.preferenceName, Context.MODE_PRIVATE)
        return sharedPreferences.getString(PreferenceKeys.token, "")
    }

    //add different Provider login Response Data class
    fun storeProviderLoginData(context: Context, providerloginResponse: ProviderLoginResponse) {
        sharedPreferences =
            context.getSharedPreferences(PreferenceKeys.preferenceName, Context.MODE_PRIVATE)
        val prefsEditor: SharedPreferences.Editor = sharedPreferences.edit()
        prefsEditor.putString(
            PreferenceKeys.providerLoginData,
            Gson().toJson(providerloginResponse)
        )
        prefsEditor.apply()
        prefsEditor.commit()
    }

    fun storeProviderAuthToken(context: Context, authToken: String) {
        sharedPreferences =
            context.getSharedPreferences(PreferenceKeys.preferenceName, Context.MODE_PRIVATE)
        val prefsEditor: SharedPreferences.Editor = sharedPreferences.edit()
        prefsEditor.putString(PreferenceKeys.providerToken, authToken)
        prefsEditor.apply()
        prefsEditor.commit()
    }

    //add different Provider login Response Data class
    fun retrieveProviderLoginData(context: Context): ProviderLoginResponse? {
        sharedPreferences = context.getSharedPreferences(PreferenceKeys.preferenceName, Context.MODE_PRIVATE)
        return Gson().fromJson(
            sharedPreferences.getString(PreferenceKeys.providerLoginData, "")!!,
            ProviderLoginResponse::class.java
        )
    }

    fun retrieveProviderAuthToken(context: Context): String? {
        sharedPreferences =
            context.getSharedPreferences(PreferenceKeys.preferenceName, Context.MODE_PRIVATE)
        return sharedPreferences.getString(PreferenceKeys.providerToken, "")
    }


    fun clearPreference(context: Context) {
        sharedPreferences = context.getSharedPreferences(PreferenceKeys.preferenceName, Context.MODE_PRIVATE)
        editor = sharedPreferences.edit()
        editor.clear()
        editor.apply()
    }

    fun storeForeverKey(context: Context, key: String, value: String) {
        //foreverSharedPreferences = context.getSharedPreferences(PreferenceKeys.foreverPreferenceName, Context.MODE_PRIVATE)
        editor = foreverSharedPreferences.edit()
        editor.putString(key, value)
        editor.apply()
    }

    fun retrieveForeverKey(context: Context, key: String): String? {
        //   foreverSharedPreferences = context.getSharedPreferences(PreferenceKeys.foreverPreferenceName, Context.MODE_PRIVATE)
        return foreverSharedPreferences.getString(key, null)
    }


}
