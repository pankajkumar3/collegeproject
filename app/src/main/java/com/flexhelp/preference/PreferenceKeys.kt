package com.flexhelp.preference

object PreferenceKeys {
    const val DEVICE_ID = "deviceID"
    const val FIREBASE_FCM_TOKEN="tokenFCM"
    var token="token"
    var providerToken="providerToken"
    const val preferenceName = "FlexHelp"
    const val foreverPreferenceName = "FlexHelpForever"
    const val loginData = "loginData"
    const val rememberMeData = "rememberMeData"
    const val signupData="signupData"
    const val providerLoginData = "providerLogindata"


  /*  const val PREFS_EMAIL= "prefsEmail"
    const val PREFS_CHECKBOX="prefsRememberMe"
    const val PREFS_PHONE="prefsphone"
    const val PREFS_PASSWORD="prefsPassword"
*/
    //const val Bearer = "Bearer "


}