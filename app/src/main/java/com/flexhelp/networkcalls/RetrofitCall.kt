package com.flexhelp.networkcalls

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.util.Log
import android.view.LayoutInflater
import androidx.appcompat.app.AlertDialog
import com.flexhelp.R
import com.flexhelp.utils.CommonMethods.showToast
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import okhttp3.OkHttpClient
import org.json.JSONObject
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class RetrofitCall {

    fun <T> callService(
        mContext: Context,
        dialogFlag: Boolean,
        token: String,
        requestProcessor: RequestProcessor<T>
    ) {
        try {
            if (dialogFlag) {
                showDialog(mContext)
            }

            val okHttpClient: OkHttpClient?
            if (token.isEmpty()) {
                okHttpClient = OkHttpClient.Builder()
                    .readTimeout(10, TimeUnit.MINUTES)
                    .connectTimeout(10, TimeUnit.MINUTES)
                    .build()
            } else {
                val client = OkHttpClient.Builder()
                    .readTimeout(10, TimeUnit.MINUTES)
                    .connectTimeout(10, TimeUnit.MINUTES)
                    .addInterceptor { chain ->
                        val original = chain.request()

                        val request = original.newBuilder()
                            .header("Authorization", "$token")
                            .method(original.method, original.body)
                            .build()

                        chain.proceed(request)
                    }

                okHttpClient = client.build()

            }

            val retrofit = Retrofit.Builder()
                .baseUrl(WebApiKeys.BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .build()


            val retrofitApi = retrofit.create(RetrofitApi::class.java)

            val coRoutineExceptionHandler = CoroutineExceptionHandler { _, t ->
                t.printStackTrace()

                CoroutineScope(Dispatchers.Main).launch {
                    hideDialog()
                    requestProcessor.onException(t.message)
                    t.printStackTrace()

                    if (t.message.equals("Unable to resolve host")) {
                     //   Alerts.commonAlert(mContext, t.message.toString())
                    } else {
                        //timeout
                       // Alerts.commonAlert(mContext, mContext.resources.getString(R.string.server_error) )
                    }

                }
            }

            CoroutineScope(Dispatchers.IO + coRoutineExceptionHandler).launch {

                val response = requestProcessor.sendRequest(retrofitApi) as Response<*>

                CoroutineScope(Dispatchers.Main).launch {
                    hideDialog()

                Log.e("apiResponse", response.toString())
                    if (!response.isSuccessful) {
                        val res = response.errorBody()!!.string()
                        if (res == "Unauthorized") {
                        //    Alerts.commonAlert(mContext, "Unauthorized")
                        } else {
                            val jsonObject = JSONObject(res)
                            showToast(mContext,jsonObject.getString("message"))
                            when {
                                jsonObject.has("message") && !jsonObject.isNull("message") -> {
                                    val message = jsonObject.getString("message")
                                  //  Alerts.commonAlert(mContext, message)
                                }
                            }
                        }
                    } else {
                        requestProcessor.onResponse(response as T)
                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        } catch (e: Throwable) {

        }
    }


    private var customDialog: AlertDialog? = null

    private fun showDialog(mContext: Context) {
        val customAlertBuilder = AlertDialog.Builder(mContext)

           val customAlertView = LayoutInflater.from(mContext).inflate(R.layout.progress_layout, null)
           customAlertBuilder.setView(customAlertView)
        customAlertBuilder.setCancelable(false)
        customDialog = customAlertBuilder.create()
        customDialog!!.show()
        customDialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

    }

    private fun hideDialog() {
        if (customDialog != null) {
            customDialog!!.dismiss()
        }
    }

}