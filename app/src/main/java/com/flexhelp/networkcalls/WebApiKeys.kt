package com.flexhelp.networkcalls

object WebApiKeys {

    const val BASE_URL = "http://15.207.74.128:9094/"

    /*Api url's*/

    const val SIGNUP = "api/app/signup"
    const val LOGIN =   "api/app/signin"
    const val FORGOTPASSWORD = "api/app/forgotPassword"
    const val OTPVERIFYPHONE ="api/app/verifyPhone"
    const val RESEND_OTP= "api/app/resendVerification"
    const val UPDATEPROFILE="api/app/updateProfile"
    const val CHANGEPASSWORD="api/app/changePassword"
    const val RESET_PASSWORD= "api/app/resetpasswordPhone"
    const val  UPLOAD_IMAGES="api/app/uploadImage"
    const val LOGOUT="api/app/logout"






    /*Base url of service provider*/

    const val PROVIDER_SIGNUP="api/serviceProvider/signup"
    const val PROVIDER_LOGIN="api/serviceProvider/signin"
    const val PROVIDER_VERIFYOTP="api/serviceProvider/verifyPhone"
    const val CHANGEPASSWORD_PROVIDER="api/serviceProvider/changePassword"
    const val RESETPASSWORD_PROVIDER="api/serviceProvider/resetpasswordPhone"
    const val LOGOUT_PROVIDER="api/serviceProvider/logout"






}