package com.flexhelp.networkcalls

data class LogoutResponse(
    val message: String,
    val success: Boolean
)
