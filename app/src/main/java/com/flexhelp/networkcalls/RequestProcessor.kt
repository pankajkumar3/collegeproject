package com.flexhelp.networkcalls

interface RequestProcessor<T> {

    suspend fun sendRequest(retrofitApi: RetrofitApi):T

    fun onResponse(res: T)

    fun onException(message: String?)
}