package com.flexhelp.networkcalls

import com.flexhelp.provider.activities.loginActivity.loginresponse.ProviderLoginResponse
import com.flexhelp.provider.activities.signupActivity.signuproviderresponse.SignupProviderResponse
import com.flexhelp.provider.activities.verifyOTP.verifyotpproviderresponse.VerifyOTPProviderResponse
import com.flexhelp.views.changepasswords.changepasswordsresponse.ChangePasswordsResponse
import com.flexhelp.views.forgotpassword.forgotresponse.ForgotPasswordsResponse
import com.flexhelp.views.login.signinresponse.SigninResponse
import com.flexhelp.views.otpverify.resendotpverificationresponse.ResendOtpVerificationResponse
import com.flexhelp.views.profile.profileresponse.ProfileImageUploadRespone
import com.flexhelp.views.resetpasswords.resetpasswordresponse.ResetPasswordResponse
import com.flexhelp.views.signuptwo.signresponse.SignUpResponse
import com.google.gson.JsonObject
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.http.*

interface RetrofitApi {
    @FormUrlEncoded
    @POST(WebApiKeys.SIGNUP)
    suspend fun signUp(
        @Field("fullName") fullName: String,
        @Field("email") email: String,
        @Field("phone") phone: String,
        @Field("password") password: String,
        @Field("confirmPassword") confirmPassword: String,
        @Field("verificationType") verificationType: String,
        @Field("countryCode") countryCode: String,
        @Field("lat") lat: String,
        @Field("lng") lng: String
    ): Response<SigninResponse>


    @Multipart
    @POST(WebApiKeys.UPLOAD_IMAGES)
    suspend fun uploadImages(
        @Part image: MultipartBody.Part?
    ): Response<ProfileImageUploadRespone>


    // @Multipart
    @POST(WebApiKeys.LOGIN)
    suspend fun login(
        @Body jsonObj: JsonObject
    ): Response<SigninResponse>


    @POST(WebApiKeys.FORGOTPASSWORD)
    suspend fun forgotPassword(@Body jsonObj: JsonObject): Response<ForgotPasswordsResponse>


    @POST(WebApiKeys.LOGOUT)
    suspend fun logout(): Response<LogoutResponse>


    @FormUrlEncoded
    @POST(WebApiKeys.RESET_PASSWORD)
    suspend fun resetPassword(
        @Header("authorization") Authorization: String,
        @Field("password") password: String,
        @Field("confirmPassword") confirmPassword: String
    ): Response<ResetPasswordResponse>


    @FormUrlEncoded
    @PUT(WebApiKeys.CHANGEPASSWORD)
    suspend fun changePassword(
        @Header("authorization") authorization: String,
        @Field("oldPassword") oldPassword: String,
        @Field("newPassword") newPassword: String,
        @Field("confirmPassword") confirmPassword: String
    ): Response<ChangePasswordsResponse>


    @POST(WebApiKeys.OTPVERIFYPHONE)
    suspend fun otpVerifyPhone(
        @Body jsonObj: JsonObject
    ): Response<SigninResponse>


    @POST(WebApiKeys.RESEND_OTP)
    suspend fun resendOTP(
        @Body jsonObj: JsonObject
    ): Response<SigninResponse>


    @Multipart
    @PUT(WebApiKeys.UPDATEPROFILE)
    suspend fun updateProfile(
        @PartMap params: Map<String, @JvmSuppressWildcards RequestBody>,
        @Part image: MultipartBody.Part?
    ): Response<SigninResponse>


    /*Provider Retrofit api's*/


    @POST(WebApiKeys.PROVIDER_LOGIN)
    suspend fun providerLogin(
        @Body jsonObj: JsonObject
    ): Response<ProviderLoginResponse>


    @FormUrlEncoded
    @POST(WebApiKeys.PROVIDER_SIGNUP)
    suspend fun providerSignUp(
        @Field("fullName") fullName: String,
        @Field("email") email: String,
        @Field("phone") phone: String,
        @Field("password") password: String,
        @Field("confirmPassword") confirmPassword: String,
        @Field("verificationType") verificationType: String,
        @Field("countryCode") countryCode: String,
        @Field("lat") lat: String,
        @Field("lng") lng: String,
        @Field("address") address: String
    ): Response<SignupProviderResponse>


    @POST(WebApiKeys.PROVIDER_VERIFYOTP)
    suspend fun callProviderVerifyOtp(
        @Body jsonObj: JsonObject
    ): Response<VerifyOTPProviderResponse>


    @FormUrlEncoded
    @PUT(WebApiKeys.CHANGEPASSWORD_PROVIDER)
    suspend fun changePasswordProvider(
        @Header("Authorization") Authorization: String,
        @Field("oldPassword") oldPassword: String,
        @Field("newPassword") newPassword: String,
        @Field("confirmPassword") confirmPassword: String
    ): Response<ChangePasswordsResponse>

    @FormUrlEncoded
    @POST(WebApiKeys.RESETPASSWORD_PROVIDER)
    suspend fun resetPasswordProvider(
        @Field("id") id: String,
        @Field("password") password: String,
        get: String
    ): Response<ResetPasswordResponse>


}