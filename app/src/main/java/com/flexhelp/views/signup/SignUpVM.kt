package com.flexhelp.views.signup

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.flexhelp.R
import com.flexhelp.views.login.SignIn
import com.flexhelp.views.otpverify.OtpVerification
import com.flexhelp.views.termsandconditions.TermAndCondition
import kotlinx.android.synthetic.main.verifybyemail_or_phone.view.*

class SignUpVM(val context: Context) : ViewModel() {

    var email = ObservableField("")
    var phone = ObservableField("")
    fun onClicks(type: String) {
        when (type) {

            "login" -> {
                (context as SignUp).startActivity(Intent(context, SignIn::class.java))
            }


            "termsandConditions" -> {
                (context as SignUp).startActivity(Intent(context, TermAndCondition::class.java))
            }


            "privacy" -> {

                (context as SignUp).startActivity(Intent(context, TermAndCondition::class.java))
            }

            "signups" -> {


                showAlert()
                // (context as SignUp).startActivity(Intent(context, VerifyByEmailOrPhone::class.java))
            }
            "backfromsignup" -> {

                (context as SignUp).onBackPressed()

            }

        }
    }

    private fun showAlert() {


        var customDialog: androidx.appcompat.app.AlertDialog? = null
        customDialog?.dismiss()

        var customAlertBuilder = androidx.appcompat.app.AlertDialog.Builder(context)
        val customAlertView =
            LayoutInflater.from(context).inflate(R.layout.verifybyemail_or_phone, null)
        customAlertBuilder.setView(customAlertView)


        // customAlertView.tvCustomAlertMessage.text = message
        customDialog = customAlertBuilder.create()
        customAlertView.rGroup.setOnCheckedChangeListener { group, checkedId ->
            when (checkedId) {
                customAlertView.rbPhone.id -> {
                    (context as SignUp).startActivity(
                        Intent(
                            context,
                            OtpVerification::class.java
                        ).putExtra("key", "phone")
                    )
                    customDialog.dismiss()
                }
                customAlertView.rbEamail.id -> {
                    (context as SignUp).startActivity(
                        Intent(
                            context,
                            OtpVerification::class.java
                        ).putExtra("key", "email")
                    )
                    customDialog.dismiss()
                }
            }
        }
        customDialog.show()

        customDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        /*   customAlertView.cardCustomAlertOk.setOnClickListener {
               customDialog.dismiss()
           }*/

    }

}