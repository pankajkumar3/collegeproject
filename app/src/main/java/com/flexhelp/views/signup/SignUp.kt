package com.flexhelp.views.signup

import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.flexhelp.R
import com.flexhelp.databinding.SignUpBinding
import com.flexhelp.factory.Factory
import java.lang.ref.WeakReference

class SignUp : AppCompatActivity() {
    private var viewModel: SignUpVM? = null
    var binding: SignUpBinding? = null
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.sign_up)
        val factory = Factory(WeakReference<Context>(this), supportFragmentManager)
        viewModel = ViewModelProvider(this, factory).get(SignUpVM::class.java)
        binding!!.vm = viewModel
    }


}