package com.flexhelp.views.usertype

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.flexhelp.views.subcat.SubCategoriesVM
import java.lang.ref.WeakReference

class UserTypesFactory(private val context: WeakReference<Context>) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(
                UserTypesFactory
                ::class.java
            )
        ) {
            return UserTypesVM(context.get()!!) as T
        }

        throw IllegalArgumentException("Error")
    }


}