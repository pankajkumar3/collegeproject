package com.flexhelp.views.usertype

import android.content.Context
import android.content.Intent
import androidx.lifecycle.ViewModel
import com.flexhelp.MainActivity
import com.flexhelp.preference.PreferenceFile
import com.flexhelp.provider.MainAct
import com.flexhelp.views.walksthrough.WalkThrough
import com.flexhelpprovider.views.walkThrough.WalkthroughActivity

class UserTypesVM(var context: Context) : ViewModel() {
    fun onClick(type: String) {
        when (type) {
            "workers" -> {
                if (PreferenceFile.retrieveLoginData(context)!= null){
                    (context as UserTypes).startActivity(Intent(context as UserTypes, MainActivity::class.java))
                    (context as UserTypes).finishAffinity()
                }else{
                    (context as UserTypes).startActivity(Intent(context as UserTypes, WalkThrough::class.java))
                    (context as UserTypes).finishAffinity()
                }

            }
            "fromLookingWork" -> {
                if (PreferenceFile.retrieveProviderLoginData(context)!= null){
                    (context as UserTypes).startActivity(Intent(context as UserTypes, MainAct::class.java))
                    (context as UserTypes).finishAffinity()
                }else{
                    (context as UserTypes).startActivity(Intent(context as UserTypes, WalkthroughActivity::class.java))
                    (context as UserTypes).finishAffinity()
                }
            }
        }
    }
}