package com.flexhelp.views.usertype

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.flexhelp.MainActivity
import com.flexhelp.R
import com.flexhelp.databinding.UserTypesBinding
import com.flexhelp.factory.Factory
import com.flexhelp.preference.PreferenceFile
import com.flexhelp.views.homee.Home
import com.flexhelp.views.login.SignIn
import java.lang.ref.WeakReference

class UserTypes : AppCompatActivity() {
    var vm: UserTypesVM? = null
    var binding: UserTypesBinding? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.user_types)
        val factory = Factory(WeakReference<Context>(this), supportFragmentManager)
        vm = ViewModelProvider(this, factory).get(UserTypesVM::class.java)
        binding!!.vm = vm

    }
}