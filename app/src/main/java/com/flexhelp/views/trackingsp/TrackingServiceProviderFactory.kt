package com.flexhelp.views.trackingsp

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.flexhelp.views.subcat.SubCategoriesVM
import java.lang.ref.WeakReference

class TrackingServiceProviderFactory(private val context: WeakReference<Context>) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(
                TrackingServiceProviderFactory
                ::class.java
            )
        ) {
            return TrackingServiceProviderVM(context.get()!!) as T
        }

        throw IllegalArgumentException("Error")
    }


}