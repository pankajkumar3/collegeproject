package com.flexhelp.views.trackingsp

import android.content.Context
import androidx.lifecycle.ViewModel
import com.flexhelp.MainActivity
import com.flexhelp.R

class TrackingServiceProviderVM(val context: Context):ViewModel() {

    fun onClicks(type: String) {
        when (type) {

            "cancel" -> {
                (context as MainActivity).navController.navigate(R.id.trackToCancelBooking)
            }


        }
    }

}