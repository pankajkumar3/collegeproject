package com.flexhelp.views.trackingsp

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.flexhelp.MainActivity
import com.flexhelp.databinding.TrackserviceProviderBinding
import com.flexhelp.factory.Factory
import java.lang.ref.WeakReference

class TrackServiceProvider : Fragment() {

    var vm: TrackingServiceProviderVM? = null
    var binding: TrackserviceProviderBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = TrackserviceProviderBinding.inflate(inflater)
        val factory = Factory(WeakReference<Context>(requireContext()), childFragmentManager)
        vm = ViewModelProvider(this, factory).get(TrackingServiceProviderVM::class.java)
        binding!!.vm = vm
        return binding!!.root
    }

    override fun onResume() {
        super.onResume()
        MainActivity.HomeInterface?.isVisible(isVisible = false)


    }


}