package com.flexhelp.views.profilesetups

import android.content.Context
import android.content.Intent
import androidx.lifecycle.ViewModel
import com.flexhelp.views.bankdetails.BankAccountDetail
import com.flexhelp.views.otpverify.OtpVerification
import com.flexhelp.views.signup.SignUp

class ProfileSetUpVM(val context: Context):ViewModel() {

    fun onClicks(type: String) {
        when (type) {
            "verifyprofile_setup" -> {

                (context as SignUp).startActivity(Intent(context, ProfileSetUp::class.java))
            }

            "Next" -> {

                (context as SignUp).startActivity(Intent(context, BankAccountDetail::class.java))
            }
        }
    }

}