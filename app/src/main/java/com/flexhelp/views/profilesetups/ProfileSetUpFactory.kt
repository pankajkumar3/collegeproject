package com.flexhelp.views.profilesetups

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.flexhelp.views.payments.PaymentVM
import java.lang.ref.WeakReference

class ProfileSetUpFactory(private val context: WeakReference<Context>) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(
                ProfileSetUpFactory
                ::class.java
            )
        ) {
            return ProfileSetUpVM(context.get()!!) as T
        }

        throw IllegalArgumentException("Error")
    }


}