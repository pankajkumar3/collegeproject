package com.flexhelp.views.chats

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.flexhelp.views.addresses.AddressVM
import java.lang.ref.WeakReference

class ChatVMFactory(private val context: WeakReference<Context>) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(
                ChatVMFactory
                ::class.java
            )
        ) {
            return ChatVM(context.get()!!) as T
        }

        throw IllegalArgumentException("Error")
    }


}