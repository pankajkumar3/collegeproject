package com.flexhelp.views.chats

import android.content.Context
import android.util.Log
import android.view.View
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.ViewModel
import com.flexhelp.MainActivity
import com.flexhelp.R
import com.flexhelp.recyclerAdapter.DemoModel
import com.flexhelp.recyclerAdapter.RecyclerAdapter

class ChatVM(val context: Context): ViewModel() {

    val adapter by lazy { RecyclerAdapter<DemoModel>(R.layout.chatadapter) }

    init {
        adapter.addItems(
            listOf(
                DemoModel("",true,ResourcesCompat.getDrawable(context.resources,R.drawable.engineering_backgroundimage,context.theme)!!),
                DemoModel("",true,ResourcesCompat.getDrawable(context.resources,R.drawable.calanderimage,context.theme)!!),
                DemoModel("",true,ResourcesCompat.getDrawable(context.resources,R.drawable.engineering_backgroundimage,context.theme)!!)

            )
        )

        adapter.setOnItemClick(object : RecyclerAdapter.OnItemClick {
            override fun onClick(view: View, position: Int, type: String) {
                when (type) {
                    "imageClick" -> {
                     ///   view.findNavController().navigate(R.id.homeToSubCat)
                    }
                }
                Log.e("adapterClick", "onClick: $position")
            }
        })

    }


    fun onClicks(type:String){
        when(type){
            "imageChatBack"->{

                (context as MainActivity).navController.navigateUp()
            }

        }
    }

}