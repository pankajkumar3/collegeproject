package com.flexhelp.views.cancelbookings

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.flexhelp.databinding.CancelBookingBinding
import com.flexhelp.factory.Factory
import java.lang.ref.WeakReference

class CancelBooking : Fragment() {


    var vm: CancelBookingVM? = null
    var binding: CancelBookingBinding? = null




    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = CancelBookingBinding.inflate(inflater)
        val factory = Factory(WeakReference<Context>(requireContext()), childFragmentManager)
        vm = ViewModelProvider(this, factory).get(CancelBookingVM::class.java)
        binding!!.vm = vm
        return binding!!.root    }

}