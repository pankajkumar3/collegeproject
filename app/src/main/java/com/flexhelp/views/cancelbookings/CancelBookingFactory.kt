package com.flexhelp.views.cancelbookings

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.flexhelp.views.bookings.BookingVM
import java.lang.ref.WeakReference

class CancelBookingFactory(private val context: WeakReference<Context>) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(
                CancelBookingFactory
                ::class.java
            )
        ) {
            return CancelBookingVM(context.get()!!) as T
        }

        throw IllegalArgumentException("Error")
    }


}