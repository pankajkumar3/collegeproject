package com.flexhelp.views.payments

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import java.lang.ref.WeakReference

class PaymentFactory(private val context: WeakReference<Context>) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(
                PaymentFactory
                ::class.java
            )
        ) {
            return PaymentVM(context.get()!!) as T
        }

        throw IllegalArgumentException("Error")
    }


}