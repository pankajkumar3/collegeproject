package com.flexhelp.views.payments

import android.content.Context
import android.util.Log
import android.view.View
import androidx.lifecycle.ViewModel
import com.flexhelp.MainActivity
import com.flexhelp.R
import com.flexhelp.recyclerAdapter.DemoModel
import com.flexhelp.recyclerAdapter.RecyclerAdapter

class PaymentVM(val context: Context): ViewModel() {


    val adapter by lazy { RecyclerAdapter<DemoModel>(R.layout.payment_adapter) }



    fun onClicks(type:String){
        when(type){
            "fromPaymentToAddCard" -> {
                (context as MainActivity).navController.navigate(R.id.action_payment2_to_addCard)

            }

            "backfromAddCard" -> {

                (context as MainActivity).navController.navigateUp()

            }

        }
    }

    init {
        adapter.addItems(
            listOf(
                DemoModel(),
                DemoModel()
            )

        )

        adapter.setOnItemClick(object : RecyclerAdapter.OnItemClick {
            override fun onClick(view: View, position: Int, type: String) {
                when (type) {
                    "imageClick" -> {
                       // view.findNavController().navigate(R.id.homeToSubCat)
                    }
                }
                Log.e("adapterClick", "onClick: $position")
            }
        })

    }



}