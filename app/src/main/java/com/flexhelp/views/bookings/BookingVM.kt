package com.flexhelp.views.bookings

import android.content.Context
import androidx.lifecycle.ViewModel
import com.flexhelp.MainActivity
import com.flexhelp.R
import com.flexhelp.recyclerAdapter.BookingModel
import com.flexhelp.recyclerAdapter.DemoModel
import com.flexhelp.recyclerAdapter.RecyclerAdapter

class BookingVM(var context: Context) : ViewModel() {
    val adapter by lazy { RecyclerAdapter<BookingModel>(R.layout.jobduration_adapter) }

    init {
        adapter.addItems(
            listOf(
                BookingModel(),
                BookingModel(),
                BookingModel(),
                BookingModel(),
                BookingModel(),
                BookingModel(),
                BookingModel(),
                BookingModel(),
                BookingModel(),
                BookingModel()
            )
        )


    }

    fun onClicks(type: String) {
        when (type) {
            "next" -> {
                (context as MainActivity).navController.navigate(R.id.bookingToPayment)
            }
            "backfrombooking" -> {

                (context as MainActivity).navController.navigateUp()

            }
        }
    }


}