package com.flexhelp.views.bookings

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.flexhelp.views.addcards.AddCardVM
import java.lang.ref.WeakReference

class BookingFactory(private val context: WeakReference<Context>) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(
                BookingFactory
                ::class.java
            )
        ) {
            return BookingVM(context.get()!!) as T
        }

        throw IllegalArgumentException("Error")
    }


}