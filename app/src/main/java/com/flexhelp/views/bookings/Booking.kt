package com.flexhelp.views.bookings

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.flexhelp.MainActivity
import com.flexhelp.databinding.BookingBinding
import com.flexhelp.factory.Factory
import java.lang.ref.WeakReference


class Booking : Fragment() {

    var vm: BookingVM? = null
    var binding: BookingBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ):

        View? {
            binding = BookingBinding.inflate(inflater)
            val factory = Factory(WeakReference<Context>(requireContext()), childFragmentManager)
            vm = ViewModelProvider(this, factory).get(BookingVM::class.java)
            binding!!.vm = vm



        return binding!!.root
        }
        // Inflate the layout for this fragment
        override fun onResume() {
            super.onResume()
            MainActivity.HomeInterface?.isVisible(isVisible = false)


        }



}