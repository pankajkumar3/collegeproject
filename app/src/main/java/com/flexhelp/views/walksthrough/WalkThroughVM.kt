package com.flexhelp.views.walksthrough

import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.viewpager.widget.ViewPager
import com.flexhelp.R
import com.flexhelp.views.login.SignIn
import com.flexhelp.views.welcome.Welcomes

class WalkThroughVM(val context: Context) : ViewModel() {

    var arrayList = arrayListOf(R.layout.walkthrough_1, R.layout.walkthrough_2, R.layout.walkthrough_3)

    val adapter = WalkThroughAdapter(context, arrayList)


    private fun pagerSkip() {
        (context as WalkThrough).startActivity(Intent(context, SignIn::class.java))
        context.finish()
    }

    fun pagerNext(viewPager: ViewPager) {
        val selected = viewPager.currentItem

        Log.e("selected", (selected + 1).toString())
        when (selected) {
            2 -> {
                pagerSkip()
            }
            else -> {
                viewPager.currentItem = selected + 1
            }
        }
    }

    fun onClick(value: String) {

        when (value) {
            "skip" -> {
                (context as WalkThrough).startActivity(Intent(context, SignIn::class.java))
                context.finish()
            }

            "next" -> {
                (context as WalkThrough).startActivity(Intent(context, SignIn::class.java))
            }
        }
    }
}
