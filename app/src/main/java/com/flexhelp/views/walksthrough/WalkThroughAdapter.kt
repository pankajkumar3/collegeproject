package com.flexhelp.views.walksthrough

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter

class WalkThroughAdapter(val context: Context, val views:ArrayList<Int>): PagerAdapter() {
    private var layoutInflater: LayoutInflater? = null
    init
    {
        layoutInflater =  context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    }
    override fun getCount(): Int {
        return 3
    }
    override fun getItemPosition(`object`: Any): Int {
        val index = views.indexOf(`object`)
        return if (index == -1) POSITION_NONE else index
    }
    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val v = layoutInflater!!.inflate(views[position], container, false)
        container.addView(v)
        return v
    }
    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }
    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }
}