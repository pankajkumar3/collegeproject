package com.flexhelp.views.walksthrough

import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.flexhelp.R
import com.flexhelp.databinding.WalkthroughBinding
import com.flexhelp.factory.Factory
import java.lang.ref.WeakReference

class WalkThrough : AppCompatActivity() {
    var vm: WalkThroughVM? = null
    var binding: WalkthroughBinding? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.walkthrough)
        val factory = Factory(WeakReference<Context>(this), supportFragmentManager)
        vm = ViewModelProvider(this, factory).get(WalkThroughVM::class.java)
        binding!!.vm = vm


    }
}