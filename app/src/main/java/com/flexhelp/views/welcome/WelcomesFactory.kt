package com.flexhelp.views.welcome

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import java.lang.ref.WeakReference

class WelcomesFactory(private val context: WeakReference<Context>) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(WelcomesVM::class.java)) {
            return WelcomesVM(context.get()!!) as T

        }
        throw IllegalArgumentException("Error")
    }

}