package com.flexhelp.views.welcome

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.appcompat.app.AppCompatActivity
import com.flexhelp.MainActivity
import com.flexhelp.R
import com.flexhelp.preference.PreferenceFile
import com.flexhelp.views.login.SignIn


class Welcomes : AppCompatActivity() {
    var auth = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.welcomes)


        Handler(Looper.getMainLooper()).postDelayed({
            val loginData = PreferenceFile.retrieveLoginData(this)
            if (loginData?.data?.accessToken != null) {
                val i = Intent(this, MainActivity::class.java)
                this.startActivity(i)
                (this as Activity).finish()
            } else {
                val i = Intent(this, SignIn::class.java)
                this.startActivity(i)
                (this as Activity).finish()
            }
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)


        }, 2000)
    }
}