package com.flexhelp.views.service

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.flexhelp.databinding.ServicesBinding
import com.flexhelp.factory.Factory
import java.lang.ref.WeakReference


class Services : Fragment() {

    var vm: ServicesVM? = null
    var binding: ServicesBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = ServicesBinding.inflate(inflater)
        val factory = Factory(WeakReference<Context>(requireContext()), childFragmentManager)
        vm = ViewModelProvider(this, factory).get(ServicesVM::class.java)
        binding!!.vm = vm
        return binding!!.root
    }

}