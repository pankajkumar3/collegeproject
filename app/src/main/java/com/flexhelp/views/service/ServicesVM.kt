package com.flexhelp.views.service

import android.content.Context
import androidx.lifecycle.ViewModel
import com.flexhelp.R
import com.flexhelp.recyclerAdapter.DemoModel
import com.flexhelp.recyclerAdapter.RecyclerAdapter

class ServicesVM(val context: Context) : ViewModel() {
    val adapter by lazy { RecyclerAdapter<DemoModel>(R.layout.sevices_item) }

    init {
        adapter.addItems(
            listOf(
                DemoModel(),
                DemoModel()


            )
        )
    }
}