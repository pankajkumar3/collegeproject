package com.flexhelp.views.termsandconditions

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.flexhelp.R
import com.flexhelp.databinding.TermandconditionBinding
import com.flexhelp.factory.Factory
import java.lang.ref.WeakReference

class TermAndCondition : AppCompatActivity() {

    var vm: TermAndConditionVM? = null
    var binding: TermandconditionBinding? = null

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.termandcondition)
        val factory = Factory(WeakReference<Context>(this), supportFragmentManager)
        vm = ViewModelProvider(this, factory).get(TermAndConditionVM::class.java)
        binding!!.vm = vm
    }
}