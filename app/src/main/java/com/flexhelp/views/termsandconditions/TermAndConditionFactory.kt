package com.flexhelp.views.termsandconditions

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.flexhelp.views.signup.SignUpVM

class TermAndConditionFactory(private val context: Context) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(SignUpVM::class.java)) {
            return TermAndConditionVM(context) as T

        }

        throw IllegalArgumentException("Error")
    }

}