package com.flexhelp.views.termsandconditions

import android.content.Context
import androidx.lifecycle.ViewModel

class TermAndConditionVM(val context: Context) : ViewModel() {
    fun onClicks(type: String) {
        when (type) {

            "backfromTermCondition" -> {
                (context as TermAndCondition).onBackPressed()
            }
        }
    }


}