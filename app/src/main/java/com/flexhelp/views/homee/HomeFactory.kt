package com.flexhelp.views.homee

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import java.lang.ref.WeakReference

class HomeFactory(private val context: WeakReference<Context>) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(
                HomeFactory
                ::class.java
            )
        ) {
            return HomeVM(context.get()!!) as T
        }

        throw IllegalArgumentException("Error")
    }


}