package com.flexhelp.views.homee

import android.content.Context
import android.util.Log
import android.view.View
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.ViewModel
import androidx.navigation.findNavController
import com.flexhelp.MainActivity
import com.flexhelp.R
import com.flexhelp.recyclerAdapter.DemoModel
import com.flexhelp.recyclerAdapter.RecyclerAdapter

class HomeVM(val context: Context) : ViewModel() {

    val adapter by lazy { RecyclerAdapter<DemoModel>(R.layout.homecat_adapter) }

    init {
        var list=   listOf(

            DemoModel("Cleaning",false, ResourcesCompat.getDrawable(context.resources,R.drawable.happy_young_woman_blue_rubber_using_mop_while_clea,context.theme)!!),
        DemoModel("Deliveries",false, ResourcesCompat.getDrawable(context.resources,R.drawable.parcel_delivery_with_good_depth_field_friendly_wor,context.theme)!!),
        DemoModel("Washing",false, ResourcesCompat.getDrawable(context.resources,R.drawable.construction_worker_sanding_down_wood_piece,context.theme)!!),
        DemoModel("Warehouse work",false, ResourcesCompat.getDrawable(context.resources,R.drawable.rows_shelves_with_goods_boxes_modern_industry_ware,context.theme)!!),
        DemoModel("Heavy lifting",false, ResourcesCompat.getDrawable(context.resources,R.drawable.helping_one_another_pretty_young_girls_helping_eac,context.theme)!!),
        DemoModel("Office work",false, ResourcesCompat.getDrawable(context.resources,R.drawable.side_view_cropped_unrecognizable_business_people_w,context.theme)!!),
        DemoModel("Washing",false, ResourcesCompat.getDrawable(context.resources,R.drawable.laundry_room_with_washing_machine_dirty_clothes_ba,context.theme)!!),
        DemoModel("Customer service",false, ResourcesCompat.getDrawable(context.resources,R.drawable.young_couple_buying_car_car_showroom,context.theme)!!)



        )

        adapter.addItems(list)

        adapter.setOnItemClick(object : RecyclerAdapter.OnItemClick {
            override fun onClick(view: View, position: Int, type: String) {
                when (type) {
                    "imageClick" -> {
                        list.map {
                            it.isSelected = it.adapterPosition==position
                            it
                        }
                        adapter.notifyDataSetChanged()
                        view.findNavController().navigate(R.id.homeToSubCat)
                    }
                }
                Log.e("adapterClick", "onClick: $position")
            }
        })

    }

    fun onClicks(type: String) {
        when (type) {
            "profile" -> {
                (context as MainActivity).navController.navigate(R.id.homeToProfile)
            }

            "notification" -> {
                (context as MainActivity).navController.navigate(R.id.homeToNotification)
            }

        }
    }

}