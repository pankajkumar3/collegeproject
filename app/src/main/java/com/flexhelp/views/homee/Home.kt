package com.flexhelp.views.homee

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.flexhelp.MainActivity
import com.flexhelp.databinding.HomeBinding
import com.flexhelp.factory.Factory
import com.flexhelp.interfaces.HomeInterface
import java.lang.ref.WeakReference

class Home : Fragment(), HomeInterface {

    var vm: HomeVM? = null
    var binding: HomeBinding? = null
    companion object{
        var HomeInterface: HomeInterface?=null
//        lateinit var HomeInterface: HomeInterface =
    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding =HomeBinding.inflate(inflater)
        val factory = Factory(WeakReference<Context>(requireContext()), childFragmentManager)
        vm = ViewModelProvider(this, factory).get(HomeVM::class.java)
        binding!!.vm = vm




        return binding!!.root
    }

    override fun backpress() {
        TODO("Not yet implemented")
    }

    override fun className(string: String) {
        /*when (string) {
            "home" -> {
                binding!!.bottomNav.setItemSelected(R.id.home,true)
                binding!!.bottomNav.setItemSelected(R.id.home2)
            }

            "payment" -> {

                binding!!.bottomNav.setItemSelected(R.id.payment2)
            }

            "booking" -> {

                binding!!.bottomNav.setItemSelected(R.id.myBookings)
            }
            "setting" -> {

                binding!!.bottomNav.setItemSelected(R.id.settingsS)
            }


        }*/
    }

    override fun homeToggle() {
    }

    override fun Navigation(boolean: Boolean) {
    }

    override fun isVisible(isVisible: Boolean) {

    }


    override fun onResume() {
        super.onResume()
        MainActivity.HomeInterface?.isVisible(isVisible = true)


    }

  /*  override fun onResume() {
        super.onResume()

        setBottomPosition()
    }
*/

    private fun setBottomPosition(){
        try {
            if (binding != null){
//                binding?.bottomNav?.setItemSelected(R.id.home,true)
            }
        }catch (e:Exception){
            e.printStackTrace()
        }
    }
}