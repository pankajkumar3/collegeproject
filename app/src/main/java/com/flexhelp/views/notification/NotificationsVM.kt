package com.flexhelp.views.notification

import android.content.Context
import android.util.Log
import android.view.View
import androidx.lifecycle.ViewModel
import com.flexhelp.MainActivity
import com.flexhelp.R
import com.flexhelp.recyclerAdapter.DemoModel
import com.flexhelp.recyclerAdapter.RecyclerAdapter

class NotificationsVM(val context: Context):ViewModel() {


    val adapter by lazy { RecyclerAdapter<DemoModel>(R.layout.recentnotification_adapter) }

    init {

        adapter.addItems(
            listOf(
                DemoModel(),
                DemoModel(),
            DemoModel()
            )
        )

        adapter.setOnItemClick(object : RecyclerAdapter.OnItemClick {
            override fun onClick(view: View, position: Int, type: String) {
                when (type) {
                    "imageviewClick" -> {
                      //  (context as MainActivity).navController.navigate(R.id.serviceDetailPage)
                    }
                }
                Log.e("adapterClick", "onClick: $position")
            }
        })

    }



    fun onClicks(type: String) {
        when (type) {
            "notiBack" -> {
                (context as MainActivity).navController.navigateUp()
            }

        }
    }
}