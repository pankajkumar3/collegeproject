package com.flexhelp.views.notification

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import java.lang.ref.WeakReference

class NotificationsFactory(private val context: WeakReference<Context>) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(
                NotificationsFactory
                ::class.java
            )
        ) {
            return NotificationsVM(context.get()!!) as T
        }

        throw IllegalArgumentException("Error")
    }


}