package com.flexhelp.views.bankdetails

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.flexhelp.views.addcards.AddCardVM
import java.lang.ref.WeakReference

class BankAccountDetailFactory(private val context: WeakReference<Context>) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(
                BankAccountDetailFactory
                ::class.java
            )
        ) {
            return BankAccountDetailVM(context.get()!!) as T
        }

        throw IllegalArgumentException("Error")
    }


}