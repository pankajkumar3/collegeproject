package com.flexhelp.views.bankdetails

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.flexhelp.R
import com.flexhelp.databinding.BankaccountDetailBinding
import com.flexhelp.databinding.ProfilesBinding
import com.flexhelp.factory.Factory
import com.flexhelp.interfaces.HomeInterface
import com.flexhelp.provider.fragments.chatFragment.ChatVM.Companion.HomeInterface
import com.flexhelp.views.profile.Profiles
import com.flexhelp.views.profile.ProfilesVM
import java.lang.ref.WeakReference


class BankAccountDetail : Fragment() {
    var vm: BankAccountDetailVM? = null
    var binding: BankaccountDetailBinding? = null

    companion object{
        var HomeInterface: HomeInterface?=null
//        lateinit var HomeInterface: HomeInterface =
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        binding = BankaccountDetailBinding.inflate(inflater)
        val factory = Factory(WeakReference<Context>(requireContext()), childFragmentManager)

        vm = ViewModelProvider(this, factory).get(BankAccountDetailVM::class.java)
        BankAccountDetail.HomeInterface?.isVisible(false)
        binding!!.vm = vm
        return binding!!.root
    }

}