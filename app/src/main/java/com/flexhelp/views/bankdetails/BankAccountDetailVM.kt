package com.flexhelp.views.bankdetails

import android.content.Context
import android.content.Intent
import androidx.lifecycle.ViewModel
import com.flexhelp.MainActivity
import com.flexhelp.views.welcome.Welcomes

class BankAccountDetailVM(val context: Context) : ViewModel() {

    fun onClicks(type: String) {
        when (type) {

            "backfBankAccDetail" -> {
                (context as MainActivity).navController.navigateUp()


            }

            "submit" -> {

                (context as BankAccountDetail).startActivity(Intent(context, Welcomes::class.java))
            }
        }
    }


}