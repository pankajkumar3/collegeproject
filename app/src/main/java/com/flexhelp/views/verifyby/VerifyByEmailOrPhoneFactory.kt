package com.flexhelp.views.verifyby

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.flexhelp.views.signup.SignUpVM

class VerifyByEmailOrPhoneFactory(private val context: Context) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(SignUpVM::class.java)) {
            return VerifyByEmailOrPhoneVM(context) as T

        }

        throw IllegalArgumentException("Error")
    }

}