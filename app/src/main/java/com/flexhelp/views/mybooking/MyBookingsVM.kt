package com.flexhelp.views.mybooking

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.databinding.ObservableBoolean
import androidx.lifecycle.ViewModel
import com.flexhelp.MainActivity
import com.flexhelp.R
import com.flexhelp.recyclerAdapter.DemoModel
import com.flexhelp.recyclerAdapter.PastModel
import com.flexhelp.recyclerAdapter.RecyclerAdapter

class MyBookingsVM(var context: Context) : ViewModel() {


    var isPast = ObservableBoolean(false)

    val adapter by lazy { RecyclerAdapter<PastModel>(R.layout.orderitems) }

    val pastAdapter by lazy { RecyclerAdapter<PastModel>(R.layout.orderitems) }

    init {


        var list = listOf<PastModel>(PastModel(), PastModel())


        var pastList = listOf<PastModel>(PastModel(true), PastModel(true))

        pastAdapter.addItems(pastList)
        adapter.addItems(list)

        adapter.setOnItemClick(object : RecyclerAdapter.OnItemClick {
            override fun onClick(view: View, position: Int, type: String) {
                when (type) {
                    "imageClick" -> {
                        var bundle = Bundle()
                        bundle.putString("key","ongoing")
                        (context as MainActivity).navController.navigate(R.id.action_myBookings_to_bookingOrderDetail,bundle)
                    }
                }
                Log.e("adapterClick", "onClick: $position")
            }
        })
        pastAdapter.setOnItemClick(object : RecyclerAdapter.OnItemClick {
            override fun onClick(view: View, position: Int, type: String) {
                when (type) {
                    "imageClick" -> {
                        var bundle = Bundle()
                        bundle.putString("key","isRate")
                        (context as MainActivity).navController.navigate(R.id.action_myBookings_to_bookingOrderDetail,bundle)
                    }
                }
                Log.e("adapterClick", "onClick: $position")
            }
        })

    }


    fun onClicks(type: String) {
        when (type) {
            "past" -> {
                isPast.set(true)
            }
            "onGoing" -> {
                isPast.set(false)
            }
        }
    }

}