package com.flexhelp.views.mybooking

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.flexhelp.views.orderdetails.BookingOrderDetailVM
import java.lang.ref.WeakReference

class MyBookingsFactory(private val context: WeakReference<Context>) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(
                MyBookingsFactory
                ::class.java
            )
        ) {
            return MyBookingsVM(context.get()!!) as T
        }

        throw IllegalArgumentException("Error")
    }


}