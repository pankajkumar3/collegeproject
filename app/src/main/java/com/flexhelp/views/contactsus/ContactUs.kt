package com.flexhelp.views.contactsus

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.flexhelp.MainActivity
import com.flexhelp.databinding.ContactusBinding
import com.flexhelp.factory.Factory
import java.lang.ref.WeakReference

class ContactUs : Fragment() {

    var vm: ContactUsVM? = null
    var binding: ContactusBinding? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = ContactusBinding.inflate(inflater)
        val factory = Factory(WeakReference<Context>(requireContext()), childFragmentManager)
        vm = ViewModelProvider(this, factory).get(ContactUsVM::class.java)
        binding!!.vm = vm
        return binding!!.root
    }

    override fun onResume() {
        super.onResume()
        MainActivity.HomeInterface?.isVisible(isVisible = false)
    }


}