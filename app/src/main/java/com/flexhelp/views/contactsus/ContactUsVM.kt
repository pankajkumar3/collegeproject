package com.flexhelp.views.contactsus

import android.content.Context
import android.view.View
import androidx.lifecycle.ViewModel
import com.flexhelp.MainActivity
import com.flexhelp.R

class ContactUsVM(var context: Context):ViewModel() {

    fun onClicks(type: String) {

        when (type) {
            "contactUs" ->
            {
                (context as MainActivity).navController.navigate(R.id.action_contactUs_to_settingsS)
            }

            "backfromContactUs" ->
            {
                (context as MainActivity).navController.navigateUp()
            }
        }
    }

}