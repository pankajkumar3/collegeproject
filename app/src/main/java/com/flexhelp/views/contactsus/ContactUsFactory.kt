package com.flexhelp.views.contactsus

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import java.lang.ref.WeakReference

class ContactUsFactory(private val context: WeakReference<Context>) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(
                ContactUsFactory
                ::class.java
            )
        ) {
            return ContactUsVM(context.get()!!) as T
        }

        throw IllegalArgumentException("Error")
    }


}