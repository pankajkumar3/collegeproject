package com.flexhelp.views.servicedetails

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.flexhelp.MainActivity
import com.flexhelp.databinding.ServicedetailPageBinding
import com.flexhelp.factory.Factory
import java.lang.ref.WeakReference


class ServiceDetailPage : Fragment() {
    var vm: ServiceDetailPageVM? = null
    var binding: ServicedetailPageBinding? = null
    private lateinit var mIndicator: View
    private var indicatorWidth = 0
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = ServicedetailPageBinding.inflate(inflater)
        val factory = Factory(WeakReference<Context>(requireContext()), childFragmentManager)
        vm = ViewModelProvider(this, factory).get(ServiceDetailPageVM::class.java)
        binding!!.vm = vm


        mIndicator = binding!!.indicator
        binding!!.tab.setupWithViewPager(binding!!.vpViewPager)
       // myTingsBinding.vpViewPager.adapter = myOrderAdapter

        val tabs = binding!!.tab.getChildAt(0) as ViewGroup

        for (i in 0 until tabs.childCount ) {
            val tab = tabs.getChildAt(i)
            val layoutParams = tab.layoutParams as LinearLayout.LayoutParams
            layoutParams.weight = 0f
            layoutParams.marginEnd = 12
           // layoutParams.marginStart = 12
            layoutParams.width = 10
            layoutParams.setMargins(0, 0, 50, 0)
            tab.layoutParams = layoutParams
            tab.requestLayout()
        }
        return binding!!.root
    }

    override fun onResume() {
        super.onResume()
        MainActivity.HomeInterface?.isVisible(isVisible = false)


    }


}