package com.flexhelp.views.servicedetails

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.flexhelp.views.aboutt.About
import com.flexhelp.views.review.Reviews
import com.flexhelp.views.service.Services

class ServicePagerAdapter(fm: FragmentManager) :
    FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
    override fun getCount() = 3
    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 ->
            {
                About()
            }
            1 ->
            {
                Services()
            }
            2 ->
            {
                Reviews()
            }

            else -> null!!
        }
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when (position) {
            0 -> {
                "About"
            }
            1 -> {
                "Services"
            }
            2 -> {
                "Reviews"
            }
            else -> ""
        }
    }
}
