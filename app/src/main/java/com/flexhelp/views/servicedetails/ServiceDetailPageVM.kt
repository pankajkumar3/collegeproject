package com.flexhelp.views.servicedetails

import android.content.Context
import androidx.databinding.ObservableBoolean
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.ViewModel
import com.flexhelp.MainActivity
import com.flexhelp.R

class ServiceDetailPageVM(val context: Context, val childFragmentManager: FragmentManager) : ViewModel() {

    var isSkip = ObservableBoolean(false)


    var vpAdapterr = ServicePagerAdapter(childFragmentManager)

    fun onClicks(type: String) {
        when (type) {

            "booknow" -> {
                (context as MainActivity).navController.navigate(R.id.booking2)
            }
            "btnSkip" -> {

                //(context as SignIn).startActivity(Intent(context, ::class.java))
            }
            "backfromservicedetailpage" -> {

                (context as MainActivity).navController.navigateUp()

            }

        }
    }

    fun onClick(type: String) {
        when (type) {
            "isSkip" -> {
                isSkip.set(true)
            }

        }
    }



}