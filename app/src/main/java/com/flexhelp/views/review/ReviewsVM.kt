package com.flexhelp.views.review

import android.content.Context
import androidx.lifecycle.ViewModel
import com.flexhelp.R
import com.flexhelp.recyclerAdapter.DemoModel
import com.flexhelp.recyclerAdapter.RecyclerAdapter

class ReviewsVM(val context: Context) : ViewModel() {
    val adapter by lazy { RecyclerAdapter<DemoModel>(R.layout.reviews_item) }

    init {
        adapter.addItems(
            listOf(
                DemoModel(),
                DemoModel()
            )
        )
    }
}