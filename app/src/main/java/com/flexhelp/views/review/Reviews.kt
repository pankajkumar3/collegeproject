package com.flexhelp.views.review

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.flexhelp.databinding.ReviewsBinding
import com.flexhelp.factory.Factory
import java.lang.ref.WeakReference

class Reviews : Fragment() {
    var vm: ReviewsVM? = null
    var binding: ReviewsBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = ReviewsBinding.inflate(inflater)
        val factory = Factory(WeakReference<Context>(requireContext()), childFragmentManager)
        vm = ViewModelProvider(this, factory).get(ReviewsVM::class.java)
        binding!!.vm = vm
        return binding!!.root
    }

}