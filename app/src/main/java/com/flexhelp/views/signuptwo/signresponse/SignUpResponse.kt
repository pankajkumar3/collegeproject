package com.flexhelp.views.signuptwo.signresponse


data class SignUpResponse(
    val `data`: Data,
    val message: String,
    val success: Boolean
)