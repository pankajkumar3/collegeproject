package com.flexhelp.views.signuptwo.signresponse

data class Data(
    val countryCode: String,
    val phone: String,
    val email: String ?= null
)