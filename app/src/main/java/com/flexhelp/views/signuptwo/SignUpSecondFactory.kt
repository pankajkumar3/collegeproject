package com.flexhelp.views.signuptwo

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.flexhelp.views.profilesetups.ProfileSetUpVM
import java.lang.ref.WeakReference

class SignUpSecondFactory(private val context: WeakReference<Context>) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(
                SignUpSecondFactory
                ::class.java
            )
        ) {
            return SignUpSecondVM(context.get()!!) as T
        }

        throw IllegalArgumentException("Error")
    }


}