package com.flexhelp.views.signuptwo

import android.content.Context
import android.os.Bundle
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.flexhelp.R
import com.flexhelp.databinding.SignupSecondBinding
import com.flexhelp.factory.Factory
import java.lang.ref.WeakReference

class SignUpSecond : AppCompatActivity() {

    var vm: SignUpSecondVM? = null
    var binding: SignupSecondBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // setContentView(R.layout.signup_second)

        binding = DataBindingUtil.setContentView(this, R.layout.signup_second)
        val factory = Factory(WeakReference<Context>(this), supportFragmentManager)
        vm = ViewModelProvider(this, factory).get(SignUpSecondVM::class.java)
        binding!!.vm = vm
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)

    }
}