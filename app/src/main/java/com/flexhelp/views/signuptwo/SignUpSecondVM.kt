package com.flexhelp.views.signuptwo

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.util.Log
import android.view.LayoutInflater
import android.view.animation.AnimationUtils
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.flexhelp.R
import com.flexhelp.networkcalls.RequestProcessor
import com.flexhelp.networkcalls.RetrofitApi
import com.flexhelp.networkcalls.RetrofitCall
import com.flexhelp.preference.PreferenceFile
import com.flexhelp.utils.CommonMethods.isNetworkAvailable
import com.flexhelp.utils.CommonMethods.isValidNumber
import com.flexhelp.utils.CommonMethods.isValidPassword
import com.flexhelp.utils.CommonMethods.showToast
import com.flexhelp.utils.MyBounceInterpolator
import com.flexhelp.views.login.SignIn
import com.flexhelp.views.login.signinresponse.SigninResponse
import com.flexhelp.views.otpverify.OtpVerification
import com.flexhelp.views.privacypolicies.PrivacyAndPolicy
import com.flexhelp.views.termsandconditions.TermAndCondition
import kotlinx.android.synthetic.main.verifybyemail_or_phone.view.*
import retrofit2.Response
import java.util.regex.Pattern


class SignUpSecondVM(val context: Context) : ViewModel() {
    var ischecked = ObservableBoolean(false)

    var email = ObservableField("")
    var fullName = ObservableField("")
    var pass = ObservableField("")
    var confirmPass = ObservableField("")
    var lat = ObservableField("30.7130")
    var lng = ObservableField("76.7093")
    var verificationType = ObservableField("phone")
    var countryCode = ObservableField("+91")
    var Location = ObservableField("")
    var phone = ObservableField("")
    var emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+"


    // var emailPattern = ("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
    //   + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")

    //var checkTerms = ObservableField("")


    var acceptTerms = ObservableField("")

    fun onClicks(type: String) {
        when (type) {
            "loginFromSignup" -> {
                (context as SignUpSecond).startActivity(Intent(context, SignIn::class.java))
                val myAnim = AnimationUtils.loadAnimation(context, R.anim.fade_out)
                val interpolator = MyBounceInterpolator(0.1, 20)
                myAnim.interpolator = interpolator
                myAnim.start()
            }

            "termsandConditionsSignup" -> {
                (context as SignUpSecond).startActivity(
                    Intent(
                        context,
                        TermAndCondition::class.java
                    )
                )
            }


            "privacyFromSignup" -> {
                (context as SignUpSecond).startActivity(
                    Intent(context, PrivacyAndPolicy::class.java)
                )
            }

            "signupsFromSecond" -> {


                if (isNetworkAvailable(context)) {
                    if (validations()) {
                        showAlert()
                    }
                } else {
                    showToast(context, "please check your internet connection first")
                }
            }
            "backfromsignupSecond" -> {
                (context as SignUpSecond).onBackPressed()
            }

            "check" -> {
                if (ischecked.get()) {
                    ischecked.set(false)
                } else {
                    ischecked.set(true)
                }
            }


        }
    }


    private fun validations(): Boolean {
        when {
            fullName.get()?.trim().toString().isEmpty() -> {
                showToast(context, context.getString(R.string.pleaseEnterFullName))
                return false
            }

            email.get()!!.toString().trim().isEmpty() -> {
                showToast(context, context.getString(R.string.pleaseEnterEmail))
                return false
            }
            !isValidEmailId(email.get()!!.toString().trim()) -> {

                showToast(context, context.getString(R.string.pleaseEnterValidEmail))
                return false
            }


            /*email.get()!!.matches(emailPattern).toString().isEmpty()->{

                showToast(context, context.getString(R.string.validEmailAddress))

                return false
            }
*/


            /*  email.get().toString().trim().contains(" ") ->{

                  showToast(context,context.getString(R.string.emailShouldNotContainSpace))

                  return false
              }*/

            /*  email.get()!!.toString().trim().isEmpty() -> {
                  showToast(context, context.getString(R.string.pleaseEnterEmail))
                  return false
              }*/


            /*  phone.get()!!.toString().isEmpty() -> {
                  showToast(context, context.getString(R.string.pleaseEnterPhone))
                  return false
              }*/


            !isValidNumber(phone.get()) && phone.get().toString().length < 8 -> {
                showToast(context, "phone number must be atleast 8 digits long")
                return false
            }


            /*  !isValidNumber(phone.get()) && phone.get().toString().length < 8 -> {

                  showToast(context, "phone number must be atleast 8 digits long")

                  return false
              }*/


            pass.get()!!.trim().isEmpty() -> {
                showToast(context, context.getString(R.string.pleaseEnterPassword))
                return false
            }
            confirmPass.get()!!.trim().isEmpty() -> {
                showToast(context, context.getString(R.string.pleaseEnterConfirmPassword))
                return false
            }
            !isValidPassword(pass.get()) && pass.get().toString().length < 8 -> {
                showToast(context, "Please enter atleast 8 digit's  password")
                return false
            }
            !pass.get().equals(confirmPass.get()!!) || confirmPass.get()!!.isEmpty() -> {
                showToast(context, context.getString(R.string.passwordNotMatch))
                return false
            }

            Location.get()!!.isEmpty() -> {
                showToast(context, context.getString(R.string.pleaseEnterLocation))
                return false
            }

            Location.get().toString().contains(" ") -> {
                showToast(context, context.getString(R.string.locationShouldNotDigits))
                return false
            }
            !ischecked.get() -> {
                showToast(context, context.getString(R.string.pleaseSelectTermCondition))
                return false
            }
            else -> {
                return true
            }
        }
    }

    private fun isValidEmailId(email: String): Boolean {
        return Pattern.compile(
            "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                    + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                    + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                    + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$"
        ).matcher(email).matches()
    }

    private fun callSignUp() {
        try {
            RetrofitCall().callService(
                context,
                true,
                "",
                object : RequestProcessor<Response<SigninResponse>> {
                    override suspend fun sendRequest(retrofitApi: RetrofitApi): Response<SigninResponse> {
                        return retrofitApi.signUp(
                            fullName = fullName.get()!!,
                            email = email.get()!!,
                            phone = phone.get()!!,
                            password = pass.get()!!,
                            confirmPassword = confirmPass.get()!!,
                            verificationType = verificationType.get()!!,
                            countryCode = countryCode.get()!!,
                            lat = lat.get()!!,
                            lng = lng.get()!!
                        )
                    }

                    override fun onResponse(res: Response<SigninResponse>) {
                        val response = res.body()!!
                        if (res.isSuccessful) {
                            if (response.success) {
                                PreferenceFile.storeLoginData(context, response)
                               // PreferenceFile.storeAuthToken(context, response?.data?.accessToken!!)
                                if (verificationType.get() == "email") {
                                    (context as SignUpSecond).startActivity(
                                        Intent(context, OtpVerification::class.java)
                                            .putExtra("key", "email")
                                            .putExtra("comes", "signUp")
                                            // .putExtra("countryCode",countryCode)
                                            .putExtra("data", email.get())
                                    )
                                } else {
                                    PreferenceFile.storeLoginData(context, response)
                                    //PreferenceFile.storeAuthToken(context, response.data.accessToken)
                                    (context as SignUpSecond).startActivity(
                                        Intent(context, OtpVerification::class.java)
                                            .putExtra("key", "phone")
                                            .putExtra("comes", "signUp")
                                            .putExtra("countryCode", countryCode)
                                            .putExtra(
                                                "data", phone.get()
                                            )
                                    )


                                }

                                //finish dont want to go back....

                                context.finish()
                            } else {

                                showToast(context, response.message)

                            }
                        } else {
                            showToast(context, res.errorBody().toString())
                        }
                    }

                    override fun onException(message: String?) {
                        Log.e("signException", "====$message")
                    }
                })

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


    private fun showAlert() {


        var customDialog: androidx.appcompat.app.AlertDialog? = null
        customDialog?.dismiss()

        val customAlertBuilder = androidx.appcompat.app.AlertDialog.Builder(context)
        val customAlertView =
            LayoutInflater.from(context).inflate(R.layout.verifybyemail_or_phone, null)
        customAlertBuilder.setView(customAlertView)


        // customAlertView.tvCustomAlertMessage.text = message
        customDialog = customAlertBuilder.create()
        customAlertView.rGroup.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                customAlertView.rbPhone.id -> {
                    verificationType.set("phone")
                    callSignUp()
                    customDialog.dismiss()
                }
                customAlertView.rbEamail.id -> {
                    verificationType.set("email")
                    callSignUp()

                    customDialog.dismiss()
                }
            }
        }
        customDialog.show()

        customDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        /*   customAlertView.cardCustomAlertOk.setOnClickListener {
               customDialog.dismiss()
           }*/

    }

}

