package com.flexhelp.views.addcards

import android.content.Context
import androidx.lifecycle.ViewModel
import com.flexhelp.MainActivity
import com.flexhelp.R

class AddCardVM(val context: Context) : ViewModel() {


    fun onClicks(type:String){
        when(type){
            "saveFromAddCard" -> {
                (context as MainActivity).navController.navigate(R.id.action_addCard_to_payment2)

            }

            "backFromAddCard" -> {

                (context as MainActivity).navController.navigateUp()

            }

        }
    }



}