package com.flexhelp.views.addcards

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import java.lang.ref.WeakReference

class AddCardFactory(private val context: WeakReference<Context>) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(
                AddCardFactory
                ::class.java
            )
        ) {
            return AddCardVM(context.get()!!) as T
        }

        throw IllegalArgumentException("Error")
    }


}