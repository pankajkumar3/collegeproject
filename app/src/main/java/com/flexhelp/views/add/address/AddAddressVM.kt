package com.flexhelp.views.add.address

import android.content.Context
import androidx.lifecycle.ViewModel
import com.flexhelp.MainActivity
import com.flexhelp.R

class AddAddressVM(val context: Context) : ViewModel() {


    fun onClicks(type: String) {
        when (type) {
            "imageBackFromAddAddress" -> {
                (context as MainActivity).navController.navigateUp()
            }

            "saveAddAddress" -> {
                (context as MainActivity).navController.navigate(R.id.action_addaddress_to_settingsS)
            }

        }
    }


}