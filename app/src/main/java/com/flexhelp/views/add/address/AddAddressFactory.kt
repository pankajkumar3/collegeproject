package com.flexhelp.views.add.address

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import java.lang.ref.WeakReference

class AddAddressFactory(private val context: WeakReference<Context>) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(
                AddAddressVM
                ::class.java
            )
        ) {
            return AddAddressVM(context.get()!!) as T
        }

        throw IllegalArgumentException("Error")
    }


}