package com.flexhelp.views.add.address

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.flexhelp.MainActivity
import com.flexhelp.databinding.AddaddressBinding
import com.flexhelp.factory.Factory
import java.lang.ref.WeakReference


class AddAddress : Fragment() {


    var vm: AddAddressVM? = null
    var binding: AddaddressBinding? = null



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View?
    {
        binding = AddaddressBinding.inflate(inflater)
        val factory = Factory(WeakReference<Context>(requireContext()), childFragmentManager)
        vm = ViewModelProvider(this, factory).get(AddAddressVM::class.java)
        binding!!.vm = vm
        return binding!!.root

    }

    override fun onResume() {
        super.onResume()
        MainActivity.HomeInterface?.isVisible(isVisible = false)


    }


}