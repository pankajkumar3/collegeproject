package com.flexhelp.views.suggestions

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.flexhelp.views.review.ReviewsVM
import com.flexhelp.views.signup.SignUpVM

class SuggestionFactory(private val context: Context) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(SuggestionVM::class.java)) {
            return ReviewsVM(context) as T
        }
        throw IllegalArgumentException("Error")
    }


}