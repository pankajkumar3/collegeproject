package com.flexhelp.views.suggestions

import android.content.Context
import androidx.lifecycle.ViewModel
import com.flexhelp.MainActivity
import com.flexhelp.R

class SuggestionVM(var context: Context) : ViewModel() {


    fun onClicks(type: String) {

        when (type) {
            "backfromSuggestion" -> {
                (context as MainActivity).navController.navigateUp()
            }

            "submit" -> {
                (context as MainActivity).navController.navigate(R.id.action_suggestion_to_settingsS)
            }

        }
    }
}