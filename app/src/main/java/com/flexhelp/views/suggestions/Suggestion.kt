package com.flexhelp.views.suggestions

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.flexhelp.MainActivity
import com.flexhelp.databinding.SuggestionBinding
import com.flexhelp.factory.Factory
import java.lang.ref.WeakReference


class Suggestion : Fragment() {


    private var vm: SuggestionVM? = null
    var binding: SuggestionBinding? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = SuggestionBinding.inflate(inflater)
        val factory = Factory(WeakReference<Context>(requireContext()), childFragmentManager)
        vm = ViewModelProvider(this, factory).get(SuggestionVM::class.java)
        binding!!.vm = vm
        return binding!!.root
    }

    override fun onResume() {
        super.onResume()
        MainActivity.HomeInterface?.isVisible(isVisible = false)


    }


}