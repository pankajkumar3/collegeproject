package com.flexhelp.views.aboutt

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.flexhelp.R
import com.flexhelp.databinding.SubcategoriesBinding
import com.flexhelp.views.subcat.SubCategoriesVM


class HomeCatAdapter : Fragment() {




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.homecat_adapter, container, false)
    }

}