package com.flexhelp.views.aboutt

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.flexhelp.views.chats.ChatVM
import java.lang.ref.WeakReference

class AboutFactory(private val context: WeakReference<Context>) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(
                AboutVM
                ::class.java
            )
        ) {
            return AboutVM(context.get()!!) as T
        }

        throw IllegalArgumentException("Error")
    }


}