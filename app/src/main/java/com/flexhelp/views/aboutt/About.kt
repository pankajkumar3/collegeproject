package com.flexhelp.views.aboutt

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.flexhelp.databinding.AboutBinding
import com.flexhelp.factory.Factory
import java.lang.ref.WeakReference


class About : Fragment() {
    var vm: AboutVM? = null
    var binding: AboutBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = AboutBinding.inflate(inflater)
        val factory = Factory(WeakReference<Context>(requireContext()), childFragmentManager)
        vm = ViewModelProvider(this, factory).get(AboutVM::class.java)
        binding!!.vm = vm
        return binding!!.root
    }


}