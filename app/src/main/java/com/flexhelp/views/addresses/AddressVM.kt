package com.flexhelp.views.addresses

import android.content.Context
import androidx.lifecycle.ViewModel
import com.flexhelp.MainActivity
import com.flexhelp.R

class AddressVM(val context: Context): ViewModel() {

    fun onClicks(type: String) {
        when (type) {
            "addnewaddress" -> {
                (context as MainActivity).navController.navigate(R.id.action_address_to_addaddress)
            }

            "imageClicksAddressBack" -> {
                (context as MainActivity).navController.navigateUp()
            }



        }
    }


}