package com.flexhelp.views.addresses

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.flexhelp.MainActivity
import com.flexhelp.databinding.AddressBinding
import com.flexhelp.factory.Factory
import java.lang.ref.WeakReference


class Address : Fragment() {

    var vm: AddressVM? = null
    var binding: AddressBinding? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = AddressBinding.inflate(inflater)
        val factory = Factory(WeakReference<Context>(requireContext()), childFragmentManager)
        vm = ViewModelProvider(this, factory).get(AddressVM::class.java)
        binding!!.vm = vm
        return binding!!.root
        // Inflate the layout for this fragment
    }


    override fun onResume() {
        super.onResume()
        MainActivity.HomeInterface?.isVisible(isVisible = false)


    }


}