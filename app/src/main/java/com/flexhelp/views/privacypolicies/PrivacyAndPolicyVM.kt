package com.flexhelp.views.privacypolicies

import android.content.Context
import androidx.lifecycle.ViewModel
import com.flexhelp.views.termsandconditions.TermAndCondition

class PrivacyAndPolicyVM(val context: Context):ViewModel() {


    fun onClicks(type: String) {
        when (type) {

            "backFromPrivacyPolicy" -> {
                (context as PrivacyAndPolicy).onBackPressed()
            }
        }
    }
}