package com.flexhelp.views.privacypolicies

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.flexhelp.R
import com.flexhelp.databinding.PrivacyandPolicyBinding
import com.flexhelp.factory.Factory
import java.lang.ref.WeakReference

class PrivacyAndPolicy : AppCompatActivity() {



    var vm: PrivacyAndPolicyVM? = null
    var binding: PrivacyandPolicyBinding? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
       // setContentView(R.layout.privacyand_policy)


        binding = DataBindingUtil.setContentView(this, R.layout.privacyand_policy)
        val factory = Factory(WeakReference<Context>(this), supportFragmentManager)
        vm = ViewModelProvider(this, factory).get(PrivacyAndPolicyVM::class.java)
        binding!!.vm = vm

    }
}