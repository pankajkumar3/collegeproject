package com.flexhelp.views.privacypolicies

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.flexhelp.views.payments.PaymentVM
import java.lang.ref.WeakReference

class PrivacyAndPolicyFactory(private val context: WeakReference<Context>) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(
                PrivacyAndPolicyFactory
                ::class.java
            )
        ) {
            return PrivacyAndPolicyVM(context.get()!!) as T
        }

        throw IllegalArgumentException("Error")
    }


}