package com.flexhelp.views.otpverify

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import java.lang.ref.WeakReference

class OtpVerificationFactory(private val context: WeakReference<Context>) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(
                OtpVerificationFactory
                ::class.java
            )
        ) {
            return OtpVerificationVM(context.get()!!) as T
        }

        throw IllegalArgumentException("Error")
    }


}