package com.flexhelp.views.otpverify.resendotpverificationresponse

data class Data(
    val countryCode: String,
    val phone: String
)