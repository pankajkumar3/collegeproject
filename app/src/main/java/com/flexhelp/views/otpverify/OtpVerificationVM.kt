package com.flexhelp.views.otpverify

import android.content.Context
import android.content.Intent
import android.os.CountDownTimer
import android.util.Log
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.flexhelp.networkcalls.RequestProcessor
import com.flexhelp.networkcalls.RetrofitApi
import com.flexhelp.networkcalls.RetrofitCall
import com.flexhelp.preference.PreferenceFile
import com.flexhelp.utils.CommonMethods.showToast
import com.flexhelp.views.login.signinresponse.SigninResponse
import com.flexhelp.views.resetpasswords.ResetPassword
import com.flexhelp.views.welcome.Welcomes
import com.google.gson.JsonObject
import com.mukesh.mukeshotpview.completeListener.MukeshOtpCompleteListener
import retrofit2.Response

class OtpVerificationVM(val context: Context) : ViewModel() {

    var key = ObservableField<String>("")
    var comes = ObservableField<String>("")
    lateinit var timer: CountDownTimer

    val setText = ObservableField("")
    val email = ObservableField("")

    var set = ObservableField("")
    var data = ObservableField<String>("")
    var phone = ObservableField("")
    var otpIs = ObservableField("")
    var otpIs2 = ObservableField("")

    var timerBoolean = ObservableBoolean(false)

    //  var signupVerification=ObservableField("")
    var type = ObservableField("signupVerification")
    var timerTextIs = ObservableField("00:00")
    var countryCode = ObservableField("+91")


    val otpListener by lazy {
        object : MukeshOtpCompleteListener {

            override fun otpCompleteListener(otp: String?) {

                otpIs.set(otp)
                /* if (type.get() == "forgot") {
                     // callVerifyOtp()
                 } else {
                     callVerifyOtp()
                     verifyResendOTP(otp!!)
                 }*/
            }
        }
    }


    init {

        timer()
    }


    private fun validOtp(): Boolean {
        when {
            otpIs2.get().toString().trim().isEmpty() || otpIs2.get().toString().trim().length < 4 -> {
                showToast(context, "Please enter valid OTP.")
                return false
            }
        }
        return true

    }


    fun onClicks(type: String) {
        when (type) {
            "fromOtpverification" -> {
                if (validOtp()) {

                    callVerifyOtp()
                }
                // validOtp()

                /*  if (comes.get()!!.isNotEmpty()) {


                      validOtp()
                      callVerifyOtp()
                  }*/
                /* if (isNetworkAvailable(context)) {
                     if (validations()) {
                         callVerifyOtp()
                     }
                     else if (comes.get()!!.isNotEmpty()){
                         callVerifyOtp()
                     }
                 } else
                 {
                     showToast(context, context.getString(R.string.pleaseCheckInternet))
                 }*/


            }
            "backfromVerification" -> {
                (context as OtpVerification).onBackPressed()
            }

            "resend" -> {
                callResend()
            }
        }
    }


    private fun timer() {
        timerBoolean.set(true)
        timer = object : CountDownTimer(30000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                timerTextIs.set("00:" + (millisUntilFinished / 1000).toString() + " seconds")
            }

            override fun onFinish() {
                timerBoolean.set(false)
                cancel()
            }

        }.start()
    }


    private fun callResend() = try {
        val objectJson = JsonObject()
        if (key.get() == "email") {

            objectJson.addProperty("email", data.get())

        } else {
            objectJson.addProperty("phone", data.get())
            objectJson.addProperty("countryCode", countryCode.get())

        }
        RetrofitCall().callService(
            context,
            true,
            "",
            object : RequestProcessor<Response<SigninResponse>> {
                override suspend fun sendRequest(retrofitApi: RetrofitApi): Response<SigninResponse> {
                    return retrofitApi.resendOTP(objectJson)
                }

                override fun onResponse(res: Response<SigninResponse>) {
                    val response = res.body()!!
                    if (res.isSuccessful) {
                        if (response.success) {

                            PreferenceFile.storeLoginData(context, response)
                            timer()
                        } else {
                            showToast(context, response.message)
                        }
                    } else {
                        showToast(context, res.errorBody().toString())
                    }
                }

                override fun onException(message: String?) {

                    Log.e("signException", "====$message")
                }
            })

    } catch (e: Exception) {
        e.printStackTrace()
    }

    private fun callVerifyOtp() = try {
        val objectJson = JsonObject()
        if (key.get() == "email") {
            objectJson.addProperty("otp", otpIs2.get())
            objectJson.addProperty("email", data.get())
        } else {
            objectJson.addProperty("phone", data.get())
            objectJson.addProperty("countryCode", countryCode.get())
            objectJson.addProperty("otp", otpIs2.get()!!)

        }
        if (comes.get() == "forgot")
            objectJson.addProperty("type", "forgotVerification")
        else
            objectJson.addProperty("type", "signupVerification")

        RetrofitCall().callService(
            context,
            true,
            "",
            object : RequestProcessor<Response<SigninResponse>> {
                override suspend fun sendRequest(retrofitApi: RetrofitApi): Response<SigninResponse> {
                    return retrofitApi.otpVerifyPhone(objectJson)
                }

                override fun onResponse(res: Response<SigninResponse>) {
                    val response = res.body()!!
                    if (res.isSuccessful) {


                        if (response.success) {
                            PreferenceFile.storeLoginData(context, response)
                            when (comes.get()) {
                                "forgot" -> {
                                    timer.cancel()
                                    PreferenceFile.storeAuthToken(
                                        context,
                                        response.data.accessToken
                                    )
                                    (context as OtpVerification).startActivity(
                                        Intent(context, ResetPassword::class.java).putExtra(
                                            "key",
                                            key
                                        ).putExtra("comes", "key")
                                    )
                                    timer.cancel()
                                    context.finish()
                                }
                                "signUp" -> {
                                    timer.cancel()
                                    PreferenceFile.storeAuthToken(
                                        context,
                                        response.data.accessToken
                                    )
                                    (context as OtpVerification).startActivity(
                                        Intent(context, Welcomes::class.java)
                                    )
                                    timer.cancel()
                                    context.finish()
                                }
                            }
                        } else {
                            showToast(context, response.message)
                        }
                    } else {
                        showToast(context, res.errorBody().toString())
                    }
                }

                override fun onException(message: String?) {
                    Log.e("signException", "====$message")
                }
            })

    } catch (e: Exception) {
        e.printStackTrace()
    }


}

