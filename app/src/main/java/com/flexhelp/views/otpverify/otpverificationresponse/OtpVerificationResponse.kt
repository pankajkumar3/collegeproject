package com.flexhelp.views.otpverify.otpverificationresponse

data class OtpVerificationResponse(
    val `data`: Data,
    val message: String,
    val success: Boolean
)