package com.flexhelp.views.otpverify.otpverificationresponse

data class Data(
    val __v: Int,
    val _id: String,
    val accessToken: String,
    val countryCode: String,
    val createdAt: String,
    val email: String,
    val fullName: String,
    val id: String,
    val isDeleted: Boolean,
    val isSocialLogin: Int,
    val isVerified: Boolean,
    val lat: Double,
    val lng: Double,
    val location: Location,
    val password: String,
    val phone: String,
    val profilePic: String,
    val status: Int,
    val updatedAt: String,
    val userId: String,
    val verificationType: String
)