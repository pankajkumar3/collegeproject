package com.flexhelp.views.otpverify.resendotpverificationresponse

data class ResendOtpVerificationResponse(
    val `data`: Data,
    val message: String,
    val success: Boolean
)