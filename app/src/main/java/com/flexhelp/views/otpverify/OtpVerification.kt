package com.flexhelp.views.otpverify

import android.content.Context
import android.os.Bundle
import android.os.CountDownTimer
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.flexhelp.R
import com.flexhelp.databinding.OtpverificationBinding
import com.flexhelp.factory.Factory
import com.flexhelp.utils.CommonMethods.getFormattedCountDownTimer
import kotlinx.android.synthetic.main.otpverification.*
import java.lang.ref.WeakReference


class OtpVerification() : AppCompatActivity() {

    private var vm: OtpVerificationVM? = null
    var binding: OtpverificationBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        binding = DataBindingUtil.setContentView(this, R.layout.otpverification)
        val factory = Factory(WeakReference<Context>(this), supportFragmentManager)
        vm = ViewModelProvider(this, factory).get(OtpVerificationVM::class.java)
        getData()
        binding!!.vm = vm
    }




    private fun getData() {
        if (intent.hasExtra("key")) {
            if (intent.getStringExtra("key") == "email") {
                vm?.key?.set("email")
            } else {
                vm?.key?.set("phone")
            }
        }
        if (intent.hasExtra("comes")) {
            when (intent.getStringExtra("comes")) {
                "forgot" -> {
                    vm!!.comes.set("forgot")

                }
                "signUp" -> {
                    vm!!.comes.set("signUp")

                }
            }
        }
        if (intent.hasExtra("data")) {
            vm!!.data.set(intent.getStringExtra("data"))
        }
    }
}