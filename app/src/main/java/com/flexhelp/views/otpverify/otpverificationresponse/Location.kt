package com.flexhelp.views.otpverify.otpverificationresponse

data class Location(
    val coordinates: List<Double>,
    val type: String
)