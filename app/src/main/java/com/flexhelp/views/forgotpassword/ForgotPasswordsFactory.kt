package com.flexhelp.views.forgotpassword

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import java.lang.ref.WeakReference

class ForgotPasswordsFactory(private val context: WeakReference<Context>) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(
                ForgotPasswordsVM
                ::class.java
            )
        ) {
            return ForgotPasswordsVM(context.get()!! as ForgotPasswords) as T
        }

        throw IllegalArgumentException("Error")
    }


}