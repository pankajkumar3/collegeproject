package com.flexhelp.views.forgotpassword

import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.flexhelp.R
import com.flexhelp.databinding.ForgotPasswordsBinding
import com.flexhelp.factory.Factory
import java.lang.ref.WeakReference

class ForgotPasswords : AppCompatActivity() {
    var vm: ForgotPasswordsVM? = null
    var binding: ForgotPasswordsBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // setContentView(R.layout.forgot_passwords)


        binding = DataBindingUtil.setContentView(this, R.layout.forgot_passwords)

        val factory = Factory(WeakReference<Context>(this), supportFragmentManager)

        vm = ViewModelProvider(this, factory).get(ForgotPasswordsVM::class.java)

        binding!!.vm = vm
        getData()
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)


    }

    private fun getData() {
        if (intent.hasExtra("key")) {
            vm!!.key.set(intent.getStringExtra("key"))
            if (intent.getStringExtra("key")=="email"){
                binding!!.edtPhonNo.visibility = View.VISIBLE
                binding!!.edtEmail.visibility = View.INVISIBLE
            }else{
                binding!!.edtPhonNo.visibility = View.VISIBLE
                binding!!.edtEmail.visibility = View.INVISIBLE
            }
        }
        if (intent.hasExtra("comes")) {
            vm!!.comes.set("forgot")
        }
    }
}