package com.flexhelp.views.forgotpassword.forgotresponse

data class ForgotPasswordsResponse(
    val `data`: Data,
    val message: String,
    val success: Boolean
)