package com.flexhelp.views.forgotpassword.forgotresponse

data class Data(
    val countryCode: String,
    val phone: String
)