package com.flexhelp.views.changepasswords

import android.content.Context
import android.util.Log
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.flexhelp.MainActivity
import com.flexhelp.R
import com.flexhelp.networkcalls.RequestProcessor
import com.flexhelp.networkcalls.RetrofitApi
import com.flexhelp.networkcalls.RetrofitCall
import com.flexhelp.preference.PreferenceFile
import com.flexhelp.utils.CommonMethods.isNetworkAvailable
import com.flexhelp.utils.CommonMethods.isValidPassword
import com.flexhelp.utils.CommonMethods.showToast
import com.flexhelp.views.changepasswords.changepasswordsresponse.ChangePasswordsResponse
import retrofit2.Response

class ChangePasswordVM(val context: Context) : ViewModel() {


    var oldPassword = ObservableField("")
    var newPassword = ObservableField("")
    var confirmPassword = ObservableField("")
    var authorization = ObservableField("")

    var token = ""


    init {
        token = PreferenceFile.retrieveAuthToken(context).toString()
    }


    fun onClicks(type: String) {

        when (type) {


            "saveChangePassword" -> {
                if (isNetworkAvailable(context)) {
                    if (validations()) {

                        changePassword()
                        // (context as MainActivity).navController.navigate(R.id.action_changePassword_to_settingsS)
                        //  changePassword()

                    }

                }


            }


            "backFromChangepassword" -> {
                (context as MainActivity).navController.navigateUp()
            }

        }
    }

    private fun validations(): Boolean {

        when {
            oldPassword.get()!!.isEmpty() -> {

                showToast(context, context.getString(R.string.pleaseEnterPassword))
                return false
            }

            !isValidPassword(oldPassword.get()) && oldPassword.get().toString().length < 8 -> {
                showToast(context, "please enter valid current password ")
                return false

            }
            newPassword.get()!!.isEmpty() -> {
                showToast(context, context.getString(R.string.pleaseEnterNewPassword))
                return false
            }

            newPassword.get()!!.toString().trim().length < 8 -> {
                showToast(context, context.getString(R.string.pleaseEnternewpassAtleast))

                return false
            }

            !isValidPassword(newPassword.get()) && newPassword.get().toString().length < 8 -> {

                showToast(context, "password must be atleast 8 characters long")

                return false
            }

            confirmPassword.get()!!.isEmpty() -> {
                showToast(context, context.getString(R.string.pleaseEnterConfirmPassword))
                return false
            }


            !newPassword.get().equals(confirmPassword.get()!!) || confirmPassword.get().toString()
                .isEmpty() -> {
                showToast(context, context.getString(R.string.passwordNotMatch))
                return false
            }
            else -> {
                return true

            }

        }
    }


    private fun changePassword() {
        try {
            RetrofitCall().callService(
                context,
                true,
                token,
                object : RequestProcessor<Response<ChangePasswordsResponse>> {
                    override suspend fun sendRequest(retrofitApi: RetrofitApi): Response<ChangePasswordsResponse> {
                        return retrofitApi.changePassword(
                            authorization.get()!!,
                            oldPassword.get()!!,
                            newPassword.get()!!,
                            confirmPassword.get()!!
                        )
                    }

                    override fun onResponse(res: Response<ChangePasswordsResponse>) {
                        if (res.body()?.success == true) {
//                            val response = res.body()!!
                            showToast(context, res.body()!!.message)

                            (context as MainActivity).navController.navigateUp()

                            //    (context as MainActivity).navigateUpTo(Intent(this@ChangePasswordVM ,Settings::class.java))
                        } else {
                            showToast(context, "Please Enter correct password")
                        }
                    }

                    override fun onException(message: String?) {
                        Log.e("userException", "====$message")
                    }
                })

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

}

