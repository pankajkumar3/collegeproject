package com.flexhelp.views.changepasswords.changepasswordsresponse

data class ChangePasswordsResponse(
    val message: String,
    val success: Boolean
)