package com.flexhelp.views.changepasswords

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.flexhelp.MainActivity
import com.flexhelp.R
import com.flexhelp.databinding.ChangePasswordBinding
import com.flexhelp.factory.Factory
import java.lang.ref.WeakReference

class ChangePassword : Fragment(R.layout.change_password) {

    var vm: ChangePasswordVM? = null
    var binding: ChangePasswordBinding? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = ChangePasswordBinding.inflate(inflater)
        val factory = Factory(WeakReference<Context>(requireContext()), childFragmentManager)
        vm = ViewModelProvider(this, factory).get(ChangePasswordVM::class.java)
        binding!!.vm = vm
        return binding!!.root
    }

    override fun onResume() {
        super.onResume()
        MainActivity.HomeInterface?.isVisible(isVisible = false)


    }


}