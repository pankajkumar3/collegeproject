package com.flexhelp.views.changepasswords

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.flexhelp.views.chats.ChatVM
import java.lang.ref.WeakReference

class ChangePasswordFactory(private val context: WeakReference<Context>) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(
                ChangePasswordFactory
                ::class.java
            )
        ) {
            return ChangePasswordVM(context.get()!!) as T
        }

        throw IllegalArgumentException("Error")
    }


}