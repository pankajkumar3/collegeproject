package com.flexhelp.views.resetpasswords

import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.flexhelp.R
import com.flexhelp.networkcalls.RequestProcessor
import com.flexhelp.networkcalls.RetrofitApi
import com.flexhelp.networkcalls.RetrofitCall
import com.flexhelp.preference.PreferenceFile
import com.flexhelp.utils.CommonMethods
import com.flexhelp.utils.CommonMethods.isValidPassword
import com.flexhelp.utils.CommonMethods.showToast
import com.flexhelp.views.login.SignIn
import com.flexhelp.views.resetpasswords.resetpasswordresponse.ResetPasswordResponse
import retrofit2.Response

class ResetPasswordVM(val context: Context) : ViewModel() {

    var authorization = ObservableField("")
    var password = ObservableField("")
    var confirmPassword = ObservableField("")
    var token = ""

    init {
        token = PreferenceFile.retrieveAuthToken(context).toString()
    }

    fun onClicks(type: String) {
        when (type) {

            "save" -> {
                if (CommonMethods.isNetworkAvailable(context)) {
                    if (validations()) {
                        resetPassword()
                    }
                }
            }
            "backfromResetPassword" -> {
                (context as ResetPassword).onBackPressed()
            }
        }
    }


    private fun validations(): Boolean {

        when {

            password.get()!!.isEmpty() -> {
                showToast(context, context.getString(R.string.pleaseEnterPassword))
                return false
            }

            password.get()!!.trim().length<8->{
                showToast(context,context.getString(R.string.pleaseEnterPassAtleast))
                return false

            }
            confirmPassword.get()!!.isEmpty() -> {
                showToast(context, context.getString(R.string.pleaseEnterConfirmPassword))
                return false
            }

            confirmPassword.get()!!.trim().length<8->{

                showToast(context,context.getString(R.string.confirmPasswordMust))
return false

            }
           /* !isValidPassword(password.get()) && password.get().toString().length < 8 -> {

                showToast(context, "please enter valid password at least 8 characters")

                return false
            }*/

            !password.get().equals(confirmPassword.get()!!) || confirmPassword.get()!!
                    .isEmpty() -> {
                showToast(context, context.getString(R.string.passwordNotMatch))
                return false
            }

            else -> {
                return true
            }
        }
    }


    private fun resetPassword() {
        try {
            RetrofitCall().callService(
                    context,
                    true,
                    token,
                    object : RequestProcessor<Response<ResetPasswordResponse>> {
                        override suspend fun sendRequest(retrofitApi: RetrofitApi): Response<ResetPasswordResponse> {
                            return retrofitApi.resetPassword(
                                    authorization.get()!!,
                                    password.get()!!,
                                    confirmPassword.get()!!
                            )
                        }

                        override fun onResponse(res: Response<ResetPasswordResponse>) {
                            val response = res.body()

                            if (res.body()?.success!!) {

                                showToast(context, res.body()!!.message)
                                context.startActivity(Intent(context, SignIn::class.java))
                                (context as ResetPassword).finishAffinity()

                                // val response = res.body()!!
                                // showToast(context, response.message)
                                //  (context as MainActivity).navController!!.navigate(R.id.signin)
                            }
                        }

                        override fun onException(message: String?) {
                            Log.e("userException", "====$message")
                        }
                    })

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


}

