package com.flexhelp.views.resetpasswords

import android.content.Context
import android.os.Bundle
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.flexhelp.R
import com.flexhelp.databinding.ResetPasswordBinding
import com.flexhelp.factory.Factory
import java.lang.ref.WeakReference


class ResetPassword : AppCompatActivity() {

    private var vm: ResetPasswordVM? = null
    var binding: ResetPasswordBinding? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
       // setContentView(R.layout.reset_password)


        binding = DataBindingUtil.setContentView(this, R.layout.reset_password)
        val factory = Factory(WeakReference<Context>(this), supportFragmentManager)
        vm = ViewModelProvider(this, factory).get(ResetPasswordVM::class.java)
        binding!!.vm = vm
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)

    }

}