package com.flexhelp.views.resetpasswords

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.flexhelp.views.profile.ProfilesVM
import java.lang.ref.WeakReference

class ResetPasswordFactory(private val context: WeakReference<Context>) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(
                ResetPasswordVM
                ::class.java
            )
        ) {
            return ResetPasswordVM(context.get()!!) as T
        }

        throw IllegalArgumentException("Error")
    }


}