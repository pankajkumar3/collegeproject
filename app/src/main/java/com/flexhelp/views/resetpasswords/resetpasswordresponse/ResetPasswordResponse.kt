package com.flexhelp.views.resetpasswords.resetpasswordresponse

data class ResetPasswordResponse(
    val message: String,
    val success: Boolean
)