package com.flexhelp.views.paymentmethods

import android.content.Context
import androidx.lifecycle.ViewModel
import com.flexhelp.MainActivity
import com.flexhelp.R

class PaymentMethodVM(val context: Context) : ViewModel() {


    fun onClicks(type: String) {
        when (type) {

            "paymentmethod" -> {
                (context as MainActivity).navController.navigate(R.id.paymentToCongratulation)

            }

            "backfrompaymentmethod" -> {

                (context as MainActivity).navController.navigateUp()

            }


        }
    }


}