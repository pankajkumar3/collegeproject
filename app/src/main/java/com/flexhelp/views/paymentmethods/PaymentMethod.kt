package com.flexhelp.views.paymentmethods

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.flexhelp.databinding.PaymentmethodBinding
import com.flexhelp.factory.Factory
import java.lang.ref.WeakReference


class PaymentMethod : Fragment() {
    var vm: PaymentMethodVM? = null
    var binding: PaymentmethodBinding? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = PaymentmethodBinding.inflate(inflater)
        val factory = Factory(WeakReference<Context>(requireContext()), childFragmentManager)
        vm = ViewModelProvider(this, factory).get(PaymentMethodVM::class.java)
        binding!!.vm = vm
        return binding!!.root
    }

}