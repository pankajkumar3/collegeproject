package com.flexhelp.views.paymentmethods

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.flexhelp.views.otpverify.OtpVerificationVM
import java.lang.ref.WeakReference

class PaymentMethodFactory(private val context: WeakReference<Context>) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(
                PaymentMethodFactory
                ::class.java
            )
        ) {
            return PaymentMethodVM(context.get()!!) as T
        }

        throw IllegalArgumentException("Error")
    }


}