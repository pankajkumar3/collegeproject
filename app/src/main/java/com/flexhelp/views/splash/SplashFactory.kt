package com.flexhelp.views.splash

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import java.lang.ref.WeakReference

class SplashFactory (private val context: WeakReference<Context>) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(SplashVM::class.java)) {
            return SplashVM(context.get()!!) as T
        }

        throw IllegalArgumentException("Error")
    }
}