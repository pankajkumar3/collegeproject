package com.flexhelp.views.splash

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import com.flexhelp.R
import com.flexhelp.views.walksthrough.WalkThrough

class Splash : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splash)


        Handler(Looper.getMainLooper()).postDelayed({
            val intent= Intent(this,WalkThrough::class.java)
            startActivity(intent)
            finish()



        },2000)
    }
}