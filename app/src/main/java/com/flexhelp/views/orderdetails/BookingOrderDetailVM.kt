package com.flexhelp.views.orderdetails

import android.content.Context
import androidx.databinding.ObservableBoolean
import androidx.lifecycle.ViewModel
import com.flexhelp.MainActivity
import com.flexhelp.R

class BookingOrderDetailVM(val context: Context) : ViewModel() {

    var isTrackNow = ObservableBoolean(false)
    var isRate = ObservableBoolean(false)
    var isOngoing = ObservableBoolean(true)
    fun onClicks(type: String) {
        when (type) {

            "cancel" -> {
                if (isOngoing.get()) {
                    (context as MainActivity).navController.navigate(R.id.action_bookingOrderDetail_to_cancelBooking)
                }
            }

            "chat" -> {
                (context as MainActivity).navController.navigate(R.id.orderDetailToChat)
            }
            "tracknow" -> {
                (context as MainActivity).navController.navigate(R.id.orderDetailToTrackOrder)
            }
            "rate" -> {
                isRate.set(false)
                (context as MainActivity).navController.navigate(R.id.action_bookingOrderDetail_to_feedBacks)

            }
            "giveRatings" -> {
                if (!isOngoing.get()) {
                    isRate.set(true)
                }
            }

            "backfromBookingorderDetail" -> {
                (context as MainActivity).navController.navigateUp()


            }


        }
    }

    fun onClick(type: String) {
        when (type) {
            "isTrackNow" -> {
                isTrackNow.set(true)
            }

        }
    }



}