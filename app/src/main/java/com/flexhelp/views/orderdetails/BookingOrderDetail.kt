package com.flexhelp.views.orderdetails

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.flexhelp.MainActivity
import com.flexhelp.databinding.BookingorderDetailBinding
import com.flexhelp.factory.Factory
import java.lang.ref.WeakReference

class BookingOrderDetail : Fragment() {

    var vm: BookingOrderDetailVM? = null
    var binding: BookingorderDetailBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = BookingorderDetailBinding.inflate(inflater)
        val factory = Factory(WeakReference<Context>(requireContext()), childFragmentManager)
        vm = ViewModelProvider(this, factory).get(BookingOrderDetailVM::class.java)
        binding!!.vm = vm

        getData()
        return binding!!.root
    }

    private fun getData() {
        if (arguments != null) {
            if (arguments?.getString("key") == "ongoing") {
                vm!!.isOngoing.set(true)
            } else {
                vm!!.isRate.set(true)
                vm!!.isOngoing.set(false)

            }
        }
    }

    override fun onResume() {
        super.onResume()
        MainActivity.HomeInterface?.isVisible(isVisible = false)


    }


}