package com.flexhelp.views.orderdetails

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import java.lang.ref.WeakReference

class BookingOrderDetailFactory(private val context: WeakReference<Context>) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(
                BookingOrderDetailFactory
                ::class.java
            )
        ) {
            return BookingOrderDetailVM(context.get()!!) as T
        }

        throw IllegalArgumentException("Error")
    }


}