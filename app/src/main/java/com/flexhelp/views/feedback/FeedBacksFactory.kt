package com.flexhelp.views.feedback

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import java.lang.ref.WeakReference

class FeedBacksFactory(private val context: WeakReference<Context>) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(
                FeedBacksFactory
                ::class.java
            )
        ) {
            return FeedBacksVM(context.get()!!) as T
        }

        throw IllegalArgumentException("Error")
    }


}