package com.flexhelp.views.feedback

import android.content.Context
import androidx.lifecycle.ViewModel
import com.flexhelp.MainActivity
import com.flexhelp.R

class FeedBacksVM(var context: Context):ViewModel() {

    fun onClicks(type: String) {

        when (type) {
            "backFromFeedbacks" -> {
                (context as MainActivity).navController.navigateUp()
            }

            "submitFeedBack" -> {
                (context as MainActivity).navController.navigate(R.id.action_feedBacks_to_home2)
            }

        }
    }

}