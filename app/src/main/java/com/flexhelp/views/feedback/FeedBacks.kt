package com.flexhelp.views.feedback

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.flexhelp.databinding.FeedbacksBinding
import com.flexhelp.factory.Factory
import java.lang.ref.WeakReference

class FeedBacks : Fragment() {

    private var vm: FeedBacksVM? = null
    var binding: FeedbacksBinding? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FeedbacksBinding.inflate(inflater)
        val factory = Factory(WeakReference<Context>(requireContext()), childFragmentManager)
        vm = ViewModelProvider(this, factory).get(FeedBacksVM::class.java)
        binding!!.vm = vm
        return binding!!.root   }

}