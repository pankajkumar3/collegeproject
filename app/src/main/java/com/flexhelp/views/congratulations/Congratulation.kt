package com.flexhelp.views.congratulations

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.flexhelp.MainActivity
import com.flexhelp.databinding.CongratulationBinding
import com.flexhelp.factory.Factory
import java.lang.ref.WeakReference


class Congratulation : Fragment() {
    var vm: CongratulationVM? = null
    var binding: CongratulationBinding? = null
    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        binding = CongratulationBinding.inflate(inflater)
        val factory = Factory(WeakReference<Context>(requireContext()), childFragmentManager)
        vm = ViewModelProvider(this, factory).get(CongratulationVM::class.java)
        binding!!.vm = vm
        return binding!!.root
    }
    override fun onResume() {
        super.onResume()
        MainActivity.HomeInterface?.isVisible(isVisible = false)


    }


}