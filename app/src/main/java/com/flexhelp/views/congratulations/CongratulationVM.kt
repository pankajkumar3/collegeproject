package com.flexhelp.views.congratulations

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.ViewModel
import com.flexhelp.MainActivity
import com.flexhelp.R
import com.flexhelp.views.orderdetails.BookingOrderDetail
import com.flexhelp.views.paymentmethods.PaymentMethod

class CongratulationVM(val context: Context):ViewModel() {

    fun onClicks(type: String) {
        when (type) {

            "previebooking" ->
            {
                var bundle = Bundle()
                bundle.putString("key","ongoing")
                (context as MainActivity).navController.navigate(R.id.conratulationToBookOrderDetails,bundle)
            }


        }
    }


}