package com.flexhelp.views.subcat

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.flexhelp.MainActivity
import com.flexhelp.databinding.SubcategoriesBinding
import com.flexhelp.factory.Factory
import java.lang.ref.WeakReference

class SubCategories : Fragment() {

    var vm: SubCategoriesVM? = null
    var binding: SubcategoriesBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = SubcategoriesBinding.inflate(inflater)
        val factory = Factory(WeakReference<Context>(requireContext()), childFragmentManager)
        vm = ViewModelProvider(this, factory).get(SubCategoriesVM::class.java)
        binding!!.vm = vm
        return binding!!.root
    }
    override fun onResume() {
        super.onResume()
        MainActivity.HomeInterface?.isVisible(isVisible = false)


    }


}