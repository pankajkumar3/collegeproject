package com.flexhelp.views.subcat

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.flexhelp.views.paymentmethods.PaymentMethodVM
import java.lang.ref.WeakReference

class SubCategoriesFactory(private val context: WeakReference<Context>) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(
                SubCategoriesFactory
                ::class.java
            )
        ) {
            return SubCategoriesVM(context.get()!!) as T
        }

        throw IllegalArgumentException("Error")
    }


}