package com.flexhelp.views.profile

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import com.flexhelp.MainActivity
import com.flexhelp.databinding.ProfilesBinding
import com.flexhelp.factory.Factory
import com.flexhelp.interfaces.HomeInterface
import com.flexhelp.utils.CommonMethods.saveBitmapToFile
import com.theartofdev.edmodo.cropper.CropImage
import kotlinx.android.synthetic.main.profiles.*
import java.io.File
import java.io.InputStream
import java.lang.ref.WeakReference


open class Profiles : Fragment() {
    private lateinit var vm: ProfilesVM
    private lateinit var binding: ProfilesBinding

    private var isProfileCick = false

    companion object {
        var HomeInterface: HomeInterface? = null
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = ProfilesBinding.inflate(inflater)
        val factory = Factory(WeakReference<Context>(requireContext()), childFragmentManager)
        vm = ViewModelProvider(this, factory).get(ProfilesVM::class.java)
        HomeInterface?.isVisible(false)
        binding.vm = vm
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        vm.imageOnClick.observe(viewLifecycleOwner) {
            if (it) {
                 CropImage.activity().start(requireActivity(), this)
                vm.imageOnClick.value = false
            }
        }
    }


    override fun onResume() {
        super.onResume()
        MainActivity.HomeInterface?.isVisible(isVisible = false)


    }



    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE -> {
                if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
                    val result = CropImage.getActivityResult(data)
                    val postImageURI = result!!.uri
                        ivProfileCicular.setImageURI(postImageURI)
                        vm.imageFile = saveBitmapToFile(File(postImageURI!!.path!!))
                        //vm.callUpdateImageAPI()
                }
            }
        }
    }
}