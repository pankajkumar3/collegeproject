package com.flexhelp.views.profile.updateprofileresponse

import com.flexhelp.provider.activities.verifyOTP.verifyotpproviderresponse.Location

data class Data(
    val __v: Int,
    val _id: String,
    val accessToken: String,
    val address: String,
    val countryCode: String,
    val createdAt: String,
    val deviceType: String,
    val email: String,
    val fullName: String,
    val id: String,
    val isDeleted: Boolean,
    val isSocialLogin: Int,
    val isVerified: Boolean,
    val lat: Double,
    val lng: Double,
    val location: Location,
    val password: String,
    val phone: String,
    val profilePic: String,
    val pushToken: String,
    val resetPasswordExpires: String,
    val resetPasswordToken: String,
    val status: Int,
    val updatedAt: String,
    val verificationType: String
)