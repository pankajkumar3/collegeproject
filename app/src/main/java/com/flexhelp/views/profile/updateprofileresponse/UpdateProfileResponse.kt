package com.flexhelp.views.profile.updateprofileresponse

data class UpdateProfileResponse(
    val `data`: Data,
    val message: String,
    val success: Boolean
)