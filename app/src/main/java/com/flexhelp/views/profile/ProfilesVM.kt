package com.flexhelp.views.profile

import android.Manifest
import android.app.Activity
import android.content.Context
import android.util.Log
import androidx.core.app.ActivityCompat
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.flexhelp.MainActivity
import com.flexhelp.R
import com.flexhelp.networkcalls.RequestProcessor
import com.flexhelp.networkcalls.RetrofitApi
import com.flexhelp.networkcalls.RetrofitCall
import com.flexhelp.preference.PreferenceFile
import com.flexhelp.utils.CommonMethods
import com.flexhelp.utils.CommonMethods.checkPermission
import com.flexhelp.utils.CommonMethods.showToast
import com.flexhelp.views.login.signinresponse.SigninResponse
import com.flexhelp.views.profile.profileresponse.ProfileImageUploadRespone
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import retrofit2.Response
import java.io.File
import java.util.regex.Pattern


class ProfilesVM(val context: Context) : ViewModel() {
    var isprofile = ObservableBoolean(true)
    var isEnabled = ObservableBoolean(false)
    var email = ObservableField("")
    var isEmail = ObservableBoolean(true)
    var imageName = ObservableField("")
    var isNumber = ObservableBoolean(true)
    var profilePic = ObservableField("")
    var phone: ObservableField<String> = ObservableField("")
    var address = ObservableField("Mohali")
    var fullName = ObservableField("")
    var countryCode = ObservableField("+91")
    var imageOnClick = MutableLiveData(false)
    var imageFile: File? = null

    init {

        val logindata = PreferenceFile.retrieveLoginData(context)
        phone.set(logindata?.data?.phone ?: "")
        email.set(logindata?.data?.email ?: "")
        fullName.set(logindata?.data?.fullName ?: "")
        profilePic.set(logindata?.data?.profilePic ?: "")
        // address.set(logindata?.data?.location.toString() ?:"")

    }

    init {
        val logindata = PreferenceFile.retrieveLoginData(context)
        phone.set(logindata?.data?.phone ?: "")
        email.set(logindata?.data?.email ?: "")
        fullName.set(logindata?.data?.fullName ?: "")
        profilePic.set(logindata?.data?.profilePic ?: "")
        // address.set(logindata?.data?.location.toString() ?:"")

    }


    fun onClicks(type: String) {
        when (type) {
            "back" -> {
                (context as MainActivity).navController.navigateUp()
            }
            "isProfile" -> {
                isprofile.set(false)
                isEnabled.set(true)
            }
            "save" -> {

                if (validations()) {
                    updateProfile()
                }
            }
            "pic" -> {
                if (!isprofile.get())
                    openGallery()
            }
        }
    }

    private fun validations(): Boolean {

        when {
            fullName.get()!!.toString().trim().isEmpty() -> {
                showToast(context, context.getString(R.string.pleaseEnterFullName))
                return false
            }


            email.get()!!.toString().trim().isEmpty() && isEmail.get() -> {

                showToast(context, context.getString(R.string.pleaseEnterEmail))

                return false
            }


            !isValidEmailId(email.get()!!.toString().trim()) -> {

                showToast(context, context.getString(R.string.pleaseEnterValidEmail))
                return false
            }

            phone.get()!!.toString().trim().isEmpty() && isNumber.get() -> {
                showToast(context, context.getString(R.string.pleaseEnterPhone))
                return false
            }
            phone.get()!!.toString().trim().length < 8 -> {
                showToast(context, context.getString(R.string.pleaseEnterPhoneAtleast))
                return false
            }

            /*address.get()!!.isEmpty() -> {
                CommonMethods.showToast(context, context.getString(R.string.pleaseEnterAddress))
                return false
            }*/

            else -> {

                return true
            }
        }
    }

    private fun openGallery() {
        val permission = arrayOf(
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        )
        if (checkPermission(context as Activity, permission) > 0) {
            ActivityCompat.requestPermissions(context, arrayOf(permission[0], permission[1]), 101)
        } else {
            imageOnClick.value = true
        }
    }

    fun callUpdateImageAPI() {
        val token = PreferenceFile.retrieveAuthToken(context)
        val requestBody: RequestBody = imageFile!!.asRequestBody("image/*".toMediaTypeOrNull())
        val imageToUpload =
            MultipartBody.Part.createFormData("image", imageFile!!.name, requestBody)

        RetrofitCall().callService(
            context,
            true,
            token!!,
            object : RequestProcessor<Response<ProfileImageUploadRespone>> {
                override suspend fun sendRequest(retrofitApi: RetrofitApi): Response<ProfileImageUploadRespone> {
                    return retrofitApi.uploadImages(imageToUpload)
                }

                override fun onResponse(res: Response<ProfileImageUploadRespone>) {
                    if (res.isSuccessful) {
                        val response = res.body()!!
                        profilePic.set(response.data.image)
                    }
                }

                override fun onException(message: String?) {
                    showToast(context, message!!)
                    Log.e("userException", "====$message")
                }
            })
    }

    private fun updateProfile() {
        val map = HashMap<String, String>()
        map["fullName"] = fullName.get()!!
        map["email"] = email.get()!!
        map["phone"] = phone.get()!!
        map["countryCode"] = countryCode.get()!!
        map["phone"] = phone.get()!!
        map["address"] = address.get()!!
        callUpdateProfile(map)

    }

    private fun isValidEmailId(email: String): Boolean {
        return Pattern.compile(
            "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                    + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                    + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                    + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$"
        ).matcher(email).matches()
    }


    private fun callUpdateProfile(map: HashMap<String, String>) {
        val token = PreferenceFile.retrieveAuthToken(context)

        var profilePic: MultipartBody.Part? = null
        if (imageFile != null) {
            val requestBody: RequestBody = imageFile!!.asRequestBody("image/*".toMediaTypeOrNull())
            val imageToUpload =
                MultipartBody.Part.createFormData("profilePic", imageFile!!.name, requestBody)
            profilePic = imageToUpload
        }

        try {
            RetrofitCall().callService(
                context,
                true,
                token!!,
                object : RequestProcessor<Response<SigninResponse>> {
                    override suspend fun sendRequest(retrofitApi: RetrofitApi): Response<SigninResponse> {
                        return retrofitApi.updateProfile(
                            CommonMethods.convertRequestBodyFromMap(map), profilePic
                        )
                    }

                    override fun onResponse(res: Response<SigninResponse>) {
                        val response = res.body()!!

                        if (res.isSuccessful) {

                            if (response.success) {
                                PreferenceFile.storeLoginData(context, response)
                                PreferenceFile.storeAuthToken(context, response.data.accessToken)
                                isprofile.set(true)
                                isEnabled.set(false)
                                (context as MainActivity).navController.navigate(R.id.action_profiles_to_home2)
                            } else {
                                showToast(context, response.message)
                            }
                        }

                        /* else
                         {
                             showToast(context, response.message)
                         }*/
                    }

                    override fun onException(message: String?) {
                        Log.e("userException", "====$message")
                    }
                })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


}

