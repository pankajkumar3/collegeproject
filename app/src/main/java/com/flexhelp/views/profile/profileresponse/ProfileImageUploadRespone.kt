package com.flexhelp.views.profile.profileresponse

data class ProfileImageUploadRespone(
    val `data`: Data,
    val message: String,
    val success: Boolean
)