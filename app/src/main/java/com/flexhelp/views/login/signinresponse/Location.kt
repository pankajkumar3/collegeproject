package com.flexhelp.views.login.signinresponse

data class Location(
    val coordinates: List<Double>,
    val type: String
)