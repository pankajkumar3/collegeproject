package com.flexhelp.views.login

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.WindowManager
import android.widget.CheckBox
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ObservableBoolean
import androidx.lifecycle.ViewModelProvider
import com.flexhelp.MainActivity
import com.flexhelp.R
import com.flexhelp.databinding.LoginBinding
import com.flexhelp.factory.Factory
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.activity_signup.*
import kotlinx.android.synthetic.main.login.*
import kotlinx.android.synthetic.main.profiles.*
import java.lang.ref.WeakReference
class SignIn : AppCompatActivity() {
    var ischecked = ObservableBoolean(false)


    var vm: SignInVM? = null
    var binding: LoginBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.login)
        val factory = Factory(WeakReference<Context>(this), supportFragmentManager)
        vm = ViewModelProvider(this, factory).get(SignInVM::class.java)
        binding!!.vm = vm
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
    }
}