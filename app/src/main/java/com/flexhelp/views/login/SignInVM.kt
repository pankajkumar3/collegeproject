package com.flexhelp.views.login

import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.flexhelp.MainActivity
import com.flexhelp.R
import com.flexhelp.model.RememberMeData
import com.flexhelp.networkcalls.RequestProcessor
import com.flexhelp.networkcalls.RetrofitApi
import com.flexhelp.networkcalls.RetrofitCall
import com.flexhelp.preference.PreferenceFile
import com.flexhelp.utils.CommonMethods.isNetworkAvailable
import com.flexhelp.utils.CommonMethods.showToast
import com.flexhelp.views.forgotpassword.ForgotPasswords
import com.flexhelp.views.login.signinresponse.SigninResponse
import com.flexhelp.views.signuptwo.SignUpSecond
import com.google.gson.JsonObject
import retrofit2.Response

class SignInVM(val context: Context) : ViewModel() {


    var ischecked = ObservableBoolean(false)
    //     var phoneNumber = ObservableField("")

    var RememberMe = ObservableField("")
    var password = ObservableField("")
    var emailPassword = ObservableField("")
    var email = ObservableField("")
    var phone = ObservableField("")
    var countryCode = ObservableField("+91")
    var profilePic = ObservableField("")

    var isEmail = ObservableBoolean(true)


    init {

        val rememberMeData = PreferenceFile.retrieveRememberMe(context)
        if (rememberMeData != null) {
            email.set(rememberMeData.email)
            phone.set(rememberMeData.phone)
            //  countryCode.set(rememberMeData.countryCode)
            password.set(rememberMeData.password)
            ischecked.set(rememberMeData.isRemember)
        }


    }


    fun onClicks(type: String) {
        when (type) {
            "email" -> {
                if (!isEmail.get())
                    isEmail.set(true)
                else
                    isEmail.set(false)
                // (context as MainActivity).navController!!.navigate(R.id.home)
            }


            "loginFromMainActivity" -> {
                if (isNetworkAvailable(context)) {
                    if (validations()) {
                        callLogin()
                    }
                } else {
                    showToast(context, context.getString(R.string.pleaseCheckInternet))
                }
            }
            "signupFromLogin" -> {

                (context as SignIn).startActivity(Intent(context, SignUpSecond::class.java))
            }

            "forgotpassFromLogin" -> {
                var key = ""
                key = if (isEmail.get()) {
                    "email"
                } else {
                    "phone"
                }
                (context as SignIn).startActivity(
                    Intent(
                        context,
                        ForgotPasswords::class.java
                    ).putExtra("key", key).putExtra("comes", "forgot")
                )
            }


            "check" -> {
                if (ischecked.get()) {
                    ischecked.set(false)
                } else {
                    ischecked.set(true)
                }
            }
        }
    }


    private fun validations(): Boolean {

        when {
            email.get()!!.toString().trim().isEmpty() && isEmail.get() -> {
                showToast(context, context.getString(R.string.pleaseEnterEmail))
                return false
            }
            phone.get()!!.toString().trim().isEmpty() && !isEmail.get() -> {
                showToast(context, context.getString(R.string.pleaseEnterPhone))
                return false
            }
            phone.get()!!.toString().trim().length < 8 && !isEmail.get() -> {
                showToast(context, context.getString(R.string.pleaseEnterPhoneAtleast))
                return false
            }

            password.get()!!.toString().trim().isEmpty() -> {
                showToast(context, context.getString(R.string.pleaseEnterPassword))
                return false
            }
            else -> {
                return true
            }
        }
    }


    private fun callLogin() {
        val jsonElement = JsonObject()
        if (isEmail.get()) {
            jsonElement.addProperty("email", email.get())
            jsonElement.addProperty("password", emailPassword.get())
        } else {
            jsonElement.addProperty("phone", phone.get())
            jsonElement.addProperty("countryCode", countryCode.get())
            jsonElement.addProperty("password", password.get())
        }
        try {
            RetrofitCall().callService(
                context,
                true,
                "",
                object : RequestProcessor<Response<SigninResponse>> {
                    override suspend fun sendRequest(retrofitApi: RetrofitApi): Response<SigninResponse> {
                        return retrofitApi.login(jsonElement)
                    }

                    override fun onResponse(res: Response<SigninResponse>) {
                        val response = res.body()!!
                        if (res.isSuccessful) {
                            val rememberMeModel = RememberMeData()
                            if (response.success) {
                                if (ischecked.get()) {
                                    if (isEmail.get()) {
                                        rememberMeModel.email = email.get()!!
                                        rememberMeModel.phone = ""
                                        rememberMeModel.emailPassword = emailPassword.get()!!
                                        // rememberMeModel.countryCode = ""
                                    } else {
                                        rememberMeModel.email = ""
                                        rememberMeModel.phone = phone.get()!!
                                        rememberMeModel.password = password.get()!!
                                    }
                                }

                                rememberMeModel.isRemember = ischecked.get()

                                PreferenceFile.storeLoginData(context, response)
                                PreferenceFile.storeAuthToken(context, response.data.accessToken)
                                PreferenceFile.storeRememberMeData(context, rememberMeModel)
                                (context as SignIn).startActivity(
                                    Intent(
                                        context,
                                        MainActivity::class.java
                                    )
                                )
                                context.finish()
                            } else {
                                showToast(context, response.message)
                            }
                        } else {
                            showToast(context, context.getString(R.string.backendError))
                        }
                    }

                    override fun onException(message: String?) {
                        Log.e("signException", "====$message")
                    }

                })

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}