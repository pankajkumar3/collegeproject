package com.flexhelp.views.login.signinresponse

data class SigninResponse(
    val `data`: Data,
    val message: String,
    val success: Boolean
)