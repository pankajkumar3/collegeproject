package com.flexhelp.views.setting

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.ViewModel
import com.flexhelp.MainActivity
import com.flexhelp.R
import com.flexhelp.preference.PreferenceFile
import com.flexhelp.views.usertype.UserTypes

class SettingsVM(val context: Context) : ViewModel() {
    fun onclicks(value: String) {
        when (value) {
            "gofromChangePassword" -> {
                (context as MainActivity).navController.navigate(R.id.action_settingsS_to_changePassword)
            }

            "gofromContactUs" -> {
                (context as MainActivity).navController.navigate(R.id.action_settingsS_to_contactUs)
            }

            "gofromNotification" -> {
                (context as MainActivity).navController.navigate(R.id.action_settingsS_to_notification)
            }
            "aboutus" -> {


            }

            "settingsTosuggestion" -> {

                (context as MainActivity).navController.navigate(R.id.action_settingsS_to_suggestion)

            }


            "logout" -> {
                showLogoutPopup()
            }


            "settingsaddAddress" -> {

                (context as MainActivity).navController.navigate(R.id.action_settingsS_to_address)

                //   CommonMethods.loadFragment(context,Address())

            }
/*
            "logout"->{
                logout()

            }
*/
        }

    }


    private fun showLogoutPopup() {

        AlertDialog.Builder(context).setTitle("Logout")
            .setMessage("Are you sure you want to logout ?")
            .setPositiveButton("Logout") { _, _ ->
                PreferenceFile.clearPreference(context)
                (context).startActivity(Intent(context, UserTypes::class.java))
                (context as Activity).finishAffinity()
            }
            .setNeutralButton("Close", null)
            .show()

    }
}