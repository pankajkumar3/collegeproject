package com.flexhelp.views.setting

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.flexhelp.views.feedback.FeedBacksVM
import java.lang.ref.WeakReference

class SettingsFactory(private val context: WeakReference<Context>) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(
              SettingsFactory  ::class.java
            )
        ) {
            return SettingsVM(context.get()!!) as T
        }

        throw IllegalArgumentException("Error")
    }


}