package com.flexhelp.views.setting

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.flexhelp.MainActivity
import com.flexhelp.databinding.SettingsBinding
import com.flexhelp.factory.Factory
import com.flexhelp.preference.PreferenceFile.sharedPreferences
import kotlinx.android.synthetic.main.profiles.*
import java.lang.ref.WeakReference


class Settings : Fragment() {

    var vm: SettingsVM? = null
    var binding: SettingsBinding? = null
lateinit var preferences: SharedPreferences


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ):View? {
        // Inflate the layout for this fragment
        binding = SettingsBinding.inflate(inflater)
        val factory = Factory(WeakReference<Context>(requireContext()), childFragmentManager)
        vm = ViewModelProvider(this, factory).get(SettingsVM::class.java)
        binding!!.vm = vm
        return binding!!.root
    }

    private fun getSharedPrefrences(unit: String, modePrivate: Int) {

    }

    override fun onResume() {
        super.onResume()
        MainActivity.HomeInterface?.isVisible(isVisible = true)


    }



}